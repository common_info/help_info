def urlify(url: str):
    conversion_table: dict[str, str] = {
        " ": "%20",
        "(": "%28",
        ")": "%29"
    }
    for k, v in conversion_table.items():
        url = url.replace(k, v)
    return url


if __name__ == '__main__':
    _ = urlify("https://svn.protei.ru/svn/techwriters/Roaming Service Module/PROTEI Roaming Services.pdf")
    print(_)
