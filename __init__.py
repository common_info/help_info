import os.path as p
from os import mkdir
import warnings
from importlib import import_module
from subprocess import run, CalledProcessError
from sys import executable, version_info

from help_info.custom_exceptions import ImportLibraryDependencyFailed

_parent_path: str = p.abspath(p.dirname(p.abspath(__file__)))
__path__.append(_parent_path)

warnings.simplefilter("ignore")


if version_info < (3, 8):
    print("The Python version must be at least 3.8.")
    input("Press <Enter> to close the window ...")
    exit(-1)


_packages: tuple[str, ...] = ("loguru", "urllib3")
for _package in _packages:
    try:
        import_module(_package)
    except (ModuleNotFoundError, ImportError):
        try:
            run([executable, "-m", "pip", "install", _package]).check_returncode()
        except CalledProcessError as e:
            print(f"{e.__class__.__name__}\nКод ответа {e.returncode}\nОшибка {e.output}")
            print(f"Не удалось импортировать пакет `{_package}`.")
            raise ImportLibraryDependencyFailed
        except OSError as e:
            print(f"{e.__class__.__name__}\nФайл {e.filename}\nОшибка {e.strerror}")
            print(f"Не удалось импортировать пакет `{_package}`")
            raise ImportLibraryDependencyFailed
        else:
            print(f"Пакет {_package} установлен")


_log_folder: str = p.join(_parent_path, "logs")

if not p.isdir(_log_folder):
    try:
        mkdir(_log_folder)
    except RuntimeError as e:
        print(f"{e.__class__.__name__}\n{str(e)}")
        raise
    except PermissionError as e:
        print(f"{e.__class__.__name__}\n{e.strerror}")
        raise
