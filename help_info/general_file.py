from pathlib import Path

from loguru import logger

from help_info.const_classes import StrPath


class GeneralFile:
    def __init__(self, path: StrPath):
        self._path: Path = Path(path).resolve()
        self._content: list[str] = []
        self._validate()

    def _validate(self):
        if not self._path.exists():
            logger.error(f"Path {self._path} does not exist")
            raise FileNotFoundError
        if not self._path.is_file():
            logger.error(f"Path {self._path} does not point to the file")
            raise IsADirectoryError
        return

    def __str__(self):
        return "\n".join(self._content)

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def read(self):
        with open(self._path, "r+") as f:
            _: list[str] = f.readlines()
        self._content = _

    def save(self):
        with open(self._path, "w+") as f:
            f.write(str(self))

    def __iter__(self):
        return iter(self._content)

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self._content
        return False

    def __getitem__(self, item):
        return self._content[item]

    def __setitem__(self, key, value):
        self._content[key] = value

    def __len__(self):
        return len(self._content)

    def __bool__(self):
        return len(self) != 0

    @property
    def empty(self):
        return [_ for _ in self._content if not _]

    @property
    def non_empty(self):
        return [_ for _ in self._content if _]

    @property
    def name(self):
        return self._path.name

    @property
    def content(self):
        return self._content

