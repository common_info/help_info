from __future__ import annotations

from pathlib import Path
from re import Pattern, compile
from typing import Iterable, NamedTuple

from loguru import logger

from common_functions import StrPath, flatten_nested, get_chunks, iter_pairs
from const_classes import LinkType, TableCellCoordinate, TableItem
from custom_exceptions import AsciiDocFileNotFoundError, AsciiDocFilePartNotationError, \
    AsciiDocFileTableItemTypeError, AsciiDocFileTableKeyError, AsciiDocFileTableRowIndexError


class AsciiDocFile:
    def __init__(self, path: StrPath):
        if isinstance(path, str):
            path: Path = Path(path).resolve()
        self._path: Path = path

    def _validate(self):
        if not self._path.exists():
            logger.error(f"Path {self._path} is invalid")
            raise AsciiDocFileNotFoundError

    @property
    def reader(self):
        return AsciiDocFileReader(self._path)

    @property
    def writer(self):
        return AsciiDocFileWriter(self._path)

    def read(self):
        return self.reader.read_list_str()

    def write(self, content: str | bytes):
        return self.writer.write_list(content)

    @property
    def path(self):
        return self._path


class AsciiDocFileReader(AsciiDocFile):
    def read_bytes(self) -> bytes:
        with open(self._path, "rb") as fb:
            _: bytes = fb.read()
        return _

    def read_str(self) -> str:
        with open(self._path, "rb") as fb:
            _: bytes = fb.read()
        return _.decode("utf-8")

    def read_list_str(self) -> list[str]:
        _: list[str] = []
        with open(self._path, "rb") as fb:
            for line in fb:
                _line: str = line.decode("utf-8")
                _.append(_line)
        return _

    def read_list_bytes(self) -> list[bytes]:
        _: list[bytes] = []
        with open(self._path, "rb") as fb:
            for line in fb:
                _.append(line)
        return _


class AsciiDocFileWriter(AsciiDocFile):
    def write(self, content: str | bytes):
        if isinstance(content, str):
            content = bytes(content)
        with open(self._path, "wb") as fb:
            fb.write(content)

    def write_list(self, content: Iterable[str | bytes]):
        _: list[bytes] = list(map(lambda x: bytes(x), content))
        with open(self._path, "wb") as fb:
            fb.writelines(_)


class AsciiDocDocument:
    def __init__(self, ascii_doc_file: AsciiDocFile):
        self._ascii_doc_file: AsciiDocFile = ascii_doc_file
        self._content: list[str] = []
        self._document_parts: dict[str, AsciiDocDocumentPart | list[AsciiDocDocumentPart] | None] = dict()

    @property
    def path(self) -> Path:
        return self._ascii_doc_file.path

    def set_content(self):
        self._content = self._ascii_doc_file.reader.read_list_str()

    @property
    def document_parts_values(self):
        return flatten_nested(self._document_parts.values())

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self._content
        elif isinstance(item, AsciiDocDocumentPart):
            return item in self.document_parts_values
        else:
            raise NotImplementedError

    def __iter__(self):
        return iter(self._content)

    def __len__(self):
        return len(self._content)

    def __bool__(self):
        return len(self._content) > 0

    def __getitem__(self, item):
        return self._content[item]

    def has_line(self, line: str):
        return any(_ == line for _ in self._content)

    def get_indexes(self, notation: str, index: int = -1) -> tuple[int, ...]:
        if not self.has_line(notation):
            logger.error(f"Notation {notation} is not found")
            raise AsciiDocFilePartNotationError
        _indexes: list[int] = [index for index, line in enumerate(iter(self)) if line == notation]
        logger.info(_indexes)
        _: list[tuple[int, ...]] = get_chunks(_indexes)
        logger.info(_)
        if index == -1:
            return _[0]
        else:
            return _[index]

    @property
    def document_parts(self):
        return self._document_parts

    @property
    def name(self):
        return self.path.name

    def __str__(self):
        return "".join(self._content)


class AsciiDocDocumentPart:
    def __init__(self, document: AsciiDocDocument, notation: str, type_name: str, part_index: int = -1):
        self._document: AsciiDocDocument = document
        self._document.set_content()
        self._content: list[str] = []
        self._part_index: int = part_index
        self._notation: str = notation
        self._index_from: int = 0
        self._index_to: int = -1

        if self._part_index == -1:
            if type_name not in self._document.document_parts.keys():
                self._document.document_parts[type_name] = None
            self._document.document_parts[type_name] = self
        else:
            if type_name not in self._document.document_parts.keys():
                self._document.document_parts[type_name] = []
            self._document.document_parts.get(type_name).append(self)

    def set_indexes(self):
        logger.info(self._part_index)
        logger.info(self._document.get_indexes(self._notation, self._part_index))
        self._index_from, self._index_to = self._document.get_indexes(self._notation, self._part_index)

    def set_content(self):
        self._content = self._document[slice(self._index_from, self._index_to + 1)]

    def __iter__(self):
        return iter(self._content)

    def __hash__(self):
        return hash((self._index_from, self._index_to))

    def __key(self):
        return self._index_from, self._index_to

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented
        if any(x == -1 for x in (self._part_index, other._part_index)):
            return self._notation == other._notation
        else:
            return self._part_index == other._part_index

    def __ne__(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented
        if any(x == -1 for x in (self._part_index, other._part_index)):
            return self._notation != other._notation
        else:
            return self._part_index != other._part_index

    def __lt__(self, other):
        if not isinstance(other, self.__class__) or all(x != -1 for x in (self._part_index, other._part_index)):
            return NotImplemented
        return self._part_index < other._part_index

    def __gt__(self, other):
        if not isinstance(other, self.__class__) or all(x != -1 for x in (self._part_index, other._part_index)):
            return NotImplemented
        return self._part_index > other._part_index

    def __le__(self, other):
        if not isinstance(other, self.__class__) or all(x != -1 for x in (self._part_index, other._part_index)):
            return self == other
        return self._part_index <= other._part_index

    def __ge__(self, other):
        if not isinstance(other, self.__class__) or all(x != -1 for x in (self._part_index, other._part_index)):
            return self == other
        return self._part_index <= other._part_index

    def __getitem__(self, item) -> str | list[str]:
        return self._content[item]

    @property
    def content(self):
        return self._content


class AsciiDocDocumentTable(AsciiDocDocumentPart):
    def __init__(self, document: AsciiDocDocument, part_index: int):
        notation: str = "|==="
        type_name: str = "table"
        super().__init__(document, notation, type_name, part_index)
        self._content: list[str] = []
        self._index_from: int = 0
        self._index_to: int = -1
        self._header: list[str] = []
        self._items: dict[TableCellCoordinate, TableItem] = dict()

    def __str__(self):
        return "".join(self._content)

    def __repr__(self):
        return "\n".join([f"{str(k)}: {str(v)}" for k, v in self.iter_items()])

    def __len__(self):
        return len(self._header)

    def get_column(self, column_index: int) -> str:
        return "\n".join(
            [str(self[(row_index, column_index)]) for row_index in range(self.max_row)])

    @property
    def max_row(self):
        return max(_.row_index for _ in self._items.keys())

    @property
    def max_col(self):
        return max(_.column_index for _ in self._items.keys())

    @property
    def _header_string(self):
        return " ".join([f"|{item}" for item in self._header])

    def set_header(self):
        pattern: Pattern = compile(r"(\|(.*)\|(.*))\n")
        for index, line in enumerate(self.iter_lines()):
            if pattern.fullmatch(line):
                header_line: str = line
                break
        else:
            header_line: str = self[2]
        _: list[str] = header_line.split("|")
        self._header = [line.strip() for line in _]

    def __getitem__(self, item) -> str | list[str] | TableItem:
        if isinstance(item, (int, slice)):
            return self._content[item]
        elif isinstance(item, TableCellCoordinate):
            return self._items[item]
        else:
            raise KeyError

    def __setitem__(self, key, value):
        if isinstance(key, int):
            if isinstance(value, str):
                self._content[key] = value
            else:
                logger.error(f"Value {value} must be str, but {type(value)} received")
                raise AsciiDocFileTableItemTypeError
        elif isinstance(key, slice):
            if isinstance(value, Iterable) and all(isinstance(item, str) for item in value):
                self._content[key] = value
            else:
                logger.error(f"Each value {value} must be str")
                raise AsciiDocFileTableItemTypeError
        elif isinstance(key, TableCellCoordinate):
            if isinstance(value, TableItem):
                self._items[key] = value
            else:
                logger.error(f"Value {value} must be TableItem, but {type(value)} received")
                raise AsciiDocFileTableItemTypeError
        else:
            logger.error(f"Key must be int, slice, or TableCellCoordinate, but {type(key)} received")
            raise AsciiDocFileTableKeyError

    def set_items(self):
        _empty: list[int] = [index for index, line in enumerate(self._content) if line == "\n"]
        logger.info(_empty)
        for row_index, (_from, _to) in enumerate(iter_pairs(_empty[1:-1])):
            lines: list[str] = [content.strip("\n") for content in self._content[_from + 1:_to + 1]]
            _: list[str] = "".join(lines).split("|")
            for column_index, text in enumerate(_[1:]):
                _key: TableCellCoordinate = TableCellCoordinate(row_index, column_index)
                _value: TableItem = TableItem(row_index, column_index, text.strip("|"))
                self[_key] = _value

    @property
    def items(self) -> dict[TableCellCoordinate, TableItem]:
        return self._items

    def get_row(self, row_index: int) -> str:
        if row_index > self.max_row:
            logger.error(f"Row index {row_index} is out of bounds")
            raise AsciiDocFileTableRowIndexError
        return " -- ".join(
            [str(v) for k, v in self.iter_items() if k.row_index == row_index and v.text])

    def get_row_items(self, row_index: int):
        if row_index > self.max_row:
            logger.error(f"Row index {row_index} is out of bounds")
            raise AsciiDocFileTableRowIndexError
        return [str(v) for k, v in self.iter_items() if k.row_index == row_index]

    def complete(self):
        self.set_indexes()
        self.set_content()
        self.set_header()
        self.set_items()

    def ascii_doc_table(self):
        self.complete()
        return AsciiDocTable(self._content)

    def iter_lines(self):
        return iter(self._content)

    def iter_items(self):
        return iter(self._items.items())


class AsciiDocTable:
    def __init__(self, lines: Iterable[str] = None):
        if lines is None:
            lines: list[str] = []
        self._content: list[str] = [*lines]
        self._header: list[str] = []
        self._items: dict[TableCellCoordinate, TableItem] = dict()

    def __iter__(self):
        return iter(self._content)

    def __str__(self):
        return "".join(self._content)

    def __repr__(self):
        return "\n".join([f"{str(k)}: {str(v)}" for k, v in self._items.items()])

    def get_column(self, column_index: int) -> str:
        return "\n".join(
            [str(self._items.get(TableCellCoordinate(row_index, column_index))) for row_index in range(self.max_row)])

    @property
    def max_row(self):
        return max(_.row_index for _ in self._items.keys())

    @property
    def max_col(self):
        return max(_.column_index for _ in self._items.keys())

    @property
    def _header_string(self):
        return " ".join([f"|{item}" for item in self._header])

    def _find_header(self):
        pattern: Pattern = compile(r"(\|(.*)\|(.*))\n")
        for index, line in enumerate(iter(self)):
            if pattern.fullmatch(line):
                return index
        else:
            return 2

    def set_header(self):
        header_line: str = self[self._find_header()]
        _: list[str] = header_line.split("|")
        self._header = [line.strip() for line in _]

    def __getitem__(self, item) -> str | list[str] | TableItem:
        if isinstance(item, (int, slice)):
            return self._content.__getitem__(item)
        elif isinstance(item, TableCellCoordinate):
            return self._items.__getitem__(item)
        else:
            raise KeyError

    def __setitem__(self, key, value):
        if isinstance(key, int):
            if isinstance(value, str):
                self._content.__setitem__(key, value)
            else:
                logger.error(f"Value {value} must be str, but {type(value)} received")
                raise AsciiDocFileTableItemTypeError
        elif isinstance(key, slice):
            if isinstance(value, Iterable) and all(isinstance(item, str) for item in value):
                self._content.__setitem__(key, value)
            else:
                logger.error(f"Each value {value} must be str")
                raise AsciiDocFileTableItemTypeError
        elif isinstance(key, TableCellCoordinate):
            if isinstance(value, TableItem):
                self._items.__setitem__(key, value)
            else:
                logger.error(f"Value {value} must be TableItem, but {type(value)} received")
                raise AsciiDocFileTableItemTypeError
        else:
            logger.error(f"Key must be int, slice, or TableCellCoordinate, but {type(key)} received")
            raise AsciiDocFileTableKeyError

    def set_items(self):
        _empty: list[int] = [index for index, line in enumerate(iter(self)) if line == "\n"]
        for row_index, (_from, _to) in enumerate(iter_pairs(_empty[1:-1])):
            lines: list[str] = [content.strip("\n") for content in self._content[slice(_from + 1, _to)]]
            _: list[str] = "".join(lines).split("|")
            for column_index, text in enumerate(_[1:]):
                _key: TableCellCoordinate = TableCellCoordinate(row_index, column_index)
                _value: TableItem = TableItem(row_index, column_index, text.strip("|"))
                self._items[_key] = _value

    @property
    def items(self) -> dict[TableCellCoordinate, TableItem]:
        return self._items

    def get_row(self, row_index: int) -> str:
        if row_index > self.max_row:
            logger.error(f"Row index {row_index} is out of bounds")
            raise AsciiDocFileTableRowIndexError
        return " -- ".join(
            [str(v) for k, v in self._items.items() if k.row_index == row_index and v.text])

    def get_row_items(self, row_index: int):
        if row_index > self.max_row:
            logger.error(f"Row index {row_index} is out of bounds")
            raise AsciiDocFileTableRowIndexError
        return [str(v) for k, v in self.items if k.row_index == row_index]

    def complete(self):
        self.set_header()
        self.set_items()

    def from_file(self, path: StrPath, part_index: int = -1):
        ascii_doc_file: AsciiDocFile = AsciiDocFile(path)
        document: AsciiDocDocument = AsciiDocDocument(ascii_doc_file)
        _ = AsciiDocDocumentTable(document, part_index)
        _.items.update(**self._items)


class AsciiDocLink(NamedTuple):
    link_type: LinkType
    text: str
    name: str

    def __str__(self):
        return f"{self.text}[{self.name}]"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.text}, {self.name})>"

    def __hash__(self):
        return hash((self.text, self.name))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.name == other.name
        else:
            return False

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.name != other.name
        else:
            return False


class AsciiDocInternalLink(NamedTuple):
    text: str
    name: str | None

    def __str__(self):
        if self.name is None:
            return f"<<{self.text}>>"
        return f"<<{self.text},{self.name}>>"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.text}, {self.name})>"

    def __hash__(self):
        return hash((self.text, self.name))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.name == other.name
        else:
            return False

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.name != other.name
        else:
            return False


class AsciiDocUnorderedList:
    def __init__(self, lines: Iterable[str]):
        if lines is None:
            lines: list[str] = []
        self._lines: list[str] = [*lines]
        self._indexes_level_1: list[int] = []
        self._indexes_level_2: list[int] = []
        self._indexes_level_3: list[int] = []
        self._indexes_level_4: list[int] = []
        self._tree: dict[str, dict] = dict()

    def __iter__(self):
        return iter(self._lines)

    def __getitem__(self, item):
        return self._lines.__getitem__(item)

    def __delitem__(self, key):
        return self._lines.__delitem__(key)

    def remove_empty(self):
        _empty: list[int] = [index for index, _ in enumerate(self.__iter__()) if not _]
        for _ in reversed(_empty):
            self._lines.__delitem__(_)

    def set_root_items(self):
        self._indexes_level_1.extend(index for index, _ in enumerate(self.__iter__()) if _.startswith(("* ", "-", "+")))

    def set_level(self, level: int):
        patterns: list[str] = [f"{_item * level} " for _item in ("*", "-", "+")]
        getattr(self, f"_indexes_level_{level}").extend(
            index for index, _ in enumerate(self.__iter__())
            if _.startswith(*patterns,))

    def set_all_levels(self):
        for level in range(5):
            self.set_level(level)

    # def set_tree(self):
    #     if not self._indexes_level_1:
    #         return
    #     for index_1 in self._indexes_level_1:
    #         self._tree[self[index_1]] = dict()
    #     if not self._indexes_level_2:
    #         return
    #     for index_ in
