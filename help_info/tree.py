from __future__ import annotations

import faulthandler
from typing import Iterable, Self

from loguru import logger

from itertools import chain

from help_info.init_logger import configure_custom_logging


class InvalidTreeElementParentNameError(KeyError):
    """Specified TreeElement name is not found."""


class InvalidChildElementError(ValueError):
    """Specified TreeElement name is not found."""


class InvalidChildElementTypeError(TypeError):
    """Specified object is inappropriate to be a child element."""


class Tree:
    _elements: dict[str, TreeElement] = dict()
    tree = dict()

    def __init__(self, tree_elements: Iterable[TreeElement] = None):
        if tree_elements is not None:
            _: dict[str, TreeElement] = {el.name: el for el in tree_elements}
            self._elements.update(_)
        TreeElement._tree = self

    @classmethod
    def __call__(cls, *args, **kwargs):
        return cls()

    def __add__(self, other):
        if isinstance(other, TreeElement):
            self._elements[other.name] = other
        elif isinstance(other, self.__class__):
            self._elements.update(**other._elements)
            del other
        else:
            return NotImplemented

    __radd__ = __add__
    __iadd__ = __add__

    def __sub__(self, other):
        if isinstance(other, str):
            if other != "root":
                self._elements.pop(other)
            else:
                logger.info("Root element cannot be deleted")
        elif isinstance(other, TreeElement):
            if isinstance(other, RootElement):
                logger.info("Root element cannot be deleted")
            else:
                self._elements.pop(other.name)
        else:
            return NotImplemented

    def __missing__(self, key):
        logger.error(f"Key {key} is missing")
        return RootElement()

    def __getitem__(self, item):
        if isinstance(item, str):
            return self._elements.get(item)
        else:
            return NotImplemented

    def __iter__(self):
        return iter(_ for _ in self._elements.values() if not isinstance(_, RootElement))

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self._elements.keys()
        elif isinstance(item, TreeElement):
            return item in self._elements.values()
        else:
            return False

    def __len__(self):
        return len(self._elements)

    def tree_values(self):
        return chain(self.tree.values())

    def generate_tree(self):
        _: TreeElement
        self.tree["root"] = []
        for index in range(0, self.max_level):
            for _ in self.get_level(index):
                self.tree[_.parent_name].append(_)

    @property
    def keys(self) -> list[str]:
        return [*self._elements.keys()]

    @property
    def values(self) -> list[TreeElement]:
        return [*self._elements.values()]

    @property
    def max_level(self) -> int:
        return max(_.level for _ in iter(self))

    def get_level(self, level: int) -> list[TreeElement]:
        return [_ for _ in self.values if _.level == level]


class TreeElement:
    _tree: Tree | None = None

    def __init__(
            self,
            name: str, *,
            parent_name: str | None = None,
            children_names: Iterable[str] = None,
            **kwargs):
        if children_names is None:
            children_names: list[str] = []
        self._name: str = name
        self._parent_name: str | None = parent_name
        self._children_names: list[str] = [*children_names]
        self._level: int | None = None
        self._kwargs: list[str] = [*kwargs.keys()]
        for k, v in kwargs.items():
            setattr(self, k, v)

    def __str__(self):
        kwargs = ", ".join([getattr(self, arg) for arg in self._kwargs])
        return f"{self._name}({kwargs})"

    __repr__ = __str__

    def complete(self):
        self.add_to_tree().find_parent().set_level()

    def add_to_tree(self):
        self._tree + self
        return self

    @property
    def parent(self):
        return self._tree[self._parent_name]

    def children(self):
        return [self._tree[_] for _ in self._children_names]

    def __bool__(self):
        return self._parent_name == "root"

    def set_level(self):
        if self._parent_name is None:
            self.find_parent()
        if self:
            self._level = 0
        else:
            self._level = self.parent.level + 1
        return

    def find_parent(self):
        if self._parent_name is not None:
            return self
        parent: Self
        for parent in iter(self._tree):
            if self._name in parent.children_names:
                self._parent_name = parent.name
                return self
        else:
            logger.error(f"No parent for element {self._name}")
            raise InvalidTreeElementParentNameError

    @property
    def children_names(self):
        return self._children_names

    @property
    def name(self):
        return self._name

    def add_child(self, child):
        if isinstance(child, str):
            if child not in self._tree.keys:
                logger.error(f"Child {child} is not found")
                raise InvalidChildElementError
            if child not in self._children_names:
                self._children_names.append(child)
            else:
                logger.info(f"Child {child} is already listed")
            return
        elif isinstance(child, self.__class__):
            if child not in self._tree.values:
                logger.error(f"Child {child._name} is not found")
                raise InvalidChildElementError
            if child._name not in self._children_names:
                self._children_names.append(child._name)
            else:
                logger.info(f"Child {child._name} is already listed")
            return
        else:
            logger.error(f"Child {child} of type {type(child)} cannot be a child element")
            raise InvalidChildElementTypeError

    def delete_child(self, child):
        if isinstance(child, str):
            if child in self._children_names:
                self._children_names.remove(child)
                self._tree - child
            else:
                logger.info(f"Child {child} is already deleted")
            return
        elif isinstance(child, self.__class__):
            if child._name in self._children_names:
                self._children_names.remove(child._name)
                self._tree - child
            else:
                logger.info(f"Child {child._name} is already deleted")
            return
        else:
            logger.error(f"Child {child} of type {type(child)} cannot be a child element")
            raise InvalidChildElementTypeError

    @property
    def level(self):
        return self._level

    @property
    def parent_name(self):
        return self._parent_name


class RootElement(TreeElement):
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
            cls._name: str = "root"
            cls._parent_name: str = ""
            cls._level: int = -1
            for k, v in kwargs.items():
                setattr(cls._instance, k, v)
        return cls._instance

    def find_parent(self):
        return

    def __bool__(self):
        return True

    @property
    def parent(self):
        return self

    @classmethod
    def __call__(cls):
        return cls._instance

    @property
    def level(self):
        return self._level


@logger.catch
@configure_custom_logging("tree")
def main():
    four: TreeElement = TreeElement("four", parent_name="two")
    five: TreeElement = TreeElement("five", parent_name="two")
    six: TreeElement = TreeElement("six", parent_name="three")
    seven: TreeElement = TreeElement("seven", parent_name="three")
    one: TreeElement = TreeElement("one", parent_name="root", children_names=["two", "three"])
    two: TreeElement = TreeElement("two", parent_name="one", children_names=["four", "five"])
    three: TreeElement = TreeElement("three", parent_name="one", children_names=["six", "seven"])
    tree_elements: list[TreeElement] = [one, two, three, four, five, six, seven]
    tree: Tree = Tree()
    for tree_element in tree_elements:
        logger.info(str(tree_element))
        tree_element.complete()
    tree.generate_tree()
    logger.info(tree.tree)


if __name__ == '__main__':
    faulthandler.enable()
    main()
    if faulthandler.is_enabled():
        faulthandler.disable()
