from __future__ import annotations

from pathlib import Path
from typing import Iterable, Iterator, NamedTuple, Any, TypeAlias

from loguru import logger

from help_info.const_classes import StrPath, TableCellCoordinate, TableItem


StrNone: TypeAlias = str | None


class HeadingLevelError(ValueError):
    """Invalid heading level."""


class HeadingNameError(ValueError):
    """Invalid heading name."""


class FileTable:
    def __init__(self, items: Iterable[TableItem] = None):
        if items is None:
            items: list[TableItem] = []
        self._items: list[TableItem] = [*items]

    @classmethod
    def from_file(cls, lines: Iterable[str] = None):
        if lines is None:
            lines: list[str] = []
        _items: list[TableItem] = []
        for row_index, line in enumerate(iter(lines)):
            for column_index, content in enumerate(line.strip("|").split(" | ")):
                _value: TableItem = TableItem(row_index, column_index, content.strip(""))
        return cls(_items)

    def write(self, path: StrPath):
        with open(path, "w+") as f:
            f.writelines(str(self))

    def __str__(self):
        return "\n".join(str(item) for item in iter(self))

    def __iter__(self) -> Iterator[TableItem]:
        return iter(self._items)

    def _coords(self):
        return [_.coord for _ in iter(self)]

    def _find_by_coord(self, coord: TableCellCoordinate):
        if coord not in self._coords():
            logger.error("Key is not found")
        for _ in iter(self):
            if _.coord == coord:
                return _
        else:
            return

    def __getitem__(self, item):
        if isinstance(item, (int, slice)):
            return self._items[item]
        elif isinstance(item, TableCellCoordinate):
            return self._find_by_coord(item)
        else:
            raise KeyError

    def __setitem__(self, key, value):
        if isinstance(key, int):
            if isinstance(value, str):
                self._lines.__setitem__(key, value)
            else:
                ...
        elif isinstance(key, TableCellCoordinate):
            if isinstance(value, TableItem):
                self._items.__setitem__(key, value)
            else:
                ...
        else:
            ...

    def __len__(self):
        return len(self._items)

    def __hash__(self):
        return hash(self._items)

    def __add__(self, other):
        if isinstance(other, FileTableRow):
            self._lines.append(other.__str__())
            other.to_markdown_table(self, self.__len__())
        elif isinstance(other, str):
            self._lines.append(other)
        else:
            return NotImplemented

    __radd__ = __add__
    __iadd__ = __add__

    def insert_row(self, row: FileTableRow | str, index: int = -1):
        if index == -1:
            return self.__add__(row)
        if isinstance(row, FileTableRow):
            return self._lines.insert(index, row.__str__())
        elif isinstance(row, str):
            return self._lines.insert(index, row)
        return

    def iter_rows(self) -> Iterator[FileTableRow]:
        return iter(FileTableRow(line, self, index) for index, line in enumerate(self.__iter__()))

    def rows(self) -> list[FileTableRow]:
        return list(self.iter_rows())

    def get_row(self, item):
        return self.rows()[item]

    def get_line(self, row_index: int):
        return [_.text for _ in iter(self) if _.coord.row_index == row_index]

    # def replace_row(self, __from: int, __to: int):
    #     _row: str = self._lines.pop(__from)
    #     self.insert_row(_row, __to)

    # def delete_row(self, index: int):
    #     self._lines.pop(index)

    @property
    def items(self):
        return self._items

    def text(self):
        return [_.text for _ in iter(self)]

    @property
    def lines(self):
        return [self.get_line(row_index) for row_index in range(self.max_row)]

    @property
    def max_row(self):
        return max(_.coord.row_index for _ in iter(self))

    @property
    def max_column(self):
        return max(_.coord.column_index for _ in iter(self))


class FileTableRow:
    def __init__(self, line: str, markdown_table: FileTable | None = None, row_index: int | None = None):
        self._line: str = line
        self._markdown_table: FileTable | None = markdown_table
        self._row_index: int | None = row_index
        self._cells: list[TableItem] = []

    def to_markdown_table(self, markdown_table: FileTable, row_index: int):
        self._markdown_table = markdown_table
        self._row_index = row_index

    def __str__(self):
        return f"| {' | '.join(self._cells)} |"

    def __iter__(self):
        return iter(self._cells)

    def __getitem__(self, item):
        return self._cells.__getitem__(item)

    def __setitem__(self, key, value):
        self._cells.__setitem__(key, value)

    def __len__(self):
        return len(self._cells)

    def __bool__(self):
        return self.__len__() == 1

    def up_down(self, diff: int = 0):
        if self._markdown_table is not None:
            self._row_index += diff

    def update(self):
        self._line = self.__str__()
        if self._markdown_table is not None:
            self._markdown_table.__setitem__(self._row_index, self._line)

    def insert_cell(self, cell: str, index: int = -1):
        if index == -1:
            self._cells.append(cell)
        else:
            self._cells.insert(index, cell)

    def replace_cell(self, __from: int, __to: int):
        _row: str = self._cells.pop(__from)
        self._cells.insert(__to, _row)

    def delete_cell(self, index: int):
        self._cells.pop(index)

    def split_cell(self, index: int, *, text_old: str | None = None, text_new: str | None = None):
        if text_old is None:
            text_old: str = self.__getitem__(index)
        if text_new is None:
            text_new: str = ""
        self.insert_cell(text_new, index + 1)
        self.__setitem__(index, text_old)

    is_subheader = __bool__


class ImageLink(NamedTuple):
    link: str
    text: str


class Formatting:
    pass


class Block:
    def __init__(self, marker: str, label: str = None, text: str = None, **kwargs):
        self._marker: str = marker
        self._label: str | None = label
        self._text: str | None = text
        self._args: list[str] = [*kwargs.keys()]
        for k, v in kwargs.items():
            setattr(self, k, v)

    def __str__(self):
        _: list[str] = [f"{k}={v}" for k, v in self._kwargs]
        _options: str = ",".join(_)
        _label: str = f".{self._label}\n" if self._label is not None else ""
        _text: str = self._text if self._text is not None else ""
        return f"{_label}[{_options}]{self._marker}\n{_text}\n{self._marker}"

    @property
    def label(self):
        return self._label

    @label.setter
    def label(self, value):
        self._label = value

    @label.deleter
    def label(self):
        self._label = None

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, value):
        self._text = value

    @text.deleter
    def text(self):
        self._text = None

    @property
    def _kwargs(self):
        return {arg: getattr(self, arg) for arg in self._args}

    def add_option(self, name: str, value: Any):
        if name not in self._args:
            self._args.append(name)
        setattr(self, name, value)

    def __len__(self):
        return len(self._text.split("\n"))

    def insert_text(self, text: str, index: int):
        _text: list[str] = self._text.split("\n")
        _text.insert(index % len(self), text)
        self._text = "\n".join(_text)
        return

    def add_text(self, text: str):
        _: str = "".join((self._text, text))
        self._text = _
        return

    def __hash__(self):
        return hash((self._marker, self._label, self._text))

    def __key(self):
        return self._marker, self._label, self._text

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def iter_kwargs(self):
        return iter(self._kwargs.items())

    def iter_lines(self):
        return iter(self._text)


class Heading:
    def __init__(self, level: int, name: StrNone):
        self._level: int = level
        self._name: StrNone = name
        self._validate()

    def __str__(self):
        _marker: str = "=" * (self._level + 1)
        return f"{_marker} {self._name}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._level}, {self._name})>"

    def _validate(self):
        if self._level <= 0:
            logger.error(f"Некорректный уровень {self._level} заголовка {self._name}")
            raise HeadingLevelError
        if self._name is None:
            logger.error(f"Заголовок не имеет имени")
            raise HeadingNameError
        return

    @property
    def level(self):
        return self._level

    @level.setter
    def level(self, value):
        self._level = value

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    def uplevel(self, diff: int) -> None:
        if diff == 0:
            return
        if diff < 0:
            return self.downlevel(-diff)
        self._level = max(self._level - diff, 0)
        return

    def downlevel(self, diff: int) -> None:
        if diff == 0:
            return
        if diff < 0:
            return self.uplevel(-diff)
        self._level += diff
        return

    def __hash__(self):
        return hash((self._level, self._name))

    def __key(self):
        return self._level, self._name

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() != other.__key()
        else:
            return NotImplemented


class CrossReference:
    def __init__(self, file: StrPath, element: StrNone, text: StrNone):
        self._file: Path = Path(file)
        self._element: StrNone = element
        self._text: StrNone = text
        self._validate()

    def __str__(self):
        _element: str = f"#{self._element}" if self._element else ""
        _text: str = f",{self._text}" if self._text else ""
        return f"<<{self._file}{_element}{_text}>>"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._file}, {self._element}, {self._text})>"

    def _validate(self):
        return

    @property
    def file(self):
        return self._file

    @file.setter
    def file(self, value):
        self._file = value

    @property
    def element(self):
        return self._element

    @element.setter
    def element(self, value):
        self._element = value

    @element.deleter
    def element(self):
        self._element = None

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, value):
        self._text = value

    @text.deleter
    def text(self):
        self._text = None

    def __hash__(self):
        return hash((self._file, self._element))

    def __key(self):
        return self._file, self._element

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            if self.__key() == other.__key():
                return self._text == other._text
            return False
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            if self.__key() == other.__key():
                return self._text != other._text
            return True
        else:
            return NotImplemented
