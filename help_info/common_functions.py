import faulthandler
import os.path as p
from itertools import chain, pairwise
from numbers import Number
from pathlib import Path
from re import Pattern, compile
from textwrap import dedent
from typing import Any, Generator, Iterable, Iterator, Mapping, Sequence, TypeAlias

from loguru import logger

JsonDict: TypeAlias = dict[str, list[str | dict] | str | dict]
StrPath: TypeAlias = str | Path


def stringify(value: str | Number | bytes | bytearray | bool | Iterable | None) -> str:
    if value is None:
        return ""
    elif isinstance(value, Iterable):
        return "".join(value)
    else:
        return value.__str__()


def is_divisor(value: int, divisor: int = 2) -> bool:
    try:
        _: int = value % divisor
    except ZeroDivisionError:
        logger.error("Divisor is zero")
    else:
        return _ == 0


def fill_values(values: Sequence[Any], divisor: int = 2) -> list[Any]:
    _: list[object] = [*values]
    _len: int = len(values)
    if not is_divisor(len(values), divisor):
        _.extend([None] * (_len % divisor))
    return _


def gen_chunks(values: Sequence[Any], chunk: int = 2) -> Generator[tuple[Any, ...], Any, None]:
    _: list[object] = fill_values(values, chunk)
    for i in range(0, len(_), chunk):
        logger.error(_[i:i + chunk])
        yield *_[i:i + chunk],


def iter_chunks(values: Sequence[Any], chunk: int = 2) -> Iterator[tuple[Any, ...]]:
    return iter(gen_chunks(values, chunk))


def get_chunks(values: Sequence[Any], chunk: int = 2) -> list[tuple[Any, ...]]:
    return list(gen_chunks(values, chunk))


def flatten_nested(values: Iterable[Any]):
    return list(chain(values))


def gen_pairs(values: Sequence[Any]):
    return pairwise(values)


def iter_pairs(values: Sequence[Any]):
    return iter(pairwise(values))


def get_pairs(values: Sequence[Any]):
    return list(pairwise(values))


# class DocumentLink(NamedTuple):
#     document: Any
#     document_type: DocumentType
#     name: str
#     text: str
#     link: str | None
#     link_type: LinkType
#
#     def link_to(self):
#         if self.document_type == DocumentType.ASCII_DOC:
#             if self.link_type == LinkType.INTERNAL:
#                 return f"{self.text}[{self.name}]"
#             elif self.link_type == LinkType.EXTERNAL:
#                 return f"xref:{self.document.name}#{self.link}:{self.text}[{self.name}]"
#             else:
#                 pass
#         elif self.document_type == DocumentType.MARKDOWN:
#             if self.link_type == LinkType.INTERNAL:
#                 return f"[{self.name}]({self.text})"
#             elif self.link_type == LinkType.EXTERNAL:
#                 return f"{self.document.name}#{self.link}{self.text}[{self.name}]"
#             else:
#                 pass
#
#     def __hash__(self):
#         return hash((self.name, self.text))
#
#     def __repr__(self):
#         ...
#         # return f"xref:{self.document.name}#{self.name}[{self.text}]"
#
#     def __bool__(self):
#         ...


def multiply_char(char: str, multiplier: int):
    if len(char) != 1 | multiplier < 1:
        raise ValueError
    return f"{char * multiplier} "


def recursive_dict_keys(item: Any):
    listed: list[Any] = []

    def iter_children(el):
        if isinstance(el, Mapping):
            listed.extend([*el.keys()])
            for v in el.values():
                iter_children(v)
        elif isinstance(el, (list, tuple)):
            for v in el:
                iter_children(v)

    iter_children(item)
    return listed


def recursive_values(item):
    listed: list[Any] = []

    def iter_children(el):
        if isinstance(el, Mapping):
            for v in el.values():
                iter_children(v)
        elif isinstance(el, (list, tuple)):
            for v in el:
                iter_children(v)
        else:
            listed.append(el)

    iter_children(item)
    return listed


interceptors: tuple[str, ...] = ("__exit__", "exit", "__quit__", "quit", "!q", "!quit", "!e", "!exit")
term_pattern: Pattern = compile(r"([\w_-]+)")
separator: str = "---------------------------------------------------------------------------------------------------\n"
path_temp: str = p.join(p.dirname(p.abspath(__file__)), "_temp_file")

_spec: str = dedent("""\
    Для вывода информации о списке необходимо ввести:
    -a/-all --- отобразить все термины и сокращения на английском;
    -f/-full --- вывод всех терминов и сокращений, а также их полные расшифровки и комментарии;
    """)

_log_folder: str = p.join(p.dirname(p.abspath(__file__)), "logs")
_disclaimer: str = dedent(f"""
    В случае ошибки необходимо сохранить лог-файл, который будет находиться здесь:
    '{_log_folder}'

    https://gitlab.com/tech_writers_protei/terms/-/tree/main

    Утилита для получения полного наименования сокращения и его описания.
    Используется таблица, хранящаяся в репозитории Gitlab:
    https://gitlab.com/tech_writers_protei/info/-/blob/main/terms.md

    Искомые термины и аббревиатуры можно вводить как по одному, так и несколько.
    При вводе нескольких необходимо отделить друг от друга любым не циферным и не буквенным символом, кроме '_' и '-'.
    """)

_help: str = dedent("""\
    Для вывода краткой справочной информации необходимо ввести любой из нижепредложенных вариантов:
    -h, --help
    Для вывода полной справочной информации readme.txt необходимо ввести любой из нижепредложенных вариантов:
    -r/--readme
    Для вывода примеров использования необходимо ввести любой из нижепредложенных вариантов:
    -s/--samples
    """)

_exit: str = dedent("""\
    Для выхода необходимо ввести любой из нижепредложенных вариантов (без кавычек):
    __exit__, exit, __quit__, quit, !e, !exit, !q, !quit
    """)


def convert_to_path(path: StrPath) -> Path:
    if isinstance(path, str):
        path: Path = Path(path)
    return path.resolve()


def run_faulthandler(func):
    def inner(*args, **kwargs):
        faulthandler.enable()
        func(*args, **kwargs)
        if faulthandler.is_enabled():
            faulthandler.disable()
        return
    return inner


def get_name(func: Any) -> str | None:
    if hasattr(func, "__name__"):
        return func.__name__
    else:
        logger.info(f"Item {func} has no attribute __name__")
        return


def get_qualname(func: Any):
    if hasattr(func, "__qualname__"):
        return func.__qualname__
    else:
        logger.info(f"Item {func} has no attribute __qualname__")
        return
