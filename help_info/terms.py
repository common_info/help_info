import faulthandler
import os.path as p
# from pathlib import Path
from typing import Iterable

from loguru import logger

from help_info.common_functions import _disclaimer, _exit, _help, _spec, interceptors, separator
from help_info.const_classes import AsciiDocTableTerms, ContentGitPage, State, _Term
from help_info.init_logger import complete_configure_custom_logging


class UserInputParser:
    def __init__(self, table_terms: AsciiDocTableTerms):
        self._table_terms: AsciiDocTableTerms = table_terms
        self.state: State = State.ACTIVE
        # logger.info(list(Path(__file__).parent.iterdir()))
        # logger.info(p.dirname(p.abspath(__file__)))

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._table_terms.__name__})>"

    def __str__(self):
        return f"{self.__class__.__name__}\n{self._table_terms.terms_short()}"

    def _print_dictionary(self) -> str:
        return "\n".join(list(map(lambda x: x.formatted(), self._terms())))

    @staticmethod
    def _print_help():
        with open(p.join(p.dirname(p.dirname(p.abspath(__file__))), "sources", "terms", "help.txt"), "rb+") as fb:
            help_text: bytes = fb.read()
        return help_text.decode(encoding="utf8")

    def _print_short(self) -> str:
        return ", ".join(self._table_terms.terms_short())

    @staticmethod
    def _print_readme() -> str:
        with open(p.join(p.dirname(p.dirname(p.abspath(__file__))), "sources", "terms", "readme.txt"), "rb+") as fb:
            readme_text: bytes = fb.read()
        return readme_text.decode(encoding="utf8")

    @staticmethod
    def _print_samples():
        with open(p.join(p.dirname(p.dirname(p.abspath(__file__))), "sources", "terms", "samples.txt"), "rb+") as fb:
            samples_text: bytes = fb.read()
        return samples_text.decode(encoding="utf8")

    def handle_input(self, user_input: str = None):
        if user_input == "":
            return
        results: str | None
        _input_lower: str = user_input.lower()
        _input_strip: str = user_input.lower().strip()

        _specified_actions: dict[tuple[str, str], str] = {
            ("-f", "--full"): self._print_dictionary(),
            ("-a", "--all"): self._print_short(),
            ("-r", "--readme"): self._print_readme(),
            ("-s", "--samples"): self._print_samples(),
            ("-h", "--help"): self._print_help()
        }
        _actions: dict[str, str] = {_action: value for key, value in _specified_actions.items() for _action in key}

        if _input_strip in interceptors:
            logger.info("Программа остановлена пользователем")
            self.state = State.STOPPED
            return
        if _input_lower in _actions:
            _: str = _actions.get(_input_lower)
        else:
            _: str = self._handle_user_terms(_input_strip)
        results = "\n".join((_, separator))
        logger.info(f"\n{results}")

    def _terms(self) -> list[_Term]:
        return [v for values in iter(self) for v in values]

    @staticmethod
    def _parse_user_input(user_input: str) -> list[str] | None:
        if user_input:
            return [_.strip().upper() for _ in user_input.split()]
        return

    def _handle_user_terms(self, user_input: str = None) -> str:
        if user_input.startswith("--abbr"):
            _to_find: list[str] = self._parse_user_input(user_input[6:])
            _: list[str] = [term.abbr() for term in self._search_terms(_to_find)]
        else:
            _to_find: list[str] = self._parse_user_input(user_input)
            _: list[str] = [term.formatted() for term in self._search_terms(_to_find)]
        return "\n".join(_)

    def _search_terms(self, terms_find: Iterable[str]) -> tuple[_Term, ...]:
        results: list[_Term] = []
        for term in terms_find:
            if term not in self._table_terms.terms_short():
                results.append(_Term())
            else:
                results.extend(self._table_terms[term])
        return *results,

    def __iter__(self):
        return iter(self._table_terms.dict_terms.values())


@logger.catch
@complete_configure_custom_logging("terms")
def run_script():
    faulthandler.enable()

    content_git_page: ContentGitPage = ContentGitPage("terms.adoc")
    content_git_page.get_content()
    table: list[str] = content_git_page.content[6:-1]

    ascii_doc_table: AsciiDocTableTerms = AsciiDocTableTerms(table)
    ascii_doc_table.complete()
    ascii_doc_table.get_terms()

    user_input_parser: UserInputParser = UserInputParser(ascii_doc_table)
    prompt: str = "Введите сокращение или несколько сокращений, чтобы получить полное наименование и описание:\n"

    logger.info(_disclaimer)
    logger.info(_spec)
    logger.info(_help)
    logger.info(_exit)

    while user_input_parser.state.value != "stopped":
        user_input: str = input(prompt)
        user_input_parser.handle_input(user_input)

    input("Нажмите любую клавишу, чтобы закрыть окно ...")
    logger.stop()
    if faulthandler.is_enabled():
        faulthandler.disable()
