from pathlib import Path
from re import Pattern, compile, fullmatch, Match

from loguru import logger

from help_info.common_functions import run_faulthandler
from help_info.const_classes import StrPath
from help_info.init_logger import configure_custom_logging


class Glossary:
    def __init__(self, path: StrPath):
        self._path: Path = Path(path)
        self._content: list[str] = []

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def __str__(self):
        return "\n".join(self._content)

    def __iter__(self):
        return iter(self._content)

    def __len__(self):
        return len(self._content)

    def __getitem__(self, item):
        return self._content[item]

    def __setitem__(self, key, value):
        self._content[key] = value

    def process_lines(self):
        pattern: Pattern = compile(r"\|\s(.*)\s\|\s(.*)\s\|\s(.*)\s\|\s(.*)\s\|\n")
        for index, line in enumerate(iter(self)):
            content_line: ContentLine = ContentLine(line, pattern)
            if content_line:
                self._content[index] = str(content_line)
        return

    def read(self):
        with open(self._path, "r+", encoding="utf-8") as f:
            _ = f.readlines()
        self._content = _

    def write(self):
        with open(self._path, "w+", encoding="utf-8") as f:
            f.write("".join(self._content))


class ContentLine:
    def __init__(self, line: str, pattern: Pattern):
        self._line: str = line
        self._pattern: Pattern = pattern
        self._m: Match = fullmatch(self._pattern, self._line)

    def __bool__(self):
        return self._m is not None

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._line == other._line
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._line != other._line
        else:
            return NotImplemented

    def __hash__(self):
        return hash(self._line)

    @property
    def is_full(self):
        return self._m.group(2) != ""

    @property
    def is_rus(self):
        return self._m.group(3) != ""

    @property
    def is_comment(self):
        return self._m.group(4) != ""

    def __str__(self):
        logger.info(self._formatting_type())
        return f"{self._m.group(1)}::{self._formatting_description()}\n"

    def _multiplier(self, attr: str) -> int:
        value: str = getattr(self, attr)
        return int(value != "")

    def _formatting_type(self) -> int:
        """1 * bool(full) + 2 * bool(rus) + 4 * bool(commentary)"""
        _: dict[str, int] = {
            "is_full": 1,
            "is_rus": 2,
            "is_comment": 4
        }
        logger.info(self.is_full)
        logger.info(self.is_rus)
        logger.info(self.is_comment)
        return int(self.is_full) * 1 + int(self.is_rus) * 2 + int(self.is_comment) * 4

        # return sum(map(lambda x: self._multiplier(x) * _.get(x), _.keys()))

    def _formatting_description(self) -> str:
        _: dict[int, str] = {
            1: f"{self._m.group(2)}",
            2: f"{self._m.group(3)}",
            3: f"{self._m.group(2)}, {self._m.group(3)}",
            4: f"{self._m.group(4)}",
            5: f"{self._m.group(2)} — {self._m.group(4)}",
            6: f"{self._m.group(3)} — {self._m.group(4)}",
            7: f"{self._m.group(2)}, {self._m.group(3)} — {self._m.group(4)}"
        }
        return _.get(self._formatting_type(), "")


@logger.catch
@configure_custom_logging("glossary")
@run_faulthandler
def main():
    glossary_file: str = r"C:\Users\tarasov-a\PycharmProjects\help_info\sources\terms.md"
    glossary: Glossary = Glossary(glossary_file)
    glossary.read()
    # logger.info(list(iter(glossary)))
    glossary.process_lines()
    glossary.write()


if __name__ == '__main__':
    # a = True
    # b = False
    # print(int(a))
    # print(int(b))
    main()
