#!/usr/bin/python
# -*- coding: cp1251 -*-
import os.path as p
from importlib import import_module
from os import mkdir
from subprocess import CalledProcessError, run
from sys import executable, modules, version_info, exit


_parent_path: str = p.abspath(p.dirname(p.abspath(__file__)))


if version_info < (3, 8):
    print("The Python version must be at least 3.8.")
    input("Press <Enter> to close the window ...")
    exit(-1)


_packages: tuple[str, ...] = ("loguru",)

for _package in _packages:
    if _package not in modules:
        try:
            import_module(_package)
        except (ModuleNotFoundError, ImportError) as _e:
            try:
                run([executable, "-m", "pip", "install", _package]).check_returncode()
            except CalledProcessError as e:
                print(f"{e.__class__.__name__}\n��� ������ {e.returncode}\n������ {e.output}")
                print(f"�� ������� ������������� ����� `{_package}`")
                raise
            except OSError as e:
                print(f"{e.__class__.__name__}\n���� {e.filename}\n������ {e.strerror}")
                print(f"�� ������� ������������� ����� `{_package}`")
                raise

log_folder: str = p.abspath(p.join(_parent_path, "logs/"))

if not p.exists(log_folder):
    mkdir(log_folder)


if __name__ == '__main__':
    from terms import run_script
    run_script()

# if len(argv) == 2:
#     path: str | None = argv[1]
# else:
#     from gui import get_user_input
#     path: str | None = get_user_input()
#
# if not path:
#     print("The file has not been chosen.")
#     input("Press <Enter> to close the window ...")
#     exit(-2)
# abs_path: str = os.path.realpath(path)
# if not os.path.exists(abs_path):
#     print("The file has not been found.")
#     input("Press <Enter> to close the window ...")
#     exit(-3)
# from xml_file import CoreDocument, XmlDocument
# core_document: CoreDocument = CoreDocument(abs_path)
# core_document.unarchive()
# xml_document: XmlDocument = XmlDocument(core_document)
# xml_document.read()
# xml_document.parse_document()
# core_document.delete_temp_archive()
# print(f"Markdown tables are located in {xml_document.table_dir}")
# input("Press <Enter> to close the window ...")
# exit(0)
