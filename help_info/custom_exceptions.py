class IpAddressFewOctetsError(ValueError):
    """IP address has too few octets."""


class IpAddressManyOctetsError(ValueError):
    """IP address has too many octets."""


class IpAddressOctetOutboundsError(ValueError):
    """Octet is out of range (0, 255)."""


class ProductNoNameError(ValueError):
    """Product does not have a specified name."""


class InvalidIpAddressError(ValueError):
    """IP address has an invalid format."""


class ProductInfoNewInvalidTypeError(TypeError):
    """New item has an invalid type."""


class JSONFileAttributeMissingError(AttributeError):
    """JSON file does not have a mandatory attribute."""


class JSONFileAttributeInvalidPatternError(ValueError):
    """JSON file does not have an appropriate pattern."""


class AsciiDocFilePartNotationError(ValueError):
    """Document part notation is not found."""


class AsciiDocFileTableItemTypeError(TypeError):
    """Invalid AsciiDocTable item value."""


class AsciiDocFileTableKeyError(KeyError):
    """Invalid AsciiDocTable item value."""


class MarkdownFileNotFoundError(FileNotFoundError):
    """The path does not lead to the Markdown file."""


class AsciiDocFileNotFoundError(FileNotFoundError):
    """The path does not lead to the AsciiDoc file."""


class CommandNameKeyError(KeyError):
    """The command with the name is not found."""


class CommandNameTypeError(TypeError):
    """The command name has an invalid type."""


class CommandTypeError(TypeError):
    """The command has an invalid type."""


class CommandArgumentNameError(KeyError):
    """The command does not have a field with such name."""


class AsciiDocFileTableRowIndexError(KeyError):
    """Invalid AsciiDocTable row index."""


class AsciiDocFileTableColumnIndexError(KeyError):
    """Invalid AsciiDocTable row index."""


class ImportLibraryDependencyFailed(ImportError):
    """Dependency lib failed to import."""


class RequiredAttributeMissing(Exception):
    """Required attribute has no specified value."""


class UserInterruptedError(InterruptedError):
    """Program is stopped by the user."""


class NotEnoughArgumentsError(ValueError):
    """Command is missing arguments"""
