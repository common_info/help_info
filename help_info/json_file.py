import json
from pathlib import Path

from common_functions import JsonDict


class JSONFile:
    def __init__(self, path: str | Path):
        if isinstance(path, str):
            path: Path = Path(path).resolve()
        self._path: Path = path
        self._content: list[JsonDict] | None = None

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def __str__(self):
        return "\n".join([_ for _ in self._content])

    def get_content(self):
        self._content = self.reader.read()

    def __iter__(self):
        return iter(self._content)

    @property
    def reader(self):
        return _JSONFileReader(self._path)

    @property
    def writer(self):
        return _JSONFileWriter(self._path)


class _JSONFileReader(JSONFile):
    def read(self):
        with open(self._path, "r+") as f:
            _: str = f.read()
        return json.loads(_)


class _JSONFileWriter(JSONFile):
    def write(self):
        with open(self._path, "w+") as f:
            _: str = json.dumps(self._content)
            f.write(_)
