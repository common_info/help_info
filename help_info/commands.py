from typing import Any, Callable, Iterable, Iterator

from loguru import logger

from help_info.common_functions import get_name, get_qualname
from help_info.custom_exceptions import CommandNameKeyError, CommandNameTypeError


class CommandList:
    def __init__(self, commands: Iterable[Callable] = None):
        if commands is None:
            commands: list[Callable] = []
        self._command_list: dict[str, Callable] = {get_name(k): k for k in commands}

    def __repr__(self):
        _names: str = ", ".join(self.names())
        return f"<{self.__class__.__name__}({_names})>"

    __str__ = __repr__

    def __len__(self):
        return len(self._command_list)

    def __bool__(self):
        return len(self) != 0

    def __getitem__(self, item) -> Callable:
        if not isinstance(item, str):
            logger.error(f"Key {item} must be str but {type(item)} received")
            raise CommandNameTypeError
        if item not in self.names:
            logger.error(f"Key {item} is not found")
            raise CommandNameKeyError
        return self._command_list.get(item)

    def __setitem__(self, key, value):
        if not isinstance(key, str):
            logger.error(f"Key {key} must be str but {type(key)} received")
            raise CommandNameTypeError
        if not isinstance(value, Callable):
            logger.error(f"Value {value} must be Callable but {type(key)} received")
        self._command_list[key] = value

    def names(self) -> list[str]:
        return list(self.iter_names())

    def call_command(self, name: str, *args, **kwargs):
        try:
            func: Callable = self[name]
            _ = func(*args, **kwargs)
        except (CommandNameKeyError, CommandNameTypeError) as e:
            logger.error(f"{e.__class__.__name__}, {str(e)}")
        except (AttributeError, NameError, RecursionError, OSError, RuntimeError) as e:
            logger.error(f"{e.__class__.__name__}, {str(e)}")
        else:
            return _

    def __iter__(self):
        return NotImplemented

    @property
    def commands(self):
        return list(self.iter_commands())

    def iter_names(self) -> Iterator[str]:
        return iter(self._command_list.keys())

    def iter_commands(self) -> Iterator[Callable]:
        return iter(self._command_list.values())

    def __add__(self, other):
        def add(item: Any, _success: Iterable[Callable] = None, _fail: Iterable[Any] = None):
            if _success is None:
                _success: list[Callable] = []
            if _fail is None:
                _fail: list[Any] = []
            _: str | None = get_qualname(item)
            if _ is None or _ not in self.names():
                _fail.append(str(item))
            else:
                _success.append(item)
            return

        if isinstance(other, Iterable):
            success = []
            fail = []
            for el in other:
                add(el, success, fail)
            for command in success:
                self[get_qualname(command)] = command
                logger.info(f"Command {get_name(command)} is added")
            for obj in fail:
                logger.info(f"Command {str(obj)} is omitted")

        elif isinstance(other, Callable):
            if get_qualname(other) not in self.names:
                self[get_qualname(other)] = other
            else:
                logger.info(f"Command {other} is already listed")

        else:
            logger.error(f"Command {other} must be Callable or Iterable[Callable] but {type(other)}")
        return

    __iadd__ = __add__
    __radd__ = __add__

    def __sub__(self, other):
        def _delete(_key: str):
            if _key in self.names():
                self._command_list.pop(_key)
            else:
                logger.info(f"Command {_key} is not found")
            return
        if isinstance(other, Callable):
            key: str = get_qualname(other)
        elif isinstance(other, str):
            key: str = other
        else:
            logger.info(f"Deleted item {other} must be str or Callable but {type(other)} received")
            return
        return _delete(key)

    __isub__ = __sub__

    def print_names(self):
        _names: str = ", ".join(self.names())
        logger.info(f"Commands:\n{_names}")


cmd_list: CommandList = CommandList()


class Command:
    _command_list: CommandList | None = None

    def __init__(self, func: Callable):
        self._func: Callable = func.__qualname__

    def __repr__(self):
        return f"{self.__class__.__name__}: {get_name(self._func)}"

    __str__ = __repr__

    def __call__(self, *args, **kwargs):
        if self.__class__._command_list is None:
            self.__class__._command_list = CommandList()
        self.__class__._command_list += self._func
        return self._func(*args, **kwargs)
