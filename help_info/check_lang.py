from loguru import logger

from help_info.general_file import GeneralFile
from help_info.init_logger import complete_configure_custom_logging


class LanguageFinder(GeneralFile):
    def find_lang(self):
        _indexes: list[int] = []
        for index, line in enumerate(self.__iter__()):
            if not line.isascii():
                _indexes.append(index + 1)
                continue
        if _indexes:
            logger.info("Russian letters are found")
            logger.info(_indexes)
        else:
            logger.info("No Russian letters are found")
        return


@complete_configure_custom_logging("check_lang")
def main():
    file_path: str = r"C:\Users\tarasov-a\PycharmProjects\making_docs\http\http.en.md"
    finder: LanguageFinder = LanguageFinder(file_path)
    finder.read()
    finder.find_lang()


if __name__ == '__main__':
    main()
