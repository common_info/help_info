from pathlib import Path
from re import Match, Pattern, compile, findall, fullmatch
from typing import Iterable, NamedTuple, TypeAlias

from loguru import logger

StrPath: TypeAlias = str | Path
StrNone: TypeAlias = str | None


class ParserMatchGroupIndexError(KeyError):
    """Invalid match group index."""


class MultiParserMatchGroupIndexError(KeyError):
    """Invalid match group index."""


class ParserMatchNotFoundError(ValueError):
    """Parser has not found the specified pattern in the line."""


class ParserIndexTypeError(TypeError):
    """Invalid parser index type."""


class MultiParserPatternError(ValueError):
    """Some line does not match the pattern."""


class MultiParserIndexTypeError(TypeError):
    """Invalid parser index type."""


class SubParserMatchNotFoundError(ValueError):
    """Parser has not found the specified pattern in the subline."""


class ListLevelMatchError(ValueError):
    """Line is not an item of a list."""


class Char(str):
    def __new__(cls, value):
        if len(str(value)) != 1:
            raise ValueError("Char must only a single character.")
        char = super().__new__(cls, value)
        return char

    def __init__(self, value):
        if isinstance(value, str):
            self.value = value
        else:
            self.value = str(value)

    def __str__(self):
        return f"{self.value}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.value})>"


class Header(NamedTuple):
    level: int
    name: str

    def __str__(self):
        _marker: str = "=" * self.level
        return f"{_marker} {self.name}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.level}, {self.name})>"


class CrossReference(NamedTuple):
    link: str
    element: StrNone
    text: StrNone

    def __str__(self):
        _element: str = f"#{self.element}" if self.element is not None else ""
        _text: str = f",{self.text}" if self.text is not None else ""
        return f"<<{self.link}{_element}{_text}>>"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.link}, {self.element}, {self.text})>"


class Image(NamedTuple):
    link: str
    text: StrNone

    def __str__(self):
        _text: str = f"[{self.text}]" if self.text is not None else ""
        return f"image:{self.link}{_text}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.link}, {self.text})>"


class Link(NamedTuple):
    link: str
    text: StrNone

    def __str__(self):
        _text: str = f"[{self.text}]" if self.text is not None else ""
        return f"link:{self.link}{_text}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.link}, {self.text})>"


class Table(NamedTuple):
    header: str
    items: list[str]

    def __str__(self):
        _header: str = self.header[:-1]
        _items: list[str] = [_[:-1] for _ in self.items]
        _lines: str = "\n".join(_items)
        return f"|===\n\n{_header}\n\n{_lines}\n|==="

    def __repr__(self):
        _items: str = "\n".join(self.items)
        return f"<{self.__class__.__name__}({self.header}, {_items})>"


class UnorderedListItem(NamedTuple):
    line: str
    level: int

    def __str__(self):
        return f"{Char('*') * self.level} {self.line}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.line}, {self.level})>"


class UnorderedList(NamedTuple):
    items: Iterable[UnorderedListItem]

    def __iter__(self):
        return iter(self.items)

    def __str__(self):
        return "\n".join(str(item) for item in iter(self))

    def __repr__(self):
        return f"<{self.__class__.__name__}(\n{str(self)}\n)>"


# def validate_int()


class OrderedListItem(NamedTuple):
    line: str
    order_number: str

    def __str__(self):
        return f"{self.order_number}. {self.line}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.line}, {self.order_number})>"

    def number(self):
        return tuple(item.strip() for item in self.order_number.split(".") if item.strip())


class OrderedList(NamedTuple):
    items: Iterable[OrderedListItem]

    def __str__(self):
        return "\n".join(str(item) for item in self.items)

    def __repr__(self):
        return f"<{self.__class__.__name__}({str(self)})>"

    # def sorted(self):



class GeneralFile:
    def __init__(self, path: StrPath):
        self._path: Path = Path(path).resolve()
        self._content: list[str] = []
        self._validate()

    def _validate(self):
        if not self._path.exists():
            logger.error(f"Path {self._path} does not exist")
            raise FileNotFoundError
        if not self._path.is_file():
            logger.error(f"Path {self._path} does not point to the file")
            raise IsADirectoryError
        return

    def __str__(self):
        return "\n".join(self._content)

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def read(self):
        with open(self._path, "r+") as f:
            _: list[str] = f.readlines()
        self._content = _

    def save(self):
        with open(self._path, "w+") as f:
            f.write(str(self))

    def __iter__(self):
        return iter(self._content)

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self._content
        return False

    def __getitem__(self, item):
        return self._content[item]

    def __setitem__(self, key, value):
        self._content[key] = value

    def __len__(self):
        return len(self._content)

    def __bool__(self):
        return len(self) != 0

    @property
    def empty(self):
        return [_ for _ in self._content if not _]

    @property
    def non_empty(self):
        return [_ for _ in self._content if _]

    @property
    def name(self):
        return self._path.name

    @property
    def content(self):
        return self._content

    @content.setter
    def content(self, value):
        self._content = value


class MultilineParser:
    def __init__(self, marker: Char, pattern: Pattern, lines: Iterable[str] = None):
        if lines is None:
            lines: list[str] = []
        self._marker: Char = marker
        self._pattern: Pattern = pattern
        self._lines: list[str] = [*lines]
        self._validate()

    def _validate(self):
        if any(_m is None for _m in self.matches):
            logger.error(f"Не все строки удовлетворяют шаблону {self._pattern.pattern}")
            raise MultiParserPatternError
        return

    def __str__(self):
        _lines: str = "\n".join(self._lines)
        return f"{_lines}"

    def __repr__(self):
        return f"<{self.__class__.__name__}>"

    def __iter__(self):
        return iter(self._lines)

    def iter_lines(self, *, index_from: int = 0, index_to: int = -1):
        if index_to == -1:
            index_to: int = len(self)
        return iter(self._lines[index_from:index_to])

    def iter_items(self):
        return self.iter_lines(index_from=2)

    def __getitem__(self, item):
        if isinstance(item, int):
            if item >= len(self):
                logger.error(f"Индекс {item} больше, чем количество групп {len(self)}")
                raise ParserMatchGroupIndexError
            return self._lines[item]
        else:
            logger.error(f"Некорректный ключ {item} типа {type(item)}")
            raise MultiParserIndexTypeError

    def __len__(self):
        return len(self._lines)

    @property
    def pattern(self):
        return self._pattern

    @pattern.setter
    def pattern(self, value):
        self._pattern = value

    @property
    def matches(self):
        return [fullmatch(self._pattern, line) for line in self.iter_items()]

    def parse(self):
        raise NotImplementedError


class LineParser:
    def __init__(self, line: str, pattern: Pattern):
        self._line: str = line
        self._pattern: Pattern = pattern
        self._validate()

    def __str__(self):
        return f"{self._line}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._line}, {self._pattern.pattern})>"

    def _validate(self):
        if self.match is None:
            logger.error(f"В строке {self._line} не найден шаблон {self._pattern.pattern}")
            raise ParserMatchNotFoundError
        return

    @property
    def pattern(self):
        return self._pattern

    @pattern.setter
    def pattern(self, value):
        self._pattern = value

    @property
    def match(self) -> Match:
        return fullmatch(self._pattern, self._line)

    def __len__(self):
        return len(self.match.groups())

    def __getitem__(self, item):
        if isinstance(item, int):
            if item >= len(self):
                logger.error(f"Индекс {item} больше, чем количество групп {len(self)}")
                raise ParserMatchGroupIndexError
            return self.match.group(item)
        else:
            logger.error(f"Некорректный ключ {item} типа {type(item)}")
            raise ParserIndexTypeError

    def __iter__(self):
        return iter(self.match.groups())

    @property
    def line(self):
        return self._line

    @line.setter
    def line(self, value):
        self._line = value

    def parse(self):
        raise NotImplementedError


class SubLineParser:
    def __init__(self, line: str, pattern: Pattern):
        self._line: str = line
        self._pattern: Pattern = pattern
        self._validate()

    def __str__(self):
        return f"{self._line}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._line}, {self._pattern.pattern})>"

    @property
    def matches(self):
        return findall(self._pattern, self._line)

    def _validate(self):
        if self.matches is None:
            logger.error(f"В строке {self._line} не найден шаблон {self._pattern.pattern}")
            raise SubParserMatchNotFoundError
        return

    def __len__(self):
        return len(self.matches)

    @property
    def line(self):
        return self._line

    @line.setter
    def line(self, value):
        self._line = value

    @property
    def pattern(self):
        return self._pattern

    @pattern.setter
    def pattern(self, value):
        self._pattern = value

    def __iter__(self):
        return iter(self.matches)

    def __getitem__(self, item):
        return self.matches[item]

    def parse(self):
        raise NotImplementedError


class TableParser(MultilineParser):
    def __init__(self, lines: Iterable[str] = None):
        marker: Char = Char("|")
        pattern: Pattern = compile(r"((?:\|\s?[\wА-Яа-я,.;()<>=*/\s]+\s?)+)\|*")
        super().__init__(marker, pattern, lines)

    def parse(self):
        _: list[str] = [f"{self._marker}{item.strip()}" for item in self._lines[0].split(self._marker)]
        header: str = " ".join(_)
        items: list[str] = [line.strip(self._marker).strip() for line in self.iter_lines(index_from=2)]
        return Table(header, items)


class HeaderParser(LineParser):
    def __init__(self, line: str):
        pattern: Pattern = compile(r"(#+)\s(.*)\s?(#?)")
        super().__init__(line, pattern)

    def parse(self) -> Header:
        level: int = len(self[1])
        name: str = self[2]
        return Header(level, name)


class ImageParser(LineParser):
    def __init__(self, line: str):
        pattern: Pattern = compile(r"!\[(.*)]\((.*)\)")
        super().__init__(line, pattern)

    def parse(self) -> Image:
        text: str = self[1]
        link: str = self[2]
        return Image(link, text)


class LinkParser(LineParser):
    def __init__(self, line: str):
        pattern: Pattern = compile(r"\[(.*)]\((.*)\)")
        super().__init__(line, pattern)

    def parse(self) -> Link:
        text: str = self[1]
        link: str = self[2]
        return Link(link, text)


class UnorderedListParser(MultilineParser):
    def __init__(self, lines: Iterable[str] = None):
        marker: Char = Char("*")
        pattern: Pattern = compile(r"\s*\*\s(.*)[.;,]?")
        super().__init__(marker, pattern, lines)

    def parse(self):
        def _level_indent(_line: str):
            pattern: Pattern = compile(rf"(\s*)\S\s(.*)([.,;]?)")
            _m: Match = fullmatch(pattern, _line)
            level: int = len(_m.group(1))
            line: str = f"{_m.group(2)}{_m.group(3)}"
            return line, level

        items: list[UnorderedListItem] = [
            UnorderedListItem(k, v) for line in iter(self)
            for k, v in _level_indent(line)]
        return UnorderedList(items)
