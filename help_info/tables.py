from __future__ import annotations

import faulthandler
import os.path as p
from tempfile import NamedTemporaryFile
from typing import Iterable

from loguru import logger

from ascii_doc import AsciiDocDocument, AsciiDocDocumentTable, AsciiDocFile, AsciiDocTable
from common_functions import StrPath
from const_classes import AVPItem, BinReleaseCause, ContentGitPage, DiamType, HexReleaseCause, LocalZoneProduct, \
    LogFile, NetworkFunction, PartyCategory
from help_info.commands import Command
from init_logger import complete_configure_custom_logging


class FileTableGit:
    _command_name_table = {
        "avp.adoc": "avp",
        "cgpc.adoc": "cgpc",
        "diam_types.adoc": "diam_types",
        "emm_causes.adoc": "emm_release_causes",
        "esm_causes.adoc": "esm_release_causes",
        "isdn_causes": "hex_release_causes",
        "networks_functions.adoc": "nf",
        "zones.adoc": "zones"
    }

    def __init__(self, file_name: str, table_index: int = 0):
        self._file_name: str = file_name
        self._table_index: int = table_index
        self._content: list[str] = []
        self._temp_file = None

    def __str__(self):
        return "\n".join(self._content)

    def get_git_page(self):
        _git_page: ContentGitPage = ContentGitPage(self._file_name)
        _git_page.get_content()
        logger.info(_git_page.content)
        _indexes: list[int] = [
            index for index, line in enumerate(iter(_git_page)) if line == "|==="]
        logger.info(f"_indexes = {_indexes}")
        _from: int = 2 * self._table_index
        _to: int = 2 * self._table_index + 1
        _from_index: int = _indexes[_from]
        _to_index: int = _indexes[_to]
        logger.info(f"{_from_index}, {_to_index}")
        self._content.extend(_git_page.content[_from_index:_to_index + 1])

    def get_temp_file(self):
        self._temp_file = NamedTemporaryFile(
            mode="w+",
            dir=p.abspath(p.dirname(p.abspath(__file__))),
            suffix=".adoc",
            delete=False)
        logger.info(self._content)

        with self._temp_file as f:
            f.write(str(self))

    @property
    def table_file(self) -> TableFile:
        logger.info(self._temp_file.name)
        return TableFile(self._temp_file.name, self._command_name_table[self._file_name])

    def close(self):
        if not self._temp_file:
            self._temp_file.close()
            self._temp_file = None

    @property
    def path(self):
        return self._temp_file.name

    @property
    def temp_file(self):
        return self._temp_file


class TableGitPage(AsciiDocTable):
    def __init__(self, command: str, lines: Iterable[str] = None, ):
        super().__init__(lines)
        self._command: str = command

    @classmethod
    def from_temp_file(cls, file_table_git: FileTableGit, command: str):
        with file_table_git.temp_file as f:
            lines: list[str] = f.readlines()
        file_table_git.close()
        return cls(command, lines)

    @property
    def items(self):
        raise NotImplementedError

    def __iter__(self):
        return iter(self.items)

    def __getitem__(self, item):
        raise NotImplementedError

    def all(self) -> str:
        return "\n".join([str(_) for _ in self.items])

    def __call__(self, *args, **kwargs):
        if self._command in args:
            if "-a" in args or "--a" in args:
                return self.all()
            else:
                logger.error("No command is found")
        else:
            logger.error("No command is found")


class TableFile(AsciiDocDocumentTable):
    def __init__(self, path: StrPath, command: str):
        ascii_file: AsciiDocFile = AsciiDocFile(path)
        ascii_document: AsciiDocDocument = AsciiDocDocument(ascii_file)
        ascii_document.set_content()
        super().__init__(ascii_document, 0)
        self._command: str = command
        self.complete()
        self._commands = dict()

    @property
    def items(self):
        raise NotImplementedError

    def __iter__(self):
        return iter(self.items)

    def __getitem__(self, item):
        raise NotImplementedError

    def all(self) -> str:
        return "\n".join([str(_) for _ in self.items])

    def __call__(self, *args, **kwargs):
        if self._command in args:
            if "-a" in args or "--a" in args:
                return self.all()
            else:
                logger.error("No command is found")
        else:
            logger.error("No command is found")


class HexReleaseCauseFile(TableFile):
    def __init__(self, path: StrPath):
        command: str = "hex_release_causes"
        super().__init__(path, command)

    @property
    def items(self) -> tuple[BinReleaseCause, ...]:
        return tuple(
            BinReleaseCause.from_string(*self.get_row_items(row_index))
            for row_index in range(self.max_row))

    @property
    def _dec_codes(self) -> tuple[int, ...]:
        _: BinReleaseCause
        return tuple(_.dec_code for _ in iter(self))

    def _bin_codes(self) -> tuple[hex, ...]:
        _: BinReleaseCause
        return tuple(_.bin_code for _ in iter(self))

    def _find_dec_code(self, dec_code: int) -> BinReleaseCause | None:
        for _ in self.items:
            if _.dec_code == dec_code:
                return _
        logger.info(f"Decimal code {dec_code} is not found")
        return

    def _find_bin_code(self, bin_code: str) -> BinReleaseCause | None:
        for _ in self.items:
            if _.bin_code == f"0b{int(bin_code, 2)}":
                return _
        logger.info(f"Bin code {bin_code} is not found")
        return

    def __getitem__(self, item):
        if isinstance(item, int):
            return self._find_dec_code(item)
        elif isinstance(item, str):
            return self._find_bin_code(item)
        else:
            return NotImplemented

    @Command
    def get_bin_code(self, item: hex) -> str | None:
        try:
            return str(self[item])
        except (KeyError, ValueError, RuntimeError):
            return

    @Command
    def get_dec_code(self, item: int) -> str | None:
        try:
            return str(self[item])
        except (KeyError, ValueError, RuntimeError):
            return


class EsmReleaseCauseFile(TableFile):
    def __init__(self, path: StrPath):
        command: str = "esm_release_causes"
        super().__init__(path, command)

    @property
    def items(self) -> tuple[HexReleaseCause, ...]:
        return tuple(
            HexReleaseCause.from_string(*self.get_row_items(row_index))
            for row_index in range(self.max_row))

    @property
    def _dec_codes(self) -> tuple[int, ...]:
        _: HexReleaseCause
        return tuple(_.dec_code for _ in iter(self))

    def _hex_codes(self) -> tuple[hex, ...]:
        _: HexReleaseCause
        return tuple(_.hex_code for _ in iter(self))

    def _find_dec_code(self, dec_code: int) -> HexReleaseCause | None:
        for _ in self.items:
            if _.dec_code == dec_code:
                return _
        logger.info(f"Decimal code {dec_code} is not found")
        return

    def _find_hex_code(self, hex_code: str) -> HexReleaseCause | None:
        for _ in self.items:
            if _.hex_code == hex(int(hex_code, 16)):
                return _
        logger.info(f"Hex code {hex_code} is not found")
        return

    def __getitem__(self, item):
        if isinstance(item, int):
            return self._find_dec_code(item)
        elif isinstance(item, str):
            return self._find_hex_code(item)
        else:
            return NotImplemented

    @Command
    def get_hex_code(self, item: hex) -> str | None:
        try:
            return str(self[item])
        except (KeyError, ValueError, RuntimeError):
            return

    @Command
    def get_dec_code(self, item: int):
        try:
            return str(self[item])
        except (KeyError, ValueError, RuntimeError):
            return


class EmmReleaseCauseFile(TableFile):
    def __init__(self, path: StrPath):
        command: str = "emm_release_causes"
        super().__init__(path, command)

    @property
    def items(self) -> tuple[HexReleaseCause, ...]:
        return tuple(
            HexReleaseCause.from_string(*self.get_row_items(row_index))
            for row_index in range(self.max_row))

    @property
    def _dec_codes(self) -> tuple[int, ...]:
        _: HexReleaseCause
        return tuple(_.dec_code for _ in iter(self))

    def _hex_codes(self) -> tuple[hex, ...]:
        _: HexReleaseCause
        return tuple(_.hex_code for _ in iter(self))

    def _find_dec_code(self, dec_code: int) -> HexReleaseCause | None:
        for _ in self.items:
            if _.dec_code == dec_code:
                return _
        logger.info(f"Decimal code {dec_code} is not found")
        return

    def _find_hex_code(self, hex_code: str) -> HexReleaseCause | None:
        for _ in self.items:
            if _.hex_code == hex(int(hex_code, 16)):
                return _
        logger.info(f"Hex code {hex_code} is not found")
        return

    def __getitem__(self, item):
        if isinstance(item, int):
            return self._find_dec_code(item)
        elif isinstance(item, str):
            return self._find_hex_code(item)
        else:
            return NotImplemented

    @Command
    def get_hex_code(self, item: hex) -> str | None:
        try:
            return str(self[item])
        except (KeyError, ValueError, RuntimeError):
            return

    @Command
    def get_dec_code(self, item: int):
        try:
            return str(self[item])
        except (KeyError, ValueError, RuntimeError):
            return


class LocalZonesFile(TableFile):
    def __init__(self, path: StrPath):
        command: str = "zones"
        super().__init__(path, command)

    @property
    def items(self) -> tuple[LocalZoneProduct, ...]:
        return tuple(
            LocalZoneProduct.from_string(*self.get_row_items(row_index))
            for row_index in range(self.max_row))

    @property
    def _names(self) -> frozenset[str]:
        _: LocalZoneProduct
        return frozenset(_.name for _ in iter(self))

    def get_names(self) -> str:
        return ", ".join(self._names)

    def __getitem__(self, item):
        if isinstance(item, str):
            if item in self._names:
                return list(filter(lambda x: x.name.find(item) != -1, iter(self)))
            else:
                logger.info(f"Product {item} is not found")
                return
        elif isinstance(item, (int, slice)):
            try:
                return self.items[item]
            except (ValueError, KeyError, RuntimeError) as e:
                logger.info(f"{e.__class__.__name}, {e.args}")
                return
        else:
            return NotImplemented

    def _product(self, name: str) -> list[str]:
        try:
            return [str(_) for _ in self[name]]
        except (KeyError, ValueError, RuntimeError) as e:
            logger.info(f"{e.__class__.__name}, {e.args}")
            raise

    @Command
    def get_product(self, name: str) -> str | None:
        try:
            return "\n".join(self._product(name))
        except (KeyError, ValueError, RuntimeError):
            return


class AVPFile(TableFile):
    def __init__(self, path: StrPath):
        command: str = "avp"
        super().__init__(path, command)

    @property
    def items(self) -> tuple[AVPItem, ...]:
        return tuple(
            AVPItem.from_string(*self.get_row_items(row_index))
            for row_index in range(self.max_row))

    @property
    def _names(self) -> tuple[str, ...]:
        _: AVPItem
        return tuple(_.attribute_name for _ in iter(self))

    @property
    def _codes(self) -> tuple[int, ...]:
        _: AVPItem
        return tuple(_.avp_code for _ in iter(self))

    def _find_code(self, code: int) -> AVPItem | None:
        for _ in iter(self):
            if _.avp_code == code:
                return _
        logger.info(f"Code {code} is not found")
        return

    def get_names(self) -> str:
        return ", ".join(self._names)

    def __getitem__(self, item):
        if isinstance(item, str):
            if item in self._names:
                return list(filter(lambda x: x.attribute_name.find(item) != -1, iter(self)))
            else:
                logger.info(f"AVP {item} is not found")
                return
        elif isinstance(item, int):
            return self._find_code(item)
        else:
            return NotImplemented

    def _attribute_name(self, name: str) -> list[str]:
        try:
            return [str(_) for _ in self[name]]
        except (KeyError, ValueError, RuntimeError) as e:
            logger.info(f"{e.__class__.__name}, {e.args}")
            raise

    @Command
    def get_attribute_name(self, name: str) -> str | None:
        try:
            return "\n".join(self._attribute_name(name))
        except (KeyError, ValueError, RuntimeError):
            return

    @Command
    def get_avp_code(self, code: int) -> str | None:
        try:
            return str(self._find_code(code))
        except (KeyError, ValueError, RuntimeError):
            return


class CategoryFile(TableFile):
    def __init__(self, path: StrPath, ):
        command: str = "cgpc"
        super().__init__(path, command)

    @property
    def items(self) -> tuple[PartyCategory, ...]:
        return tuple(
            PartyCategory.from_string(*self.get_row_items(row_index))
            for row_index in range(self.max_row))

    @property
    def _categories(self) -> tuple[int, ...]:
        _: PartyCategory
        return tuple(_.category for _ in iter(self))

    def _codes(self) -> tuple[int, ...]:
        _: PartyCategory
        return tuple(_.code for _ in iter(self))

    def _find_category(self, category: int) -> PartyCategory | None:
        for _ in self.items:
            if _.category == category:
                return _
        logger.info(f"Category {category} is not found")
        return

    def _find_code(self, code: int) -> PartyCategory | None:
        for _ in self.items:
            if _.code == code:
                return _
        logger.info(f"Code {code} is not found")
        return

    def __getitem__(self, item):
        if isinstance(item, int):
            if item < 10:
                return self._find_code(item)
            else:
                return self._find_category(item)
        else:
            return NotImplemented

    @Command
    def get_cgpc(self, item: int) -> str | None:
        try:
            return str(self[item])
        except (KeyError, ValueError, RuntimeError):
            return


class DiameterTypesFile(TableFile):
    def __init__(self, path: StrPath):
        command: str = "diam_types"
        super().__init__(path, command)

    @property
    def items(self) -> tuple[DiamType, ...]:
        return tuple(
            DiamType(*self.get_row_items(row_index))
            for row_index in range(self.max_row))

    @property
    def _names(self) -> tuple[str, ...]:
        _: DiamType
        return tuple(_.name for _ in iter(self))

    def get_names(self) -> str:
        return ", ".join(self._names)

    def __getitem__(self, item):
        if isinstance(item, str):
            if item in self._names:
                return list(filter(lambda x: x.name.find(item) != -1, iter(self)))
            else:
                logger.info(f"Diameter type {item} is not found")
                return
        elif isinstance(item, (int, slice)):
            try:
                return self.items[item]
            except (ValueError, KeyError, RuntimeError) as e:
                logger.info(f"{e.__class__.__name}, {e.args}")
                return
        else:
            return NotImplemented

    def _diam_type(self, name: str) -> list[str]:
        try:
            return [str(_) for _ in self[name]]
        except (KeyError, ValueError, RuntimeError) as e:
            logger.info(f"{e.__class__.__name}, {e.args}")
            raise

    @Command
    def get_diam_type(self, name: str) -> str | None:
        try:
            return "\n".join(self._diam_type(name))
        except (KeyError, ValueError, RuntimeError):
            return


class LogNamesFile(TableFile):
    def __init__(self, path: StrPath):
        command: str = "logs"
        super().__init__(path, command)

    @property
    def items(self) -> tuple[LogFile, ...]:
        return tuple(
            LogFile(*self.get_row_items(row_index))
            for row_index in range(self.max_row))

    @property
    def _names(self) -> tuple[str, ...]:
        _: LogFile
        return tuple(_.name for _ in iter(self))

    def get_names(self) -> str:
        return ", ".join(self._names)

    def __getitem__(self, item):
        if isinstance(item, str):
            if item in self._names:
                return list(filter(lambda x: x.name.find(item) != -1, iter(self)))
            else:
                logger.info(f"Log file {item} is not found")
                return
        elif isinstance(item, (int, slice)):
            try:
                return self.items[item]
            except (ValueError, KeyError, RuntimeError) as e:
                logger.info(f"{e.__class__.__name}, {e.args}")
                return
        else:
            return NotImplemented

    def _log_file(self, name: str) -> list[str]:
        try:
            return [str(_) for _ in self[name]]
        except (KeyError, ValueError, RuntimeError) as e:
            logger.info(f"{e.__class__.__name}, {e.args}")
            raise

    @Command
    def get_log_file(self, name: str) -> str | None:
        try:
            return "\n".join(self._log_file(name))
        except (KeyError, ValueError, RuntimeError):
            return


class NetworkFunctionsFile(TableFile):
    def __init__(self, path: StrPath):
        command: str = "nf"
        super().__init__(path, command)

    @property
    def items(self) -> tuple[NetworkFunction, ...]:
        return tuple(
            NetworkFunction.from_string(*self.get_row_items(row_index))
            for row_index in range(self.max_row))

    @property
    def _names(self) -> tuple[str, ...]:
        _: NetworkFunction
        return tuple(_.nf_name for _ in iter(self))

    def get_names(self) -> str:
        return ", ".join(self._names)

    def __getitem__(self, item):
        if isinstance(item, str):
            if item in self._names:
                return list(filter(lambda x: x.nf_name.find(item) != -1, iter(self)))
            else:
                logger.info(f"Network function {item} is not found")
                return
        elif isinstance(item, (int, slice)):
            try:
                return self.items[item]
            except (ValueError, KeyError, RuntimeError) as e:
                logger.info(f"{e.__class__.__name}, {e.args}")
                return
        else:
            return NotImplemented

    def _nf(self, name: str) -> list[str]:
        try:
            return [str(_) for _ in self[name]]
        except (KeyError, ValueError, RuntimeError) as e:
            logger.info(f"{e.__class__.__name}, {e.args}")
            raise

    @Command
    def get_nf(self, name: str) -> str | None:
        try:
            return "\n".join(self._nf(name))
        except (KeyError, ValueError, RuntimeError):
            return


@logger.catch
@complete_configure_custom_logging("tables")
def main():
    file_table_git: FileTableGit = FileTableGit("esm_causes.adoc")
    file_table_git.get_git_page()
    file_table_git.get_temp_file()
    table_file: EsmReleaseCauseFile = EsmReleaseCauseFile(file_table_git.path)
    # table_file.complete()
    # logger.info(table_file.items)
    logger.info(Command._command_list)


if __name__ == "__main__":
    faulthandler.enable()
    main()
    if faulthandler.is_enabled():
        faulthandler.disable()
