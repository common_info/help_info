from typing import TypeAlias, Sequence, NamedTuple

from help_info.common_functions import run_faulthandler
from help_info.general_file import GeneralFile
from help_info.init_logger import configure_custom_logging

StrNone: TypeAlias = str | None


class ItemModifier(NamedTuple):
    prefix: str | None
    suffix: str | None

    def __str__(self):
        return f"{self.__class__.__name__}: {self.prefix}<>{self.suffix}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.prefix}, {self.suffix})>"

    def __bool__(self):
        return (self.prefix, self.suffix) != (None, None)

    def modify(self, value: str):
        return f"{self.prefix}{value.strip()}{self.suffix}"


class ItemJoin:
    def __init__(self, first_modifier: ItemModifier, second_modifier: ItemModifier, separator: str = " "):
        self._first_modifier: ItemModifier = first_modifier
        self._second_modifier: ItemModifier = second_modifier
        self._separator: str = separator

    def __repr__(self):
        return f"{self.__class__.__name__}({repr(self._first_modifier)}, " \
               f"{repr(self._second_modifier)}, {self._separator})"

    def __getattribute__(self, item):
        if item in ("first_prefix", "first_suffix"):
            return getattr(self._first_modifier, item[6:])
        elif item in ("second_prefix", "second_suffix"):
            return getattr(self._second_modifier, item[7:])
        else:
            return object.__getattribute__(self, item)

    def __bool__(self):
        return (bool(self._first_modifier), bool(self._second_modifier)) != (False, False)

    def _modify_pair(self, value_one: str, value_two: str):
        return f"{self._first_modifier.modify(value_one)}{self._separator}{self._second_modifier.modify(value_two)}"

    def modify_iterables(self, values_one: Sequence[str], values_two: Sequence[str]):
        _len_one: int = len(values_one)
        _len_two: int = len(values_two)
        if _len_one != _len_two:
            print(f"Lengths are {_len_one} and {_len_two}, but must be equal. The latest unpaired values are ignored")
        return [self._modify_pair(value_one, value_two) for value_one, value_two in zip(values_one, values_two)]


@configure_custom_logging("join")
@run_faulthandler
def main():
    first_file: GeneralFile = GeneralFile(r"C:\Users\tarasov-a\Desktop\fqdn.txt")
    first_file.read()
    second_file: GeneralFile = GeneralFile(r"C:\Users\tarasov-a\Desktop\ip.txt")
    second_file.read()

    list_one: list[str] = first_file.content
    list_two: list[str] = second_file.content

    item_modifier_one: ItemModifier = ItemModifier("", "")
    item_modifier_two: ItemModifier = ItemModifier("(", ")")
    _separator: str = " "

    item_join: ItemJoin = ItemJoin(item_modifier_one, item_modifier_two, _separator)
    for item in item_join.modify_iterables(list_one, list_two):
        print(item)


if __name__ == "__main__":
    main()
