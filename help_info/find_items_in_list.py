from typing import Iterable


def read_file(path: str):
    with open(path, "r+") as f:
        lines: list[str] = f.readlines()
    return lines


def check_listed(base: Iterable[str], *items: str):
    _base: frozenset[str] = frozenset(base)
    _check_dict: dict[str, bool] = dict()
    for item in items:
        _check_dict[item] = True if item in _base else False
    return _check_dict


if __name__ == '__main__':
    _path_ubuntu: str = "C:\\Users\\tarasov-a\\Desktop\\libs_ubuntu.txt"
    ubuntu_libs: list[str] = read_file(_path_ubuntu)
    _path_spgw: str = "C:\\Users\\tarasov-a\\Desktop\\libs_spgw.txt"
    _items: list[str] = read_file(_path_spgw)
    _results: dict[str, bool] = check_listed(ubuntu_libs, *_items)
    for k, v in _results.items():
        _k = k.strip("\n")
        _v = "installed" if v else "not installed"
        print(f"{_k}: {_v}")
    print("--------------------")
    _false: list[str] = [f"{k}" for k, v in _results.items() if not v]
    for _ in _false:
        print(_.strip("\n"))
