from __future__ import annotations

import faulthandler
import os.path as p
from re import Pattern, Match, match
from re import compile
from typing import Iterable

from loguru import logger

from init_logger import complete_configure_custom_logging


class LineNotMatchesPatternError(ValueError):
    """Line does not match the specified pattern"""


class Table:
    def __init__(self, lines: Iterable[str] = None):
        if lines is None:
            lines: list[str] = []
        self._lines: list[TableLine] = [TableLine(line) for line in lines]
        self._content: list[str] = []

    def __iter__(self):
        return iter(self._lines)

    def iter_content(self):
        return iter(self._content)

    def __getitem__(self, item):
        return self._lines[item]

    def get_content_item(self, item):
        return self._content[item]

    def process(self):
        table_notation: str = "|===\n"
        header: str = "|Bin |Dec | Cause\n"
        self._content.extend((table_notation, header))
        self._content.extend([table_line.process() for table_line in iter(self)])
        self._content.append(table_notation)

    def __str__(self):
        return "\n".join(self._content)


def join_matches(m: Match, separator: str = ""):
    return separator.join([m.group(i + 1) for i in range(len(m.groups()))])


class TableLine:
    def __init__(self, line: str):
        self._line: str = line
        logger.info(line)

    def get_binary(self):
        pattern: Pattern = compile(r"(\d)\s?(\d)\s?(\d)\s?(\d)\s?(\d)\s?(\d)\s?(\d)\s?(\d)")
        _: Match = match(pattern, self._line)
        if not _:
            logger.error(f"Line {self._line} and pattern {pattern.pattern} are not associated")
            raise LineNotMatchesPatternError
        return f"0b{join_matches(_)}"

    def get_value(self):
        pattern: Pattern = compile(r"\s*(.*)")
        _: Match = match(pattern, self._line[16:])
        if not _:
            logger.error(f"Line {self._line} and pattern {pattern.pattern} are not associated")
            raise LineNotMatchesPatternError
        return f"{_.group(1)}"

    def process(self):
        _bin, _value = self.get_binary(), self.get_value()
        _dec: int = int(_bin, 2)
        return f"|{_bin}\n|{_dec}\n|{_value}\n"


class TableFile:
    def __init__(self, name: str, table: Table):
        self._name: str = name
        self._path: str = p.join(p.abspath(p.dirname(p.dirname(p.realpath(__file__)))), "sources", self._name)
        self._table: Table = table

    def write(self):
        if not p.exists(self._path):
            with open(self._path, "x") as f:
                f.close()
        with open(self._path, "w+") as f:
            f.write(str(self._table))


@logger.catch
@complete_configure_custom_logging("table_file")
def main():
    sentinel: str = ""
    _ = iter(input, sentinel)
    lines: list[str] = list(_)
    table: Table = Table(lines)
    table.process()
    table_file: TableFile = TableFile("esm_causes.adoc", table)
    table_file.write()
    logger.info(f"Success")


if __name__ == '__main__':
    faulthandler.enable()
    main()
    if faulthandler.is_enabled():
        faulthandler.disable()
