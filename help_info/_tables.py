import os.path as p
from pathlib import Path
from tempfile import NamedTemporaryFile

import click
from loguru import logger

from help_info.ascii_doc import AsciiDocDocument, AsciiDocDocumentTable, AsciiDocFile
from help_info.const_classes import AVPItem, ArgumentType, BinReleaseCause, ContentGitPage, DiamType, HexReleaseCause, \
    LocalZoneProduct, LogFile, NetworkFunction, PartyCategory, StrPath, get_type
from help_info.custom_exceptions import NotEnoughArgumentsError


class FileTableGitPage:
    def __init__(self, file_name: str, table_index: int = 0):
        self._file_name: str = file_name
        self._table_index: int = table_index
        self._content: list[str] = []
        self._temp_file_path: Path | None = None

    def __str__(self):
        _: str = "\n".join(self._content)
        return f"{_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._file_name})>"

    def git_page(self):
        _git_page: ContentGitPage = ContentGitPage(self._file_name)
        _git_page.get_content()
        return _git_page

    def get_table(self):
        _indexes: list[int] = [
            index for index, line in enumerate(iter(self.git_page())) if line.startswith("|===")]
        _from: int = _indexes[2 * self._table_index]
        _to: int = _indexes[2 * self._table_index + 1]
        self._content = self.git_page()[_from, _to + 1]

    def __iter__(self):
        return iter(self._content)

    def __contains__(self, item):
        return item in self._content

    def get_cls_item(self, item):
        return self._content[item]

    def __setitem__(self, key, value):
        self._content[key] = value

    def temp_file(self):
        with NamedTemporaryFile(
                mode="w+",
                dir=p.abspath(p.dirname(p.abspath(__file__))),
                suffix=".adoc",
                delete=False) as tf:
            tf.write("\n".join(self._content))
            self._temp_file_path = tf.name


class TableGit(AsciiDocDocumentTable):
    _all: tuple[str, str] = ("-a", "--all")

    def __init__(self, path: StrPath, part_index: int = 0):
        doc_file: AsciiDocFile = AsciiDocFile(path)
        doc_file.read()
        document: AsciiDocDocument = AsciiDocDocument(doc_file)
        document.set_content()
        super().__init__(document, part_index)
        self.complete()

    @property
    def clsitems(self):
        raise NotImplementedError

    def iter_cls_items(self):
        return iter(self.clsitems)

    def get_cls_item(self, item):
        raise NotImplementedError

    def get_all(self):
        return "\n".join(str(_) for _ in self.clsitems)


class IsdnReleaseCause(TableGit):
    def __init__(self, path: StrPath):
        super().__init__(path)

    @property
    def clsitems(self) -> tuple[BinReleaseCause, ...]:
        return tuple(
            BinReleaseCause.from_string(*self.get_row_items(row_index))
            for row_index in range(self.max_row))

    @property
    def _dec_codes(self) -> tuple[int, ...]:
        _: BinReleaseCause
        return tuple(_.dec_code for _ in self.iter_cls_items())

    def _bin_codes(self) -> tuple[hex, ...]:
        _: BinReleaseCause
        return tuple(_.bin_code for _ in self.iter_cls_items())

    def _find_dec_code(self, dec_code: int) -> BinReleaseCause | None:
        for _ in self.clsitems:
            if _.dec_code == dec_code:
                return _
        logger.info(f"Decimal code {dec_code} is not found")
        return

    def _find_bin_code(self, bin_code: str) -> BinReleaseCause | None:
        for _ in self.clsitems:
            if _.bin_code == f"0b{int(bin_code, 2)}":
                return _
        logger.info(f"Bin code {bin_code} is not found")
        return

    def get_cls_item(self, item):
        if isinstance(item, int):
            return self._find_dec_code(item)
        elif isinstance(item, str):
            return self._find_bin_code(item)
        else:
            return NotImplemented

    def get_bin_code(self, item: hex) -> str | None:
        try:
            return str(self.get_cls_item(item))
        except (KeyError, ValueError, RuntimeError):
            return

    def get_dec_code(self, item: int) -> str | None:
        try:
            return str(self.get_cls_item(item))
        except (KeyError, ValueError, RuntimeError):
            return

    @click.command()
    @click.option('-a', '--all', 'all_', is_flag=True, default=False)
    @click.argument('values', nargs=-1, type=click.STRING, prompt="Введите искомые значения:")
    def get_code(self, all_, values):
        if not all_ and values is None:
            return click.echo(NotEnoughArgumentsError)
        if all_:
            return click.echo(self.get_all())
        for value in values:
            _: ArgumentType = get_type(value)
            if _ == ArgumentType.INT:
                click.echo(self.get_dec_code(value))
            elif _ == ArgumentType.BIN:
                click.echo(self.get_bin_code(value))
            else:
                logger.error(f"Invalid value {value}")
                continue
        return


class EsmReleaseCauseFile(TableGit):
    def __init__(self, path: StrPath):
        super().__init__(path)

    @property
    def clsitems(self) -> tuple[HexReleaseCause, ...]:
        return tuple(
            HexReleaseCause.from_string(*self.get_row_items(row_index))
            for row_index in range(self.max_row))

    @property
    def _dec_codes(self) -> tuple[int, ...]:
        _: HexReleaseCause
        return tuple(_.dec_code for _ in self.iter_cls_items())

    def _hex_codes(self) -> tuple[hex, ...]:
        _: HexReleaseCause
        return tuple(_.hex_code for _ in self.iter_cls_items())

    def _find_dec_code(self, dec_code: int) -> HexReleaseCause | None:
        for _ in self.clsitems:
            if _.dec_code == dec_code:
                return _
        logger.info(f"Decimal code {dec_code} is not found")
        return

    def _find_hex_code(self, hex_code: str) -> HexReleaseCause | None:
        for _ in self.clsitems:
            if _.hex_code == hex(int(hex_code, 16)):
                return _
        logger.info(f"Hex code {hex_code} is not found")
        return

    def get_cls_item(self, item):
        if isinstance(item, int):
            return self._find_dec_code(item)
        elif isinstance(item, str):
            return self._find_hex_code(item)
        else:
            return NotImplemented

    def get_hex_code(self, item: hex) -> str | None:
        try:
            return str(self.get_cls_item(item))
        except (KeyError, ValueError, RuntimeError):
            return

    def get_dec_code(self, item: int):
        try:
            return str(self.get_cls_item(item))
        except (KeyError, ValueError, RuntimeError):
            return

    @click.command()
    @click.option('-a', '--all', 'all_', is_flag=True, default=False)
    @click.argument('values', nargs=-1, type=click.STRING, prompt="Введите искомые значения:")
    def get_code(self, all_, values):
        if not all_ and values is None:
            return click.echo(NotEnoughArgumentsError)
        if all_:
            return click.echo(self.get_all())
        for value in values:
            _: ArgumentType = get_type(value)
            if _ == ArgumentType.INT:
                click.echo(self.get_dec_code(value))
            elif _ == ArgumentType.HEX:
                click.echo(self.get_hex_code(value))
            else:
                logger.error(f"Invalid value {value}")
                continue
        return


class EmmReleaseCauseFile(TableGit):
    def __init__(self, path: StrPath):
        super().__init__(path)

    @property
    def clsitems(self) -> tuple[HexReleaseCause, ...]:
        return tuple(
            HexReleaseCause.from_string(*self.get_row_items(row_index))
            for row_index in range(self.max_row))

    @property
    def _dec_codes(self) -> tuple[int, ...]:
        _: HexReleaseCause
        return tuple(_.dec_code for _ in self.iter_cls_items())

    def _hex_codes(self) -> tuple[hex, ...]:
        _: HexReleaseCause
        return tuple(_.hex_code for _ in self.iter_cls_items())

    def _find_dec_code(self, dec_code: int) -> HexReleaseCause | None:
        for _ in self.clsitems:
            if _.dec_code == dec_code:
                return _
        logger.info(f"Decimal code {dec_code} is not found")
        return

    def _find_hex_code(self, hex_code: str) -> HexReleaseCause | None:
        for _ in self.clsitems:
            if _.hex_code == hex(int(hex_code, 16)):
                return _
        logger.info(f"Hex code {hex_code} is not found")
        return

    def get_cls_item(self, item):
        if isinstance(item, int):
            return self._find_dec_code(item)
        elif isinstance(item, str):
            return self._find_hex_code(item)
        else:
            return NotImplemented

    def get_hex_code(self, item: hex) -> str | None:
        try:
            return str(self[item])
        except (KeyError, ValueError, RuntimeError):
            return

    def get_dec_code(self, item: int):
        try:
            return str(self[item])
        except (KeyError, ValueError, RuntimeError):
            return

    @click.command()
    @click.option('-a', '--all', 'all_', is_flag=True, default=False)
    @click.argument('values', nargs=-1, type=click.STRING, prompt="Введите искомые значения:")
    def get_code(self, all_, values):
        if not all_ and values is None:
            return click.echo(NotEnoughArgumentsError)
        if all_:
            return click.echo(self.get_all())
        for value in values:
            _: ArgumentType = get_type(value)
            if _ == ArgumentType.INT:
                click.echo(self.get_dec_code(value))
            elif _ == ArgumentType.HEX:
                click.echo(self.get_hex_code(value))
            else:
                logger.error(f"Invalid value {value}")
                continue
        return


class LocalZonesFile(TableGit):
    def __init__(self, path: StrPath):
        super().__init__(path)

    @property
    def clsitems(self) -> tuple[LocalZoneProduct, ...]:
        return tuple(
            LocalZoneProduct.from_string(*self.get_row_items(row_index))
            for row_index in range(self.max_row))

    @property
    def _names(self) -> frozenset[str]:
        _: LocalZoneProduct
        return frozenset(_.name for _ in self.iter_cls_items())

    def get_names(self) -> str:
        return ", ".join(self._names)

    def get_cls_item(self, item):
        if isinstance(item, str):
            if item in self._names:
                return list(filter(lambda x: x.name.find(item) != -1, self.iter_cls_items()))
            else:
                logger.info(f"Product {item} is not found")
                return
        elif isinstance(item, (int, slice)):
            try:
                return self.clsitems[item]
            except (ValueError, KeyError, RuntimeError) as e:
                logger.info(f"{e.__class__.__name}, {e.args}")
                return
        else:
            return NotImplemented

    def _product(self, name: str) -> list[str]:
        try:
            return [str(_) for _ in self.get_cls_item(name)]
        except (KeyError, ValueError, RuntimeError) as e:
            logger.info(f"{e.__class__.__name}, {e.args}")
            raise

    def get_product(self, name: str) -> str | None:
        try:
            return "\n".join(self._product(name))
        except (KeyError, ValueError, RuntimeError):
            return


class AVPFile(TableGit):
    def __init__(self, path: StrPath):
        super().__init__(path)

    @property
    def clsitems(self) -> tuple[AVPItem, ...]:
        return tuple(
            AVPItem.from_string(*self.get_row_items(row_index))
            for row_index in range(self.max_row))

    @property
    def _names(self) -> tuple[str, ...]:
        _: AVPItem
        return tuple(_.attribute_name for _ in self.iter_cls_items())

    @property
    def _codes(self) -> tuple[int, ...]:
        _: AVPItem
        return tuple(_.avp_code for _ in self.iter_cls_items())

    def _find_code(self, code: int) -> AVPItem | None:
        for _ in self.iter_cls_items():
            if _.avp_code == code:
                return _
        logger.info(f"Code {code} is not found")
        return

    def get_names(self) -> str:
        return ", ".join(self._names)

    def get_cls_item(self, item):
        if isinstance(item, str):
            if item in self._names:
                return list(filter(lambda x: x.attribute_name.find(item) != -1, self.iter_cls_items()))
            else:
                logger.info(f"AVP {item} is not found")
                return
        elif isinstance(item, int):
            return self._find_code(item)
        else:
            return NotImplemented

    def _attribute_name(self, name: str) -> list[str]:
        try:
            return [str(_) for _ in self[name]]
        except (KeyError, ValueError, RuntimeError) as e:
            logger.info(f"{e.__class__.__name}, {e.args}")
            raise

    def get_attribute_name(self, name: str) -> str | None:
        try:
            return "\n".join(self._attribute_name(name))
        except (KeyError, ValueError, RuntimeError):
            return

    def get_avp_code(self, code: int) -> str | None:
        try:
            return str(self._find_code(code))
        except (KeyError, ValueError, RuntimeError):
            return


class CategoryFile(TableGit):
    def __init__(self, path: StrPath):
        super().__init__(path)

    @property
    def clsitems(self) -> tuple[PartyCategory, ...]:
        return tuple(
            PartyCategory.from_string(*self.get_row_items(row_index))
            for row_index in range(self.max_row))

    @property
    def _categories(self) -> tuple[int, ...]:
        _: PartyCategory
        return tuple(_.category for _ in self.iter_cls_items())

    def _codes(self) -> tuple[int, ...]:
        _: PartyCategory
        return tuple(_.code for _ in self.iter_cls_items())

    def _find_category(self, category: int) -> PartyCategory | None:
        for _ in self.clsitems:
            if _.category == category:
                return _
        logger.info(f"Category {category} is not found")
        return

    def _find_code(self, code: int) -> PartyCategory | None:
        for _ in self.clsitems:
            if _.code == code:
                return _
        logger.info(f"Code {code} is not found")
        return

    def get_cls_item(self, item):
        if isinstance(item, int):
            if item < 10:
                return self._find_code(item)
            else:
                return self._find_category(item)
        else:
            return NotImplemented

    def get_cgpc(self, item: int) -> str | None:
        try:
            return str(self.get_cls_item(item))
        except (KeyError, ValueError, RuntimeError):
            return


class DiameterTypesFile(TableGit):
    def __init__(self, path: StrPath):
        super().__init__(path)

    @property
    def clsitems(self) -> tuple[DiamType, ...]:
        return tuple(
            DiamType(*self.get_row_items(row_index))
            for row_index in range(self.max_row))

    @property
    def _names(self) -> tuple[str, ...]:
        _: DiamType
        return tuple(_.name for _ in self.iter_cls_items())

    def get_names(self) -> str:
        return ", ".join(self._names)

    def get_cls_item(self, item):
        if isinstance(item, str):
            if item in self._names:
                return list(filter(lambda x: x.name.find(item) != -1, self.iter_cls_items()))
            else:
                logger.info(f"Diameter type {item} is not found")
                return
        elif isinstance(item, (int, slice)):
            try:
                return self.clsitems[item]
            except (ValueError, KeyError, RuntimeError) as e:
                logger.info(f"{e.__class__.__name}, {e.args}")
                return
        else:
            return NotImplemented

    def _diam_type(self, name: str) -> list[str]:
        try:
            return [str(_) for _ in self.get_cls_item(name)]
        except (KeyError, ValueError, RuntimeError) as e:
            logger.info(f"{e.__class__.__name}, {e.args}")
            raise

    def get_diam_type(self, name: str) -> str | None:
        try:
            return "\n".join(self._diam_type(name))
        except (KeyError, ValueError, RuntimeError):
            return


class LogNamesFile(TableGit):
    def __init__(self, path: StrPath):
        super().__init__(path)

    @property
    def clsitems(self) -> tuple[LogFile, ...]:
        return tuple(
            LogFile(*self.get_row_items(row_index))
            for row_index in range(self.max_row))

    @property
    def _names(self) -> tuple[str, ...]:
        _: LogFile
        return tuple(_.name for _ in self.iter_cls_items())

    def get_names(self) -> str:
        return ", ".join(self._names)

    def get_cls_item(self, item):
        if isinstance(item, str):
            if item in self._names:
                return list(filter(lambda x: x.name.find(item) != -1, self.iter_cls_items()))
            else:
                logger.info(f"Log file {item} is not found")
                return
        elif isinstance(item, (int, slice)):
            try:
                return self.clsitems[item]
            except (ValueError, KeyError, RuntimeError) as e:
                logger.info(f"{e.__class__.__name}, {e.args}")
                return
        else:
            return NotImplemented

    def _log_file(self, name: str) -> list[str]:
        try:
            return [str(_) for _ in self.get_cls_item(name)]
        except (KeyError, ValueError, RuntimeError) as e:
            logger.info(f"{e.__class__.__name}, {e.args}")
            raise

    def get_log_file(self, name: str) -> str | None:
        try:
            return "\n".join(self._log_file(name))
        except (KeyError, ValueError, RuntimeError):
            return


class NetworkFunctionsFile(TableGit):
    def __init__(self, path: StrPath):
        super().__init__(path)

    @property
    def clsitems(self) -> tuple[NetworkFunction, ...]:
        return tuple(
            NetworkFunction.from_string(*self.get_row_items(row_index))
            for row_index in range(self.max_row))

    @property
    def _names(self) -> tuple[str, ...]:
        _: NetworkFunction
        return tuple(_.nf_name for _ in self.iter_cls_items())

    def get_names(self) -> str:
        return ", ".join(self._names)

    def get_cls_item(self, item):
        if isinstance(item, str):
            if item in self._names:
                return list(filter(lambda x: x.nf_name.find(item) != -1, self.iter_cls_items()))
            else:
                logger.info(f"Network function {item} is not found")
                return
        elif isinstance(item, (int, slice)):
            try:
                return self.clsitems[item]
            except (ValueError, KeyError, RuntimeError) as e:
                logger.info(f"{e.__class__.__name}, {e.args}")
                return
        else:
            return NotImplemented

    def _nf(self, name: str) -> list[str]:
        try:
            return [str(_) for _ in self.get_cls_item(name)]
        except (KeyError, ValueError, RuntimeError) as e:
            logger.info(f"{e.__class__.__name}, {e.args}")
            raise

    def get_nf(self, name: str) -> str | None:
        try:
            return "\n".join(self._nf(name))
        except (KeyError, ValueError, RuntimeError):
            return
