from re import Pattern, fullmatch
from typing import Iterable, Literal


class LineContainer:
    def __init__(self, lines: Iterable[str] = None):
        if lines is None:
            lines: list[str] = []
        self._lines: list[str] = [*lines]
        self._pattern_from: Pattern | None = None
        self._pattern_to: Pattern | None = None

    def __iter__(self):
        return iter(self._lines)

    def __getitem__(self, item):
        return self._lines.__getitem__(item)

    def __setitem__(self, key, value):
        self._lines.__setitem__(key, value)

    def __len__(self):
        return len(self._lines)

    def set_pattern(self, pattern: Pattern, pattern_type: Literal["from", "to"]):
        setattr(self, f"_pattern_{pattern_type}", pattern)

    def find_lines(self):
        return list(filter(lambda x: fullmatch(self._pattern_from, x), self))


