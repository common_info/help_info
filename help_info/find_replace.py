import faulthandler
import os.path as p
import re
from contextlib import closing
from os import walk as w
from pathlib import Path
from typing import Iterator, NamedTuple
from re import compile, Pattern, Match, fullmatch
from loguru import logger

from common_functions import StrPath
from help_info.abstract_elements import StrNone
from init_logger import complete_configure_custom_logging


class SymbolFindEmptyError(ValueError):
    """The line to search im empty."""


class FindReplaceParameters(NamedTuple):
    user_path: str
    user_find: str
    user_sub: str | None

    def processed(self):
        if not self.user_sub:
            self._replace(user_sub=None)
        if not self.user_find:
            logger.error(f"Искомая строка пуста")
            raise SymbolFindEmptyError
        return [_ for _ in self._asdict().values()]


class RootFolder:
    def __init__(self, path: StrPath):
        if isinstance(path, str):
            path: Path = Path(path).resolve()
        self._path: Path = path

    def recursive_files(self) -> list[Path]:
        return [Path(root).joinpath(file) for root, _, files in w(self._path) for file in files]

    def iter_recursive_files(self) -> Iterator[str]:
        return iter(p.join(root, file) for root, _, files in w(self._path) for file in files)

    def recursive_subfolders(self) -> list[Path]:
        return [Path(root).joinpath(subdir) for root, subdirs, _ in w(self._path) for subdir in subdirs]

    def recursive_file_names(self) -> tuple[str, ...]:
        return tuple(map(lambda x: Path(x).name, self.recursive_files()))

    def iter_recursive_file_names(self) -> Iterator[str]:
        return iter(map(lambda x: Path(x).name, self.recursive_files()))

    def repr_recursive_files(self):
        return "\n".join([str(path) for path in self.recursive_files()])

    def repr_recursive_subfolders(self) -> str:
        return "\n".join([str(path) for path in self.recursive_subfolders()])

    def repr_recursive_file_names(self) -> str:
        return "\n".join([str(path) for path in self.recursive_file_names()])

    def __iter__(self):
        return self.iter_recursive_files()

    def __contains__(self, item):
        return item in self.recursive_files()

    @property
    def path(self):
        return self._path


class SymbolFinder:
    def __init__(self, file: StrPath, symbols_find: str):
        if isinstance(file, str):
            file: Path = Path(file).resolve()

        self._symbols_find: str = symbols_find
        self._file: Path = file
        self._content: list[str] = []
        self._found: dict[int, int] = dict()

    def set_content(self):
        with closing(open(self._file, "r+", encoding="utf-8")) as f:
            _ = f.readlines()
        # logger.info(_)
        self._content = _
        # logger.info(self._content)

    def set_found(self):
        _: dict[int, int] = {
            line_index: index.find(self._symbols_find)
            for line_index, index in enumerate(self._content)
            if index.find(self._symbols_find) != -1
        }
        # logger.info(_)
        self._found.update(_)
        logger.info(self._found)

    def __len__(self):
        return len(self._symbols_find)

    def __iter__(self):
        return iter(self._found.items())

    def __getitem__(self, item):
        return self._content[item]

    def __str__(self):
        _: list[str] = [
            f"Строка {line_index}, символ {index}\n{self[line_index]}"
            for line_index, index in iter(self)]
        return "\n".join(_)

    @property
    def found(self):
        return self._found


class SymbolReplacer(SymbolFinder):
    def __init__(self, file: StrPath, symbols_find: str, symbols_sub: str | None):
        if isinstance(file, str):
            file: Path = Path(file).resolve()
        super().__init__(file, symbols_find)
        self._symbols_sub: StrNone = symbols_sub
        self._file: Path = file
        self._content: list[str] = []

    def substitute(self):
        if self._symbols_sub is None:
            return
        for _index, _ in iter(self):
            _line: str = self[_index].replace(self._symbols_find, self._symbols_sub)
            self[_index] = _line
        self.save()

    def save(self):
        with closing(open(self._file, "w+", encoding="utf-8")) as f:
            f.writelines(self._content)

    def __len__(self):
        return len(self._symbols_find)

    def __iter__(self):
        return iter(self._found.items())

    def __getitem__(self, item):
        return self._content[item]

    def __setitem__(self, key, value):
        self._content[key] = value

    def __str__(self):
        if self._symbols_sub is None:
            _: list[str] = [
                f"Строка {line_index}, символ {index}\n{self[line_index]}"
                for line_index, index in iter(self)]
        else:
            _: list[str] = [
                f"Строка {line_index}, символ {index}, замена:\n{self[line_index]}"
                for line_index, index in iter(self)]
        return "\n".join(_)


class RootFolderFile:
    def __init__(self, folder: StrPath, name: str):
        if isinstance(folder, str):
            folder: Path = Path(folder).resolve()
        self._folder: Path = folder
        self._name: str = name

    @property
    def full_path(self):
        return self._folder.joinpath(self._name)

    def find(self, symbols_find: str):
        _: SymbolFinder = SymbolFinder(self.full_path, symbols_find)
        _.set_content()
        _.set_found()
        logger.info(f"{_.found}")
        return str(_)

    def substitute(self, symbols_find: str, symbols_sub: str | None):
        if symbols_sub is None:
            return
        _: SymbolReplacer = SymbolReplacer(self.full_path, symbols_find, symbols_sub)
        _.set_content()
        _.set_found()
        _.substitute()
        return str(_)


@logger.catch
@complete_configure_custom_logging("find_n_replace")
def main(path: StrPath, char_find: str, char_sub: str | None):
    try:
        root_folder: RootFolder = RootFolder(path)
        # logger.info(root_folder.path)
        for name in root_folder.iter_recursive_file_names():
            # logger.info(name)
            root_folder_file: RootFolderFile = RootFolderFile(path, name)
            # logger.info(root_folder_file.full_path)
            if char_sub is None:
                _: str = root_folder_file.find(char_find)
            else:
                _: str = root_folder_file.substitute(char_find, char_sub)
            # logger.info(_)
    except OSError as e:
        logger.error(f"{e.__class__.__name__}: {e.strerror}")
        raise


if __name__ == '__main__':
    faulthandler.enable()
    _path: list[Path] = [
        Path(r"C:\Users\tarasov-a\PycharmProjects\Protei_MME\content\common\data_records\cdr\connect.md"),
        Path(r"C:\Users\tarasov-a\PycharmProjects\Protei_MME\content\common\data_records\cdr\diam.md"),
        Path(r"C:\Users\tarasov-a\PycharmProjects\Protei_MME\content\common\data_records\cdr\tau.md")]
    # _conversion: dict[str, str] = {"| Field": "| Поле", "| Description": "| Описание", "| Type": "| Тип"}

    _pattern_errors: Pattern = compile(r"^\*\s(\d+)\s-\s(.*)\n$")
    _pattern_table: Pattern = compile(r"^(\d+)\.\s(.*)\s-\s(.*)\n$")

    for file in _path:
        with open(file, "r+", encoding="cp1251", errors="ignore") as fr:
            _content: list[str] = fr.readlines()
        for index, line in enumerate(_content):
            logger.info(line)
            _m_errors: Match = fullmatch(_pattern_errors, line)
            logger.info(_m_errors)
            if _m_errors:
                logger.info(_m_errors.groupdict())
                _content[index] = re.sub(_pattern_errors, f"| {_m_errors.group(1)} | {_m_errors.group(2)} |\n", line)
            _m_table: Match = fullmatch(_pattern_table, line)
            logger.info(_m_table)
            if _m_table:
                logger.info(_m_table.groupdict())
                _content[index] = re.sub(
                    _pattern_table,
                    f"| {_m_table.group(1)} | {_m_table.group(2)} | {_m_table.group(3)} |\n",
                    line)

        with open(file, "w+", encoding="cp1251", errors="replace") as fw:
            fw.writelines(_content)

        # with open(file, "r+b") as f:
        #     _ = f.read()
        #
        # with open(file, "w+") as f:
        #     f.write(_.decode("cp1251"))

    # _find: list[str] = ["Field", "Description", "Type"]
    # _sub: list[str] = ["Поле", "Описание", "Тип"]
    # for file in _path.iterdir():
    #     for k, v in _conversion.items():
    #         main(_path, k, v)
    if faulthandler.is_enabled():
        faulthandler.disable()
