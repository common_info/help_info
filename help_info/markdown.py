from __future__ import annotations

from pathlib import Path
from typing import Iterable, Iterator

from loguru import logger

from help_info.common_functions import StrPath
from help_info.const_classes import TableCellCoordinate, TableItem
from help_info.custom_exceptions import MarkdownFileNotFoundError


class MarkdownFile:
    def __init__(self, path: StrPath, content: Iterable[str] = None):
        if content is None:
            content: list[str] = []
        if isinstance(path, str):
            path: Path = Path(path).resolve()
        self._path: Path = path
        self._content: list[str] = [*content]

    def _validate(self):
        if not self._path.exists():
            logger.error(f"Path {self._path} is invalid")
            raise MarkdownFileNotFoundError

    @property
    def reader(self):
        return MarkdownFileReader(self._path)

    def read(self):
        return self.reader.read()

    @property
    def writer(self):
        return MarkdownFileWriter(self._path, self._content)

    def write(self, content: Iterable[str] = None):
        return self.writer.write()

    def __iter__(self):
        return iter(self._content)

    def __getitem__(self, item):
        return self._content.__getitem__(item)

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self._content
        else:
            return False

    @property
    def content(self):
        return self._content


class MarkdownFileReader(MarkdownFile):
    def read(self):
        with open(self._path, "r+") as f:
            _: list[str] = f.readlines()
        return _


class MarkdownFileWriter(MarkdownFile):
    def write(self, content: Iterable[str] = None):
        if content is None:
            content: list[str] = self._content
        with open(self._path, "w+") as f:
            f.writelines(content)


class MarkdownTable:
    def __init__(self, lines: Iterable[str] = None):
        if lines is None:
            lines: list[str] = []
        self._lines: list[str] = [*lines]
        self._items: dict[TableCellCoordinate, TableItem] = dict()

    def set_items(self):
        for row_index, line in enumerate(self.__iter__()):
            for column_index, content in enumerate(line.strip("|").split(" | ")):
                _key: TableCellCoordinate = TableCellCoordinate(row_index, column_index)
                _value: TableItem = TableItem(row_index, column_index, content.strip(""))
                self.__setitem__(_key, _value)

    @classmethod
    def from_markdown_file(cls, markdown_file: MarkdownFile):
        return cls(markdown_file.read())

    def write(self, path: str | Path):
        with open(path, "w+") as f:
            f.writelines(self._lines)

    def __str__(self):
        return "\n".join(self._lines)

    def __iter__(self) -> Iterator[str]:
        return iter(self._lines[2:])

    def __getitem__(self, item):
        if isinstance(item, (int, slice)):
            return self._lines.__getitem__(item)
        elif isinstance(item, TableCellCoordinate):
            return self._items.__getitem__(item)
        else:
            raise KeyError

    def __setitem__(self, key, value):
        if isinstance(key, int):
            if isinstance(value, str):
                self._lines.__setitem__(key, value)
            else:
                ...
        elif isinstance(key, TableCellCoordinate):
            if isinstance(value, TableItem):
                self._items.__setitem__(key, value)
            else:
                ...
        else:
            ...

    def __len__(self):
        return len(self._lines)

    def __hash__(self):
        return hash(self._lines)

    def __add__(self, other):
        if isinstance(other, MarkdownRow):
            self._lines.append(other.__str__())
            other.to_markdown_table(self, self.__len__())
        elif isinstance(other, str):
            self._lines.append(other)
        else:
            return NotImplemented

    __radd__ = __add__
    __iadd__ = __add__

    def insert_row(self, row: MarkdownRow | str, index: int = -1):
        if index == -1:
            return self.__add__(row)
        if isinstance(row, MarkdownRow):
            return self._lines.insert(index, row.__str__())
        elif isinstance(row, str):
            return self._lines.insert(index, row)
        return

    def iter_rows(self) -> Iterator[MarkdownRow]:
        return iter(MarkdownRow(line, self, index) for index, line in enumerate(self.__iter__()))

    def rows(self) -> list[MarkdownRow]:
        return list(self.iter_rows())

    def get_row(self, item):
        return self.rows().__getitem__(item)

    def replace_row(self, __from: int, __to: int):
        _row: str = self._lines.pop(__from)
        self.insert_row(_row, __to)

    def delete_row(self, index: int):
        self._lines.pop(index)

    @property
    def items(self):
        return self._items

    @property
    def lines(self):
        return self._lines


class MarkdownRow:
    def __init__(self, line: str, markdown_table: MarkdownTable | None = None, row_index: int | None = None):
        self._line: str = line
        self._markdown_table: MarkdownTable | None = markdown_table
        self._row_index: int | None = row_index
        self._cells: list[TableItem] = []

    def to_markdown_table(self, markdown_table: MarkdownTable, row_index: int):
        self._markdown_table = markdown_table
        self._row_index = row_index

    def __str__(self):
        return f"| {' | '.join(self._cells)} |"

    def __iter__(self):
        return iter(self._cells)

    def __getitem__(self, item):
        return self._cells.__getitem__(item)

    def __setitem__(self, key, value):
        self._cells.__setitem__(key, value)

    def __len__(self):
        return len(self._cells)

    def __bool__(self):
        return self.__len__() == 1

    def up_down(self, diff: int = 0):
        if self._markdown_table is not None:
            self._row_index += diff

    def update(self):
        self._line = self.__str__()
        if self._markdown_table is not None:
            self._markdown_table.__setitem__(self._row_index, self._line)

    def insert_cell(self, cell: str, index: int = -1):
        if index == -1:
            self._cells.append(cell)
        else:
            self._cells.insert(index, cell)

    def replace_cell(self, __from: int, __to: int):
        _row: str = self._cells.pop(__from)
        self._cells.insert(__to, _row)

    def delete_cell(self, index: int):
        self._cells.pop(index)

    def split_cell(self, index: int, *, text_old: str | None = None, text_new: str | None = None):
        if text_old is None:
            text_old: str = self.__getitem__(index)
        if text_new is None:
            text_new: str = ""
        self.insert_cell(text_new, index + 1)
        self.__setitem__(index, text_old)

    is_subheader = __bool__


# class MarkdownCellParser:
#     _parse_patterns = {
#         "Type": Pattern(r"Тип\s*.\s*(.*),?\s*.*\s+\."),
#         "Range": Pattern(r"Диапазон\s?.\s*(\w+)\W(\w+)\."),
#         "OMPR": Pattern(r"([OM]*)\s?/\s?([PR]?)"),
#         "Note": Pattern(r"Примечание\s*.?\s?(\w+)\."),
#         "Default": Pattern(r"Значение по умолчанию\s*.\s*(.+)\.")
#     }
#
#     def __init__(self, markdown_row: MarkdownRow, column_index: int):
#         self._markdown_row: MarkdownRow = markdown_row
#         self._column_index: int = column_index
#         self._text: str = markdown_row.__getitem__(column_index)
#         self._subheader = self._markdown_row.is_subheader()
#         self._parsed: dict[str, str | None] = {
#             "description": None,
#             "type": None,
#             "units": None,
#             "range": None,
#             "default": None,
#             "om": None,
#             "pr": None
#         }
#
#     def has_pattern(self, pattern: str | Pattern):
#         if not self._subheader:
#             return fullmatch(pattern, self._text) is not None
#
#     def _ompr(self):
#         return fullmatch(self._parse_patterns.get("OMPR"), self._markdown_row.__getitem__(1))
#
#     def _parse_om(self):
#         if self._ompr():
#             return self._ompr().group(1)
#         return
#
#     def _parse_pr(self):
#         if self._ompr():
#             return self._ompr().group(2)
#         return
#
#     def _description(self):
#         return self._markdown_row.__getitem__(2).split(".")
#
#     def _type_units(self):
#         for line in self._description():
#             _: Match = fullmatch(self._parse_patterns.get("Type"), line)
#             if _:
#                 return _.group(1)
#         return
#
#     def _parse_range(self):
#         for line in self._description():
#             _: Match = fullmatch(self._parse_patterns.get("Range"), line)
#             if _:
#                 return _.group(1)
#         return
#
#     def _parse_default(self):
#         for line in self._description():
#             _: Match = fullmatch(self._parse_patterns.get("Default"), line)
#             if _:
#                 return _.group(1)
#         return


# class CellParsedText(NamedTuple):
#     row_index: int
#     column_index: int
#     column_name: str
#     text: str
