from __future__ import annotations

import os
from pathlib import Path
from re import Pattern, Match, fullmatch, compile
from typing import TypeAlias, Iterator, Mapping

from loguru import logger

StrPath: TypeAlias = str | Path

CFG_PATTERN: Pattern = compile(r"\|\sName\s+\|\sDescription\s+\|\sType\s+\|\sDefault\s+\|\sO/M\s\|\sP/R\s\|"
                               r"\sVersion\s\|")
CFG_REPLACE: str = "| Параметр | Описание | Тип | По умолчанию | O/M | P/R | Версия |"
CDR_PATTERN: Pattern = compile(r"\|\sN\s+\|\sField\s+\|\sDescription\s+\|\sType\s+\|")
CDR_REPLACE: str = "| N | Поле | Описание | Тип |"


class BaseFolder:
    def __init__(self, path: StrPath):
        self._path: Path = Path(path).resolve()
        self._validate()

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def _validate(self):
        if not self._path.exists():
            raise FileNotFoundError(f"{self._path} is not found")
        if not self._path.is_dir():
            raise KeyError(f"{self._path} does not lead to the directory")

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        self._path = value

    def recursive_files(self) -> list[Path]:
        return [Path(root).joinpath(file) for root, _, files in os.walk(self._path) for file in files]

    def iter_recursive_files(self) -> Iterator[str]:
        return iter(os.path.join(root, file) for root, _, files in os.walk(self._path) for file in files)

    def recursive_subfolders(self) -> list[Path]:
        return [Path(root).joinpath(subdir) for root, subdirs, _ in os.walk(self._path) for subdir in subdirs]

    def recursive_file_names(self) -> tuple[str, ...]:
        return tuple(map(lambda x: Path(x).name, self.recursive_files()))

    def iter_recursive_file_names(self) -> Iterator[str]:
        return iter(map(lambda x: Path(x).name, self.recursive_files()))

    def repr_recursive_files(self):
        return "\n".join([str(path) for path in self.recursive_files()])

    def repr_recursive_subfolders(self) -> str:
        return "\n".join([str(path) for path in self.recursive_subfolders()])

    def repr_recursive_file_names(self) -> str:
        return "\n".join([str(path) for path in self.recursive_file_names()])

    def __iter__(self) -> Iterator[BaseFile]:
        return iter(BaseFile(self, os.path.basename(_)) for _ in self.iter_recursive_files())


class BaseFile:
    def __init__(self, base_folder: BaseFolder, name: str):
        self._base_folder: BaseFolder = base_folder
        self._name: str = name
        self._content: list[str] = []

    @property
    def _path(self):
        return self._base_folder.path.joinpath(self._name)

    def _validate(self):
        if not self._path.exists():
            logger.error(f"Path {self._path} does not exist")
            raise FileNotFoundError
        if not self._path.is_file():
            logger.error(f"Path {self._path} does not point to the file")
            raise IsADirectoryError
        return

    def __str__(self):
        return "\n".join(self._content)

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def read(self):
        with open(self._path, "r+") as f:
            _: list[str] = f.readlines()
        self._content = _

    def save(self):
        with open(self._path, "w+") as f:
            f.write(str(self))

    def __iter__(self):
        return iter(self._content)

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self._content
        return False

    def __getitem__(self, item):
        return self._content[item]

    def __setitem__(self, key, value):
        self._content[key] = value

    def __len__(self):
        return len(self._content)

    def __bool__(self):
        return len(self) != 0

    def replace(self, replacement: Mapping[str, str]):
        for _ in iter(self):
            for k, v in replacement.items():
                _ = _.replace(k, v)
        return self

    def replace_pattern(self, pattern: Pattern, sub: Mapping[int, str]):
        for _ in self.__iter__():
            _m: Match = fullmatch(pattern, _)
            if _m:
                for index, line in sub.items():
                    _ = _.replace(_m.group(index), line)
        return self
