import string
from bisect import bisect_left
from enum import Enum, auto
from pathlib import Path
from re import Match, Pattern, compile, fullmatch
from typing import Iterable, NamedTuple, TypeAlias

from loguru import logger
from urllib3 import HTTPResponse

from custom_exceptions import AsciiDocFileTableRowIndexError, InvalidIpAddressError
from help_info.common_functions import iter_pairs
from http_request import CustomHTTPRequest, CustomHTTPResponseChunked, CustomPort, CustomPreparedRequest, \
    CustomScheme

StrPath: TypeAlias = str | Path


class LinkType(Enum):
    INTERNAL = auto
    EXTERNAL = auto

    def __str__(self):
        return f"{self.__class__.__name__}: {self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._name_})>"


class DocumentType(Enum):
    MARKDOWN = auto
    ASCII_DOC = auto

    def __str__(self):
        return f"{self.__class__.__name__}: {self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._name_})>"


class TableCellCoordinate(NamedTuple):
    row_index: int | None
    column_index: int | None

    def __str__(self):
        return f"({self.row_index}, {self.column_index})"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.row_index}, {self.column_index})>"

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return (self.row_index, self.column_index) == (other.row_index, other.column_index)
        else:
            return False

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return (self.row_index, self.column_index) != (other.row_index, other.column_index)
        else:
            return False

    def __lt__(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented
        if self.row_index == other.row_index:
            return self.column_index < other.column_index
        elif self.column_index == other.column_index:
            return self.row_index < other.row_index
        else:
            return False

    def __gt__(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented
        if self.row_index == other.row_index:
            return self.column_index > other.column_index
        elif self.column_index == other.column_index:
            return self.row_index > other.row_index
        else:
            return False

    def __le__(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented
        if self.row_index == other.row_index:
            return self.column_index <= other.column_index
        elif self.column_index == other.column_index:
            return self.row_index <= other.row_index
        else:
            return False

    def __ge__(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented
        if self.row_index == other.row_index:
            return self.column_index >= other.column_index
        elif self.column_index == other.column_index:
            return self.row_index >= other.row_index
        else:
            return False

    def __bool__(self):
        return self.row_index is not None and self.column_index is not None

    @classmethod
    def null_coordinate(cls):
        return cls(None, None)


class TableItem(NamedTuple):
    row_index: int | None
    column_index: int | None
    text: str | None

    @property
    def coord(self) -> TableCellCoordinate:
        return TableCellCoordinate(self.row_index, self.column_index)

    def __str__(self):
        return f"{self.text}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.row_index}, {self.column_index}, {self.text})>"

    @property
    def is_null(self) -> bool:
        return self.coord.__bool__()

    def __bool__(self):
        return bool(self.text)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.coord == other.coord
        else:
            return False

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.coord != other.coord
        else:
            return False

    def __lt__(self, other):
        if isinstance(other, self.__class__):
            return self.coord < other.coord
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__):
            return self.coord > other.coord
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, self.__class__):
            return self.coord <= other.coord
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, self.__class__):
            return self.coord >= other.coord
        else:
            return NotImplemented

    def str_table_item(self):
        return f"|{self.text}"

    @classmethod
    def null_table_item(cls):
        return cls(None, None, None)


class Standard(NamedTuple):
    name: str
    link: str

    def __str__(self):
        return f"{self.link}[{self.name}]"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.name}, {self.link})>"

    @classmethod
    def from_string(cls, standard: str):
        link, name = standard[-1].split("[")
        return cls(name, link)


class ContentGitPage(NamedTuple):
    file_name: str
    content: list[str] = []

    def __iter__(self):
        return iter(self.content)

    def __str__(self):
        return "\n".join(iter(self))

    @property
    def _custom_http_request(self):
        scheme: str = CustomScheme.HTTPS.value
        web_hook: str = f"api/v4/projects/46389466/repository/files/{self.file_name}"
        host: str = "gitlab.com"
        port: int = CustomPort.HTTPS.value
        params: tuple[tuple[str, str], ...] = (("ref", "main"),)
        return CustomHTTPRequest(scheme=scheme, web_hook=web_hook, host=host, port=port, params=params)

    def get_content(self):
        _prepared_request: CustomPreparedRequest = CustomPreparedRequest.from_custom_request(self._custom_http_request)
        _http_response: HTTPResponse = _prepared_request.http_response()
        _chunked: CustomHTTPResponseChunked
        _chunked = CustomHTTPResponseChunked(_http_response)
        self.content.extend(_chunked.decode_base64().split("\n"))
        _chunked.delete_temp_file()

    def __getitem__(self, item):
        return self.content.__getitem__(item)

    def __len__(self):
        return len(self.content)

    def __bool__(self):
        return len(self.content) != 0


class PartyCategory(NamedTuple):
    category: int
    code: int
    description: str

    def __str__(self):
        return f"{self.category} --- {self.description} ({self.code})"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.category}, {self.code}, {self.description})>"

    @classmethod
    def from_string(cls, category: str, code: str, description: str):
        return cls(int(category), int(code), description)


class DiamType(NamedTuple):
    name: str
    description_en: str
    description_ru: str
    length: str
    additional: str | None

    def __str__(self):
        if self.additional is None:
            add: str = ""
        else:
            add: str = f", {self.additional}"
        return f"{self.name} --- {self.description_en} | {self.description_ru} ({self.length}){add}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.name}, {self.description_en}, " \
               f"{self.description_ru}, {self.length}, {self.additional})>"


class LogFile(NamedTuple):
    name: str
    description_ru: str
    description_en: str | None

    def __str__(self):
        if self.description_en is None:
            en: str = ""
        else:
            en: str = f", ({self.description_en})"
        return f"{self.name} --- {self.description_ru}{en}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.name}, {self.description_ru}, {self.description_en})>"


class ParamType(Enum):
    STRING = "string"
    INT = "int"
    HEX = "hex"
    INT_HEX = "int/hex"

    def __str__(self):
        return f"{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._name_})>"

    def __missing__(self, key):
        return


class Param(NamedTuple):
    param_name: str
    param_type: ParamType

    def __str__(self):
        return f"{self.param_name} ({self.param_type})"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.param_name}, {self.param_type})>"


class AppType(Enum):
    SERVER = "Server"
    WEB = "Web"
    API = "API"
    DATABASE = "Database"
    CLI = "CLI"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._value_})>"

    def __str__(self):
        return f"{self._name_}"

    def __missing__(self, key):
        return


class Credentials(NamedTuple):
    login: str = "support"
    password: str = "elephant"

    def __str__(self):
        return f"{self.login}/{self.password}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.login}, {self.password})>"


class IPAddress:
    def __init__(self, ip_address: str, port: int | None, web_hook: str | None):
        self._ip_address: str = ip_address
        self._port: int | None = port
        self._web_hook: str = web_hook
        self.validate()

    def __str__(self):
        _port: str = "" if self._port is None else f":{self._port}"
        _web_hook: str = "" if self._web_hook is None else f"/{self._web_hook}"
        return f"{self._ip_address}{_port}{_web_hook}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._ip_address}, {self._port}, {self._web_hook})>"

    def validate(self):
        ip_octets: list[str] = self._ip_address.split(".")
        # logger.info(ip_octets)
        if not all(0 <= int(_) < 256 for _ in ip_octets) or len(ip_octets) != 4:
            raise InvalidIpAddressError

    @classmethod
    def from_string(cls, address: str):
        pattern: Pattern = compile(r"^([\d.]*):?(\d*)/?(.*)")
        match: Match = fullmatch(pattern, address)
        if match:
            ip_address: str = match.group(1)
            port: int | None = None if not match.group(2) else int(match.group(2))
            web_hook: str | None = None if not match.group(3) else match.group(3)
            return cls(ip_address, port, web_hook)


class LocalZoneProduct(NamedTuple):
    name: str
    app_type: AppType
    credentials: Credentials
    ip_address: IPAddress
    additional: str | None

    def __str__(self):
        _add = "" if not self.additional else f". Дополнительно: {self.additional}"
        return f"Продукт {self.name}({str(self.app_type)}): {str(self.credentials)}, " \
               f"{str(self.ip_address)}{_add})"

    def __repr__(self):
        return f"<{self.__class__.__name__}(" \
               f"{self.name}, {repr(self.app_type)}, {repr(self.credentials)}, {repr(self.ip_address)}, " \
               f"{self.additional})>"

    @classmethod
    def from_string(cls, name: str, app_type: str, login: str, password: str, address: str, additional: str | None):
        return cls(
            name,
            AppType(app_type),
            Credentials(login, password),
            IPAddress.from_string(address),
            additional)


class _Term(NamedTuple):
    short: str = None
    full: str = None
    rus: str = None
    commentary: str = None

    def __repr__(self):
        return f"<{self.__class__.__name__}(short={self.short}, full={self.full}, rus={self.rus}, " \
               f"commentary={self.commentary})>"

    def __str__(self):
        return f"{self.short}\n{self.full}, {self.rus} -- {self.commentary}"

    def _multiplier(self, attr: str) -> int:
        value: str = getattr(self, attr)
        return int(value != "")

    def __bool__(self):
        return all(not v for v in self._asdict().values())

    def _formatting_type(self) -> int:
        """1 * bool(full) + 2 * bool(rus) + 4 * bool(commentary)"""
        _: dict[str, int] = {
            "full": 1,
            "rus": 2,
            "commentary": 4
        }
        return sum(map(lambda x: self._multiplier(x) * _.get(x), _.keys()))

    def _formatting_description(self) -> str:
        _: dict[int, str] = {
            1: f"{self.full}",
            2: f"{self.rus}",
            3: f"{self.full}, {self.rus}",
            4: f"{self.commentary}",
            5: f"{self.full} — {self.commentary}",
            6: f"{self.rus} — {self.commentary}",
            7: f"{self.full}, {self.rus} — {self.commentary}"
        }
        return _.get(self._formatting_type(), "")

    def abbr(self):
        if self.__bool__():
            return f"Not found"
        else:
            return f'<abbr title="{self.full}">{self.short}</abbr>'

    def formatted(self) -> str:
        if self.__bool__():
            return f"Не найдено"
        else:
            return f"{self.short}\n{self._formatting_description()}"


class NetworkFunction(NamedTuple):
    nf_name: str
    full_name: str
    standard: Standard

    def __str__(self):
        return f"{self.nf_name} ({self.full_name}) --- {self.standard.name}({self.standard.link})"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.nf_name}, {self.full_name}, {self.standard})>"

    @classmethod
    def from_string(cls, nf_name: str, full_name: str, standard: str):
        return cls(nf_name, full_name, Standard.from_string(standard))


class DataType(Enum):
    ADDRESS = "Address"
    DIAMETER_IDENTITY = "DiameterIdentity"
    DIAMETER_URI = "DiameterURI"
    ENUMERATED = "Enumerated"
    GROUPED = "Grouped"
    INTEGER = "Integer"
    IP_FILTER_RULE = "IPFilterRule"
    OCTET_STRING = "OctetString"
    TIME = "Time"
    UNSIGNED_32 = "Unsigned32"
    UNSIGNED_64 = "Unsigned64"
    UTF8_STRING = "UTF8String"

    def __str__(self):
        return f"{self.__class__.__name__}: {self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._name_})>"

    def __missing__(self, key):
        return "None"


class AVPItem(NamedTuple):
    avp_code: int
    attribute_name: str
    data_type: DataType
    standard: Standard

    def __str__(self):
        return f"{self.attribute_name} ({self.avp_code}) --- {self.data_type}, {self.standard}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.avp_code}, {self.attribute_name}, " \
               f"{self.data_type}, {self.standard})>"

    @classmethod
    def from_string(cls, avp_code: str, attribute_name: str, data_type: str, standard: str):
        return cls(int(avp_code), attribute_name, DataType(data_type), Standard.from_string(standard))


class HexReleaseCause(NamedTuple):
    hex_code: hex
    dec_code: int
    description: str

    def __str__(self):
        return f"{self.hex_code}/{self.dec_code} --- {self.description}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.hex_code}, {self.dec_code}, {self.description})>"

    @classmethod
    def from_string(cls, hex_code: str, dec_code: str, description: str):
        return cls(hex(int(hex_code, 16)), int(dec_code), description)


class BinReleaseCause(NamedTuple):
    bin_code: str
    dec_code: int
    description: str

    def __str__(self):
        return f"{self.bin_code}/{self.dec_code} --- {self.description}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.bin_code}, {self.dec_code}, {self.description})>"

    @classmethod
    def from_string(cls, bin_code: str, dec_code: str, description: str):
        return cls(f"0b{int(bin_code, 2)}", int(dec_code), description)


class State(Enum):
    ACTIVE = "active"
    STOPPED = "stopped"

    def __str__(self):
        return f"{self.__class__.__name__}.{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self._name_}>"


class AsciiDocTableTerms:
    def __init__(self, lines: Iterable[str] = None):
        self._content: list[str] = [*lines]
        self._items: dict[TableCellCoordinate, TableItem] = dict()
        self.dict_terms: dict[str, tuple[_Term, ...]] = dict()
        self._header: list[str] = []

    def get_terms(self):
        _: list[_Term] = [_Term(*self.get_row_item(row_index)) for row_index in range(self.max_row)]
        _dict_proxy: dict[str, list[_Term]] = dict()
        for term in _:
            term_short: str = term.short
            if term_short not in _dict_proxy:
                _dict_proxy[term_short] = []
            _dict_proxy[term_short].append(term)
        _dict_terms: dict[str, tuple[_Term, ...]] = {k.upper(): (*v,) for k, v in _dict_proxy.items()}
        self.dict_terms.update(**_dict_terms)

    def __getitem__(self, item):
        if isinstance(item, str):
            return self._binary_search(item.upper())
        elif isinstance(item, int):
            _term_short: str = self._index_to_key(item)
            return self.dict_terms[_term_short]
        else:
            logger.debug(f"Термин {item} не найден")
            return _Term(),

    def __len__(self):
        return len(self.dict_terms)

    @property
    def _keys(self):
        return list(self.dict_terms.keys())

    def _index_to_key(self, index: int | None):
        return self._keys[index] if index else None

    def terms_short(self) -> tuple[str, ...]:
        return tuple(map(lambda x: x.upper(), self.dict_terms.keys()))

    def _binary_search(
            self,
            search_value: str,
            min_index: int = 0,
            max_index: int = None) -> tuple[_Term, ...]:
        """
        Specifies the binary search to accelerate the common one.

        :param str search_value: the value to search
        :param int min_index: the lower bound, default = 0
        :param max_index: the higher bound, default = len(values)
        :return: the searched value position in the list if the value is in the list.
        :rtype: int
        """
        if max_index is None:
            max_index = len(self)
        _index: int = bisect_left(self._keys, search_value, min_index, max_index)
        _term_short: str = self._index_to_key(_index)
        if _index != max_index and _term_short == search_value:
            return self.dict_terms[_term_short]
        return _Term()

    def get_row_item(self, row_index: int):
        return [v.text for v in self.get_row(row_index)]

    def get_row(self, row_index: int):
        if row_index > self.max_row:
            logger.error(f"Row index {row_index} is out of bounds")
            raise AsciiDocFileTableRowIndexError
        return [v for k, v in self._items.items() if k.row_index == row_index]

    @property
    def max_row(self):
        return max(_.row_index for _ in self._items.keys())

    def _find_header(self):
        pattern: Pattern = compile(r"\|(.*)\|(.*)\|(.*)\|(.*)\n")
        for index, line in enumerate(iter(self._content)):
            if pattern.fullmatch(line):
                return index
        else:
            return 2

    def set_header(self):
        header_line: str = self._content[self._find_header()]
        _: list[str] = header_line.split("|")
        self._header = [line.strip() for line in _ if line]

    def set_items(self):
        _empty: list[int] = [index for index, line in enumerate(self._content) if not line]
        for row_index, (_from, _to) in enumerate(iter_pairs(_empty[1:-1])):
            lines: list[str] = [content.strip("\n") for content in self._content[_from + 1:_to]]
            _: list[str] = "".join(lines).split("|")
            for column_index, text in enumerate(_[1:]):
                _key: TableCellCoordinate = TableCellCoordinate(row_index, column_index)
                _value: TableItem = TableItem(row_index, column_index, text.strip("|"))
                self._items[_key] = _value

    def complete(self):
        self.set_header()
        self.set_items()


class ArgumentType(Enum):
    HEX = "hex"
    INT = "int"
    BIN = "bin"
    INVALID_TYPE = "error"

    def __str__(self):
        return f"{self.__class__.__name__}.{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self._name_}>"


def get_type(value: str):
    _: str = value.strip()
    if _.isdigit():
        return ArgumentType.INT
    if _.startswith("0x") and any(char in string.hexdigits for char in _[2:]):
        return ArgumentType.HEX
    if _.startswith("0b") and any(char in ("0", "1") for char in _[2:]):
        return ArgumentType.BIN
    return ArgumentType.INVALID_TYPE


class Converter:
    @staticmethod
    def convert_to_int(value: str):
        _: ArgumentType = get_type(value)
        if _ == ArgumentType.INT:
            return int(value, 10)
        elif _ == ArgumentType.HEX:
            return int(value, 16)
        elif _ == ArgumentType.BIN:
            return int(value, 2)
        else:
            return None

    @staticmethod
    def convert_to_bin(value: str):
        _: ArgumentType = get_type(value)
        if _ == ArgumentType.INT:
            return bin(int(value, 10))
        elif _ == ArgumentType.HEX:
            return bin(int(value, 16))
        elif _ == ArgumentType.BIN:
            return bin(int(value, 2))
        else:
            return None

    @staticmethod
    def convert_to_hex(value: str):
        _: ArgumentType = get_type(value)
        if _ == ArgumentType.INT:
            return hex(int(value, 10))
        elif _ == ArgumentType.HEX:
            return hex(int(value, 16))
        elif _ == ArgumentType.BIN:
            return hex(int(value, 2))
        else:
            return None
