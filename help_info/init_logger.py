import logging
from functools import cached_property, wraps
from logging import Handler, LogRecord
from pathlib import Path
from sys import stdout as sysout
from time import perf_counter_ns
from types import FrameType
from typing import Type, Literal, Any, Callable

from loguru import logger


HandlerType: Type[str] = Literal["stream", "file_rotating"]
LoggingLevel: Type[str] = Literal["TRACE", "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]


def convert_ns_to_readable(time_ns: int) -> str:
    hours: int
    minutes: int
    seconds: int = time_ns // 10 ** 9
    if seconds == 0:
        return "менее секунды"
    if seconds < 60:
        hours = 0
        minutes = 0
    else:
        hours = 0
        minutes = seconds // 60
        seconds %= 60
        if minutes >= 60:
            hours = minutes // 60
            minutes %= 60
    hour_string: str = f"{hours} ч " if hours != 0 else ""
    minutes_string: str = f"{minutes} мин " if minutes != 0 else ""
    seconds_string: str = f"{seconds} с" if seconds != 0 else ""
    return f"{hour_string}{minutes_string}{seconds_string}"


def func_logging(func: Callable):
    @wraps(func)
    def wrapper(*args, **kwargs):
        _start: int = perf_counter_ns()
        logger.info(f"Function: {func.__name__}, args={args}, kwargs={kwargs}")
        result = func(*args, **kwargs)
        _end: int = perf_counter_ns()
        _elapsed: int = _end - _start
        logger.info(f"{func.__name__} returned {result} in {convert_ns_to_readable(_elapsed)}")
        return result
    return wrapper


def retry_failed(attempts: int = 1):
    def wrapper(func: Callable):
        @wraps(func)
        def inner(*args, **kwargs):
            for _ in range(attempts):
                try:
                    result = func(*args, **kwargs)
                except OSError as e:
                    if _ == attempts - 1:
                        logger.error(f"{e.__class__.__name__}, {e.strerror}")
                        raise
                    else:
                        logger.error(f"Attempt {_} failed.")
                        continue
                else:
                    return result
        return inner
    return wrapper


class InterceptHandler(Handler):
    def emit(self, record: LogRecord):
        # Get corresponding loguru level if it exists
        try:
            level: str = logger.level(record.levelname).name
        except ValueError:
            level: int = record.levelno
        # Find caller from where originated the logged message
        frame, depth = logging.currentframe(), 2
        while frame.f_code.co_filename == logging.__file__:
            frame: FrameType = frame.f_back
            depth += 1
        logger.opt(depth=depth, exception=record.exc_info).log(level, record.getMessage())


class LoggerConfiguration:
    _log_folder: Path = Path(__file__).parent.joinpath("logs")

    def __init__(
            self,
            file_name: str,
            handlers: dict[HandlerType, LoggingLevel] = None):
        if handlers is None:
            handlers = dict()
        self._file_name: str = file_name
        self._handlers: dict[str, str] = handlers

    @staticmethod
    def _no_http(record: dict):
        return not record["name"].startswith("urllib3")

    @cached_property
    def _colored_format(self):
        return " | ".join(
            ("<green>{time:DD-MMM-YYYY HH:mm:ss}</green>::<level>{level.name}</level>",
             "<cyan>{module}</cyan>::<cyan>{function}</cyan>",
             "<cyan>{file.name}</cyan>::<cyan>{name}</cyan>::<cyan>{line}</cyan>",
             "\n<level>{message}</level>")
        )

    @cached_property
    def _wb_format(self):
        return " | ".join(
            ("{time:DD-MMM-YYYY HH:mm:ss}::{level.name}",
             "{module}::{function}",
             "{file.name}::{name}::{line}",
             "\n{message}")
        )

    @cached_property
    def _user_format(self):
        return "{message}"

    def stream_handler(self):
        try:
            _logging_level: str | None = self._handlers.get("stream")
            _handler: dict[str, Any] = {
                "sink": sysout,
                "level": _logging_level,
                "format": self._colored_format,
                "colorize": True,
                "filter": self._no_http,
                "diagnose": True
            }
        except KeyError:
            return
        else:
            return _handler

    def rotating_file_handler(self):
        try:
            _log_path: Path = self._log_folder.joinpath(f"{self._file_name}_debug.log")
            _logging_level: str = self._handlers.get("file_rotating")
            _handler: dict[str, Any] = {
                "sink": _log_path,
                "level": _logging_level,
                "format": self._colored_format,
                "colorize": False,
                "diagnose": True,
                "rotation": "2 MB",
                "mode": "a",
                "encoding": "utf8"
            }
        except KeyError:
            return
        else:
            return _handler


def configure_custom_logging(name: str):
    def inner(func: Callable):
        def wrapper(*args, **kwargs):
            handlers: dict[HandlerType, LoggingLevel] = {
                "stream": "INFO",
                "file_rotating": "DEBUG"
            }

            file_name: str = name

            logger_configuration: LoggerConfiguration = LoggerConfiguration(file_name, handlers)
            stream_handler: dict[str, Any] = logger_configuration.stream_handler()
            rotating_file_handler: dict[str, Any] = logger_configuration.rotating_file_handler()

            logger.configure(handlers=[stream_handler, rotating_file_handler])

            logging.basicConfig(handlers=[InterceptHandler()], level=0)
            logger.debug("========================================")
            logger.debug("Запуск программы:")

            return func(*args, **kwargs)
        return wrapper
    return inner


def complete_configure_custom_logging(name: str):
    def inner(func: Callable):
        start_time: int = perf_counter_ns()

        handlers: dict[HandlerType, LoggingLevel] = {
            "stream": "INFO",
            "file_rotating": "DEBUG"
        }

        file_name: str = name

        logger_configuration: LoggerConfiguration = LoggerConfiguration(file_name, handlers)
        stream_handler: dict[str, Any] = logger_configuration.stream_handler()
        rotating_file_handler: dict[str, Any] = logger_configuration.rotating_file_handler()

        logger.configure(handlers=[stream_handler, rotating_file_handler])

        logging.basicConfig(handlers=[InterceptHandler()], level=0)
        logger.debug("========================================")
        logger.debug("Запуск программы:")

        def wrapper(*args, **kwargs):
            return func(*args, **kwargs)

        end_time: int = perf_counter_ns()
        delta_time: int = end_time - start_time
        logger.debug(f"Время выполнения: {delta_time} наносекунд, {convert_ns_to_readable(delta_time)}\n")

        return wrapper
    return inner
