from re import Pattern, Match, compile, match
from typing import Iterable


class HeadingModifier:
    def __init__(self, lines: Iterable[str] = None, level: int = -1):
        if level < 0:
            raise ValueError
        if lines is None:
            lines: list[str] = []
        self._lines: list[str] = [*lines]
        self._level: int = level

    def __iter__(self):
        return iter(self._lines)

    def __getitem__(self, item):
        return self._lines[item]

    def convert(self):
        pattern: Pattern = compile(r"\s*[*\-+]?\s?\[([\w\s-]+)]\(#([\w-]+).*")
        _lvl: str = "#" * self._level
        results: list[str] = []
        for line in iter(self):
            _m: Match = match(pattern, line)
            if _m:
                _name: list[str] = []
                for item in _m.group(1).split():
                    if not item[0].isupper():
                        item = item.capitalize()
                    _name.append(item)
                heading: str = " ".join(_name)
                _: str = f'{_lvl} <a name="{_m.group(2)}">{heading}</a> {_lvl}'
            else:
                _: str = line
            results.append(_)
        return results

    def __len__(self):
        return len(self._lines)


if __name__ == '__main__':
    sentinel: str = ""
    user_input: list[str] = list(iter(input, sentinel))
    print(len(user_input))
    _level: str = input("Level: \n")
    if not _level.isdigit():
        raise TypeError
    level: int = int(_level)
    heading_modifier: HeadingModifier = HeadingModifier(user_input, level)
    for _ in heading_modifier.convert():
        print(_)
