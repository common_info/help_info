from enum import Enum
from pathlib import Path


class EncodingName(Enum):
    UTF8 = "utf-8"
    CP1251 = "cp1251"

    def __str__(self):
        return f"{self.__class__.__name__}: {self._value_}"

    def __repr__(self):
        return f"{self.__class__.__name__}: {self._name_}"


class ProcessFile:
    def __init__(self, path: str | Path, encoding: EncodingName = EncodingName.UTF8):
        self._path: Path = Path(path)
        self._encoding: EncodingName = encoding
        self._content: list[bytes] = []
        self._lines: list[str] = []

    def read(self):
        with open(self._path, "rb") as fb:
            _: list[bytes] = fb.readlines()
        self._content = _
        self._lines = [line.decode(self._encoding.value) for line in _]

    def write_bytes(self):
        with open(self._path, "wb") as fb:
            fb.writelines(self._content)

    def write_str(self):
        with open(self._path, "w+") as f:
            f.writelines(self._lines)

    def save(self):
        with open(self._path, "r", encoding=self._encoding.value) as f:
            f.close()

    def __iter__(self):
        return iter(self._lines)

    def __str__(self):
        return "".join(self._lines)

    def __bytes__(self):
        return b'\n'.join(self._content)

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path, self._encoding, self._content})>"

    def get_line(self, key):
        return self._lines.__getitem__(key)

    def get_content(self, key):
        return self._content.__getitem__(key)

    def __setitem__(self, key, value):
        if isinstance(value, bytes):
            return self._content.__setitem__(key, value)
        elif isinstance(value, str):
            return self._lines.__setitem__(key, value)
        else:
            raise KeyError

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._path == other._path
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._path != other._path
        else:
            return NotImplemented

    def __missing__(self, key):
        return None

    @property
    def name(self):
        return self._path.name

    @property
    def path(self):
        return self._path

    @property
    def lines(self):
        return self._lines

    @property
    def content(self):
        return self._content


class CSVFile(ProcessFile):
    def get_all_content(self):
        self.read()

    def process_lines(self):
        _ = str(self).replace("\n", " +\n").replace("\t", "\t\n|").split("\t")
        with open(self._path, "w+", encoding=self._encoding.value) as f:
            f.writelines(_)


# class AsciiDocTableFromCsv(AsciiDocTable):
#     def __init__(self, csv_file: CSVFile):
#         csv_file.get_all_content()
#         self._csv_file: CSVFile = csv_file
#         lines: list[str] = self._csv_file.lines
#         super().__init__(self._csv_file.lines)

if __name__ == '__main__':
    path: str = "/Users/user/Desktop/IP tables.txt"
    csv_file: CSVFile = CSVFile(path)
    csv_file.read()
    print(str(csv_file))
    csv_file.process_lines()
