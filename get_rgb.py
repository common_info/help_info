from typing import Sequence

from loguru import logger


def get_rgb(color: bytes):
    if not isinstance(color, bytes):
        logger.error(f"Improper value, must be bytes, but {type(color)} received")
        raise TypeError
    if len(color) != 6:
        logger.error(f"Improper value, length must be 6, but {len(color)} received")
        raise ValueError
    r, g, b = color[0:2], color[2:4], color[4:6]
    return f"R {int(r, 16)} G {int(g, 16)} B {int(b, 16)}"


def get_color(*, rgb: Sequence[int] = None):
    if rgb is None:
        logger.error("All fields cannot be None at the same time")
        raise AttributeError
    if len(rgb) != 3:
        logger.error(f"Improper value, length must be 3, but {len(rgb)} received")
        raise ValueError
    if any(x < 0 or x > 255 for x in rgb):
        _: str = ", ".join([str(item) for item in rgb])
        logger.error(f"All values must be in range [0, 255], but {_} received")
        raise ValueError
    if any(not isinstance(x, int) for x in rgb):
        _: str = ", ".join([str(type(item)) for item in rgb])
        logger.error(f"All values must be int, but {_} received")
        raise TypeError
    return "".join([f"{hex(item)[2:]}" for item in rgb])
