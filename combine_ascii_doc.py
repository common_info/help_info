from pathlib import Path
from re import Pattern, compile, fullmatch
from typing import Iterable, NamedTuple

from help_info.const_classes import StrPath


class UserInputError(ValueError):
    """Invalid user input."""


class AsciiDocFile(NamedTuple):
    file_path: StrPath

    def __str__(self):
        return f"include::{str(self)}[]"

    def __repr__(self):
        return f"<{self.__class__.__name__}({str(self.file_path)})>"


class NonValueAttribute(NamedTuple):
    arg: str

    def __str__(self):
        return f":{self.arg}:"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.arg})>"


class ValueAttribute(NamedTuple):
    arg: str
    value: str

    def __str__(self):
        return f":{self.arg}: {self.value}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.arg}, {self.value})>"


class AsciiDocFileManager:
    def __init__(self, title: str, file_path: StrPath, files: Iterable[AsciiDocFile] = None, *args, **kwargs):
        if files is None:
            files: list[str] = []
        self._title: str = title
        self._file_path: Path = Path(file_path)
        self._files: list[AsciiDocFile] = [*files]
        self._args: list[NonValueAttribute] = [NonValueAttribute(arg) for arg in args]
        self._kwargs: list[ValueAttribute] = [ValueAttribute(arg, value) for arg, value in kwargs.items()]
        self._content: list[str] = []

    def __str__(self):
        return f"File path:\n{self._file_path}\nFiles:\n{self._files}\n" \
               f"Args:\n{self._args}\nKwargs:\n{self._kwargs}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._file_path}, {self._files}, {self._args}, {self._kwargs})>"

    def __len__(self):
        return len(self._content)

    def __iter__(self):
        return iter(self._content)

    def iter_files(self):
        return iter(self._files)

    def iter_args(self):
        return iter(self._args)

    def iter_kwargs(self):
        return iter(self._kwargs)

    @property
    def _kwarg_names(self) -> list[str]:
        return [value.arg for value in self._kwargs]

    @property
    def _arg_names(self) -> list[str]:
        return [value.arg for value in self._args]

    def process(self):
        self._add_args()
        self._add_kwargs()
        _title: str = f"= {self._title.title()}"
        _args: list[str] = [str(arg) for arg in self._args]
        _kwargs: list[str] = [str(kwarg) for kwarg in self._kwargs]
        return self._content.extend((_title, *_args, *_kwargs))

    def _add_kwargs(self):
        _kwargs: list[tuple[str, str]] = [
            ("chapter-signifier", "Раздел"),
            ("icons", "font"),
            ("toc", "auto"),
            ("toclevels", "5"),
            ("toc-title", "Содержание"),
            ("example-caption", "Пример"),
            ("sectnumlevels", "5"),
            ("figure-caption", "Рисунок"),
            ("table-caption", "Таблица")
        ]
        _new_kwargs: list[ValueAttribute] = [
            ValueAttribute(arg, value) for arg, value in _kwargs
            if arg not in self._kwarg_names]
        self._kwargs.extend(_new_kwargs)

    def _add_args(self):
        _args: tuple[str, ...] = ("experimental", "sectnums", "showtitle")
        _new_args: list[NonValueAttribute] = [
            NonValueAttribute(arg) for arg in _args
            if arg not in self._arg_names]
        self._args.extend(_new_args)
        return

    @property
    def to_file(self):
        return "\n".join(self._content)

    def read(self):
        with open(self._file_path, "r+") as f:
            _: list[str] = f.readlines()
        self._content = _

    def write(self):
        with open(self._file_path, "w+") as f:
            f.write(self.to_file)

    def __add__(self, other):
        def _add(_value):
            if isinstance(_value, str):
                self._content.append(_value)
            elif isinstance(_value, AsciiDocFile):
                self._files.append(_value)
            elif isinstance(_value, NonValueAttribute):
                self._args.append(_value)
            elif isinstance(_value, ValueAttribute):
                self._kwargs.append(_value)
            else:
                return NotImplemented

        if isinstance(other, Iterable):
            for value in other:
                _add(value)
                return
        return _add(other)


class CliUserInput:
    _pattern: Pattern = compile(r".* --title=(.*)\s(--flags=\[.*]\s)?(--params=\[.*])?")

    def __init__(self, user_input: str):
        self._user_input: str = user_input
        self._items: list[str] = [_.strip() for _ in self._user_input.split()]

    def __iter__(self):
        return iter(self._items)

    def __getitem__(self, item):
        return self._items[item]

    def __len__(self):
        return len(self._items)

    def _validate(self):
        if not fullmatch(self._pattern, self._user_input):
            raise UserInputError
        return
