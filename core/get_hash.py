import faulthandler
from enum import Enum
from hashlib import md5, sha1, sha256, sha224, sha384, sha512, sha3_224, sha3_256, sha3_384, sha3_512, shake_128, \
    shake_256, blake2s, blake2b
from pathlib import Path

from loguru import logger

from help_info.init_logger import configure_custom_logging


class HashAlgo(Enum):
    MD5 = "md5"
    SHA1 = "sha1"
    SHA_224 = "sha224"
    SHA_256 = "sha256"
    SHA_384 = "sha384"
    SHA_512 = "sha512"
    SHA3_224 = "sha3_224"
    SHA3_256 = "sha3_256"
    SHA3_384 = "sha3_384"
    SHA3_512 = "sha3_512"
    SHAKE_128 = "shake_128"
    SHAKE_256 = "shake_256"
    BLAKE2B = "blake2b"
    BLAKE2S = "blake2s"
    PBKDF2_HMAC = "pbkdf2_hmac"

    def __str__(self):
        return f"{self.__class__.__name__}: {self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self._name_}>"

    @property
    def converted(self):
        _conversion_table = {
            self.__class__.MD5: md5(),
            self.__class__.SHA1: sha1(),
            self.__class__.SHA_224: sha224(),
            self.__class__.SHA_256: sha256(),
            self.__class__.SHA_384: sha384(),
            self.__class__.SHA_512: sha512(),
            self.__class__.SHA3_224: sha3_224(),
            self.__class__.SHA3_256: sha3_256(),
            self.__class__.SHA3_384: sha3_384(),
            self.__class__.SHA3_512: sha3_512(),
            self.__class__.SHAKE_128: shake_128(),
            self.__class__.SHAKE_256: shake_256(),
            self.__class__.BLAKE2B: blake2b(),
            self.__class__.BLAKE2S: blake2s(),
        }
        return _conversion_table.get(self.value)


class HashProcessor:
    def __init__(self, algo: HashAlgo, *, line: str = None, lines: list[str] = None):
        if line is None and lines is None:
            raise AttributeError
        self._line: str = line
        self._lines: list[str] = lines
        self._algo: HashAlgo = algo
        self._dict: dict[str, str] = dict()

    def process(self):
        if self._line is not None and self._lines is not None:
            self._lines.append(self._line)
            self._line = None

    def get_hash(self):
        if self._lines is not None:
            for line in self._lines:
                _hash = self._algo.converted
                _hash.update(line.encode())
                self._dict[line] = _hash.hexdigest()
        if self._line is not None:
            _hash = self._algo.converted
            _hash.update(self._line.encode())
            self._dict[self._line] = _hash.hexdigest()

    @property
    def content(self):
        return "\n".join([f"{self._algo.value}({k}) = {v}" for k, v in self._dict.items()])

    def write_to_file(self, file_name: str | Path):
        if isinstance(file_name, str):
            file_name: Path = Path(file_name).resolve()
        if not file_name.exists():
            file_name.touch()
        with open(file_name, "a") as f:
            f.write(self.content)


@logger.catch
@configure_custom_logging("hash")
def main():
    user_input = input("Type the value(s) to hash:\n")
    if len(user_input.split()) == 1:
        line: str = user_input.strip()
        lines: list[str] | None = None
    else:
        lines: list[str] = user_input.split()
        line: str | None = None
    algo: HashAlgo = HashAlgo.MD5
    hash_processor: HashProcessor = HashProcessor(line=line, lines=lines, algo=algo)
    hash_processor.process()
    hash_processor.get_hash()
    file: Path = Path(__file__).parent.joinpath("hashes.log")
    hash_processor.write_to_file(file)


if __name__ == "__main__":
    faulthandler.enable()
    main()
    if faulthandler.is_enabled():
        faulthandler.disable()
