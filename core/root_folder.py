import os.path as p
from os import walk as w
from pathlib import Path
from typing import TypeAlias, Iterator

StrPath: TypeAlias = str | Path


class RootFolder:
    def __init__(self, path: StrPath):
        if isinstance(path, str):
            path: Path = Path(path)
        self._path: Path = path.resolve()

    def recursive_files(self) -> list[Path]:
        return [Path(root).joinpath(file) for root, _, files in w(self._path) for file in files]

    def iter_recursive_files(self) -> Iterator[str]:
        return iter(p.join(root, file) for root, _, files in w(self._path) for file in files)

    def recursive_subfolders(self) -> list[Path]:
        return [Path(root).joinpath(subdir) for root, subdirs, _ in w(self._path) for subdir in subdirs]

    def recursive_file_names(self) -> tuple[str, ...]:
        return tuple(map(lambda x: Path(x).name, self.recursive_files()))

    def iter_recursive_file_names(self) -> Iterator[str]:
        return iter(map(lambda x: Path(x).name, self.recursive_files()))

    def repr_recursive_files(self):
        return "\n".join([str(path) for path in self.recursive_files()])

    def repr_recursive_subfolders(self) -> str:
        return "\n".join([str(path) for path in self.recursive_subfolders()])

    def repr_recursive_file_names(self) -> str:
        return "\n".join([str(path) for path in self.recursive_file_names()])

    def __iter__(self):
        return self.iter_recursive_files()

    def __contains__(self, item):
        return item in self.recursive_files()

    @property
    def path(self):
        return self._path
