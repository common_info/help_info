class TreeElementIndexNotFoundError(KeyError):
    """No element with the specified index is found."""


class TreeElementParentIndexError(KeyError):
    """No element is capable of being a self-parent."""


class LoopError(Exception):
    """Header and tail are the same item."""


class SameItemError(Exception):
    """Link cannot connect two equal items."""


class RequiredAttributeMissing(Exception):
    """Required attribute has no specified value."""


class TableElementIndexError(KeyError):
    """Index cannot be negative or preceed the table limits."""
