from typing import Self, TypeAlias, Iterable, Iterator

from loguru import logger

from core.core_exceptions import SameItemError, LoopError

IntNone: TypeAlias = int | None


class ItemArray:
    index: int = 0
    items: dict[int, Self] = dict()

    @classmethod
    def get(cls, item) -> Self:
        if isinstance(item, int):
            return cls.items.get(item, None)
        else:
            raise KeyError

    def __init__(self, before: int = -1, after: int = -2, _id: int = None, **kwargs):
        self._before: int = before
        self._after: int = after
        if _id is None:
            _id: int = self.__class__.index
        self._item_id: int = _id
        self._locked: bool = False
        self._args: list[str] = [*kwargs.keys()]
        for k, v in kwargs.items():
            setattr(self, k, v)

        self.__class__.items[self._item_id] = self
        if _id is not None:
            self.__class__.index += 1

    @property
    def before(self):
        return self._before

    @before.setter
    def before(self, value):
        if self._locked:
            raise KeyError
        if isinstance(value, int) and value in self.__class__.items:
            self._before = value
        elif isinstance(value, str) and value.isnumeric():
            self._before = int(value)
        else:
            raise ValueError

    @before.deleter
    def before(self):
        self._before = None

    @property
    def after(self):
        return self._after

    @after.setter
    def after(self, value):
        if self._locked:
            raise KeyError
        if isinstance(value, int) and value in self.__class__.items:
            self._after = value
        elif isinstance(value, str) and value.isnumeric():
            self._after = int(value)
        else:
            raise ValueError

    @after.deleter
    def after(self):
        self._after = None

    def delete(self):
        self._before = None
        self._after = None

    def lock(self):
        self._locked = True

    def unlock(self):
        if not self._locked:
            return self
        item: Self = self.__class__(self._before, self._after, self._item_id)
        del self
        return item

    def __str__(self):
        return f"{self._before}, {self._item_id}, {self._after}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._before}, {self._after}, {self._item_id}, {self._locked})>"

    def __hash__(self):
        return hash(self._item_id)

    def __key(self):
        return self._before, self._item_id, self._after

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, self.__class__):
            return self._item_id < other._item_id
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__):
            return self._item_id > other._item_id
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, self.__class__):
            return self._item_id <= other._item_id
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, self.__class__):
            return self._item_id >= other._item_id
        else:
            return NotImplemented

    def __bool__(self):
        return self._item_id > 0

    @property
    def item_id(self):
        return self._item_id


class ItemArrayBoundItems(ItemArray):
    def __new__(cls, _id: int, *args, **kwargs):
        if _id in super().items:
            return super().items.get(_id)
        return super().__new__(cls, _id)

    def __init__(self, _id: int):
        before: int = -1
        after: int = -1
        super().__init__(before, after, _id)

    @classmethod
    def __call__(cls, *args, **kwargs):
        raise NotImplementedError

    def __bool__(self):
        return False

    def __lt__(self, other):
        raise NotImplementedError

    def __gt__(self, other):
        raise NotImplementedError

    def __le__(self, other):
        raise NotImplementedError

    def __ge__(self, other):
        raise NotImplementedError


class ItemArrayHead(ItemArrayBoundItems):
    def __new__(cls, *args, **kwargs):
        return super().__new__(cls, -1)

    @property
    def before(self):
        return self._before

    @before.setter
    def before(self, value):
        return

    @before.deleter
    def before(self):
        return

    @classmethod
    def __call__(cls, *args, **kwargs):
        return cls()

    def __lt__(self, other):
        if isinstance(other, self.__class__.__mro__):
            return True
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__.__mro__):
            return False
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, self.__class__.__mro__):
            return True
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, self.__class__.__mro__):
            return False
        else:
            return NotImplemented


class ItemArrayTail(ItemArrayBoundItems):
    def __new__(cls, *args, **kwargs):
        return super().__new__(cls, -2)

    @property
    def after(self):
        return self._after

    @after.setter
    def after(self, value):
        return

    @after.deleter
    def after(self):
        return

    @classmethod
    def __call__(cls, *args, **kwargs):
        return cls()

    def __lt__(self, other):
        if isinstance(other, self.__class__.__mro__):
            return False
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__.__mro__):
            return True
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, self.__class__.__mro__):
            return False
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, self.__class__.__mro__):
            return True
        else:
            return NotImplemented


class OrderNumber(int):
    def __init__(self, value: int):
        super().__init__()
        self.value: int = value

    def __str__(self):
        return str(self.value)

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.value})>"


class ItemIdentifier(int):
    def __init__(self, value: int):
        super().__init__()
        self.value: int = value

    def __str__(self):
        return str(self.value)

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.value})>"


class ArrayLinked:
    _spec: tuple[str, str] = ("head", "tail")

    def __init__(self, ids: Iterable[int] = None):
        if ids is None:
            ids: list[int] = []
        self._items: list[int] = [ItemArrayTail().item_id, ItemArrayHead()]
        self._items.extend(ids)
        self._items_array: list[int] = []

    def _find_start(self):
        for _ in self._items[2:]:
            if ItemArray(_).before == -1:
                return _
        else:
            logger.error("Loop")
            raise

    def _find_end(self):
        for _ in self._items[2:]:
            if ItemArray(_).after == -2:
                return _
        else:
            logger.error("Loop")
            raise

    def _generate(self):
        pass

    def array_items(self) -> list[ItemArray]:
        return list(iter(self.iter_array_items()))

    def iter_array_items(self) -> Iterator[ItemArray]:
        return iter(ItemArray.get(item_id) for item_id in iter(self))

    def __iter__(self) -> Iterator[int]:
        return iter(self._items[2:])

    def __getitem__(self, item):
        if isinstance(item, int) and item >= 0:
            return self._items[item + 2]
        elif isinstance(item, str):
            if item == "head":
                return self._items[1]
            elif item == "tail":
                return self._items[0]
            else:
                logger.error(f"Key {item} type must be int but str received")
                raise TypeError
        else:
            logger.error(f"Key {item} must be int but {type(item)} received")
            raise KeyError


class LinkedArrayItem:
    index: int = 0
    array_items: dict[int, Self] = dict()

    def __init__(self, before: int = None, after: int = None, **kwargs):
        self._index: int = self.__class__.index
        self._before: IntNone = before
        self._after: IntNone = after
        self.idx: IntNone = None
        for k, v in kwargs.items():
            setattr(self, k, v)

        self.__class__.array_items[self._index] = self
        self.__class__.index += 1

    @property
    def is_header(self) -> bool:
        return self._before is None

    @property
    def is_tail(self) -> bool:
        return self._after is None

    def __bool__(self):
        return bool(self.is_header and self.is_tail)

    def before_item(self):
        if not self.is_header:
            return self.__class__.array_items.get(self._before)
        return

    def after_item(self):
        if not self.is_tail:
            return self.__class__.array_items.get(self._after)
        return

    @classmethod
    def values(cls):
        return cls.array_items.values()

    @classmethod
    def keys(cls):
        return cls.array_items.keys()

    @property
    def after(self):
        return self._after

    @after.setter
    def after(self, value):
        self._after = value

    @property
    def before(self):
        return self._before

    @before.setter
    def before(self, value):
        self._before = value

    def __hash__(self):
        return self._index

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._index == other._index
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._index != other._index
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__):
            return self in other.children()
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, self.__class__):
            return other in self.children()
        else:
            return NotImplemented

    def children(self):
        def _get_children(item: Self, _children: list[Self] = None):
            if _children is None:
                _children: list[int] = []
            if item.after is None:
                return _children
            _children.append(self._after)
            return _get_children(self.after_item(), _children)

        return _get_children(self)

    def parents(self):
        def _get_parents(item: Self, _parents: list[int] = None):
            if _parents is None:
                _parents: list[int] = []
            if item.before is None:
                return _parents
            _parents.append(self._before)
            return _get_parents(self.before_item(), _parents)

        return _get_parents(self)

    @classmethod
    def items(cls):
        return cls.array_items.items()

    def connect_before(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented
        if self == other:
            raise SameItemError
        else:
            self._before = other._index
            other._after = self._index

    def connect_after(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented
        if self == other:
            raise SameItemError
        else:
            self._after = other._index
            other._before = self._index

    def shift(self, value: int):
        for _ in self.children():
            _.idx += value


class LinkedArray:
    def __init__(self):
        self._indexes: list[int] = []

    def __iter__(self):
        return iter(LinkedArrayItem.values())

    def __len__(self):
        return len(LinkedArrayItem.values())

    @property
    def head(self) -> LinkedArrayItem:
        for item in iter(self):
            if item.is_header:
                return item

    @property
    def tail(self) -> LinkedArrayItem:
        for item in iter(self):
            if item.is_tail:
                return item

    @property
    def array(self) -> list[LinkedArrayItem]:
        def _generate(item: LinkedArrayItem, _array: list[LinkedArrayItem] = None, counter: int = 0):
            if _array is None:
                _array: list[LinkedArrayItem] = []
            if item.is_tail:
                item.idx = counter
                self._indexes.append(item.index)
                return _array
            _: LinkedArrayItem = item.after_item()
            if _ in _array:
                raise LoopError
            _array.append(_)
            self._indexes.append(item.index)
            _.idx = counter
            counter += 1
            return _generate(_, _array, counter)

        return _generate(self.head)

    def __getitem__(self, item):
        return self.array.__getitem__(item)

    def insert_item(self, item: LinkedArrayItem, index: int):
        if index < 0 | index > len(self):
            raise IndexError
        if index == 0:
            self.head.connect_before(item)
        elif index == len(self):
            self.tail.connect_after(item)
        else:
            left: LinkedArrayItem = LinkedArrayItem.array_items.get(self._indexes[index - 1])
            left.connect_after(item)
            right: LinkedArrayItem = LinkedArrayItem.array_items.get(self._indexes[index])
            right.connect_before(item)
        self._indexes.insert(index, item.index)
        item.shift(1)

    def add_item(self, item: LinkedArrayItem):
        if item.before is None:
            return self.insert_item(item, 0)
        if item.after in None:
            return self.insert_item(item, len(self))
        left: LinkedArrayItem = item.before_item()
        right: LinkedArrayItem = item.after_item()
        if left.after == right.index and right.before == left.index:
            left.after = item.index
            right.before = item.index
            item.before = left.index
            item.after = right.index
        item.shift(1)

    def delete_item(self, item):
        if not isinstance(item, (int, LinkedArrayItem)):
            return NotImplemented
        if isinstance(item, int):
            item: LinkedArrayItem = LinkedArrayItem.array_items[self._indexes[item]]
        left: LinkedArrayItem = item.before_item()
        right: LinkedArrayItem = item.after_item()
        left.after = right.index
        right.before = left.index
        del item
        return
