from typing import Iterable, Self, Iterator, Any, NamedTuple, TypeAlias
from loguru import logger
from base64 import b64decode
from enum import Enum
from http.client import HTTPResponse
from json import loads
from os import environ
from pathlib import Path
from urllib.parse import quote_plus
from urllib.request import Request, urlopen

from core.core_exceptions import TreeElementIndexNotFoundError, SameItemError, LoopError, RequiredAttributeMissing, \
    TableElementIndexError, TreeElementParentIndexError
from help_info.const_classes import TableCellCoordinate, TableItem

IntNone: TypeAlias = int | None


class TreeElement:
    identifier: int = 0
    tree_items: dict[int, Self] = dict()

    def __init__(self, parent: int | None, children: Iterable[int] = None, **kwargs):
        if children is None:
            children: list[int] = []
        if parent is None:
            parent: int = 0
        self._index: int = self.__class__.identifier
        self._parent: int = parent
        self._children: list[int] = [*children]
        for k, v in kwargs.items():
            setattr(self, k, v)

        self.__class__.tree_items[self._index] = self
        self.__class__.identifier += 1

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._parent}, {', '.join([str(_) for _ in self._children])})>"

    def __str__(self):
        return f"{self.__class__.__name__}: {self._index}"

    def __bool__(self) -> bool:
        return self._parent is None

    @property
    def parent_element(self) -> Self:
        return self.__class__.tree_items[self._parent]

    def __len__(self):
        return len(self._children)

    @property
    def children_elements(self) -> list[Self] | None:
        if len(self):
            return [self.__class__.tree_items[_] for _ in self._children]
        return

    def iter_elements(self) -> Iterator[Self]:
        return iter(self.children_elements)

    def iter_indexes(self) -> Iterator[int]:
        return iter(self._children)

    # @property
    # def index(self):
    #     return self._index
    #
    # @property
    # def children(self):
    #     return self._children

    def __class_getitem__(cls, item):
        if isinstance(item, (int, slice)):
            return cls.tree_items[item]
        else:
            return NotImplemented

    def __getitem__(self, item):
        if isinstance(item, (int, slice)):
            return self.children_elements[item]
        else:
            raise KeyError

    def __missing__(self, key):
        logger.info(f"Key {key} is not found")
        return

    def __contains__(self, item):
        if isinstance(item, int):
            return item in self._children
        elif isinstance(item, self.__class__):
            return item._index in self._children
        else:
            return NotImplemented

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._index == other._index
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._index != other._index
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, self.__class__):
            return self._index in other._children
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__):
            return other._index in self._children
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, self.__class__):
            return self._index == other._index or self._index in other._children
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, self.__class__):
            return other._index == self._index or other._index in self._children
        else:
            return NotImplemented

    def add_children(self, children):
        if isinstance(children, Iterable):
            _indexes: list[int] = []
            _ignored: list = []
            for child in children:
                if isinstance(child, int) and child not in self._children:
                    _indexes.append(child)
                elif isinstance(child, self.__class__) and child._index not in self._children:
                    _indexes.append(child._index)
                else:
                    _ignored.append(child)
            self._children.extend(_indexes)
            logger.info(f"Indexes {_indexes} are added")
            logger.debug(f"Items {', '.join([str(_) for _ in _ignored])} are ignored")
        elif isinstance(children, int):
            if children not in self._children:
                self._children.append(children)
                logger.debug(f"Child {children} is added")
            else:
                logger.info(f"Child {children} is already listed")
        elif isinstance(children, self.__class__):
            if children._index not in self._children:
                self._children.append(children._index)
                logger.debug(f"Child {children._index} is added")
            else:
                logger.info(f"Child {children._index} is already listed")
        else:
            return

    def set_parent(self, index: int):
        if index not in self.__class__.tree_items.keys():
            logger.error(f"Index {index} is not found")
            raise TreeElementIndexNotFoundError
        if index == self._index:
            logger.error(f"Index {index} points to the same instance")
            raise TreeElementParentIndexError
        self._parent = index
        self.__class__.tree_items[index].add_children(self._index)
        return

    def find_parent(self):
        element: Self
        for element in self.__class__.tree_items.values():
            if self._index in element._children:
                self._parent = element._index
        else:
            self._parent = None

    def find_children(self):
        element: Self
        _children: list[int] = [
            element._index for element in self.__class__.tree_items.values()
            if element._parent == self._index]
        self._children.extend(_children)

    def remove(self):
        self.parent_element.add_children(self._children)
        self.parent_element._children.remove(self._index)
        for child in self.iter_elements():
            child.set_parent(self._parent)
        self.__class__.tree_items.pop(self._index)
        del self


class RootElement(TreeElement):
    def __init__(self, children: Iterable[int] = None, **kwargs):
        parent = None
        super().__init__(parent, children, **kwargs)
        self._index: int = 0

        self.__class__.tree_items[0] = self
        self.__class__.identifier += 1

    def __bool__(self):
        return True

    def __str__(self):
        return f"{self.__class__.__name__}: 0"

    def __repr__(self):
        return f"<{self.__class__.__name__}({', '.join([str(_) for _ in self._children])})>"



StrPath: TypeAlias = str | Path



class CustomTable:
    def __init__(self, table_items: Iterable[TableItem] = None):
        if table_items is None:
            table_items: list[TableItem] = []
        self._table_items: list[TableItem] = [*table_items]

    def __getitem__(self, item):
        if isinstance(item, TableCellCoordinate):
            for _ in self._table_items:
                if _.coord == item:
                    return _
            else:
                return TableItem.null_table_item()
        elif isinstance(item, (int, slice)):
            try:
                _: TableItem = self._table_items[item]
            except (KeyError, RuntimeError, OSError) as e:
                logger.info(f"{e.__class__.__name__}, {str(e)}")
                raise
        else:
            logger.info(f"Item {item} must be TableCoordinate, int or slice, but {type(item)} received")
            return TableItem.null_table_item()

    def __str__(self):
        return "".join([str(_) for _ in iter(self)])

    def __iter__(self) -> Iterator[TableItem]:
        return iter(self._table_items)

    @property
    def coords(self) -> list[TableCellCoordinate]:
        return [_.coord for _ in iter(self)]

    @property
    def texts(self) -> list[str]:
        return [_.text for _ in iter(self)]

    @property
    def max_row(self) -> int:
        return max([_.row_index for _ in self.coords])

    @property
    def max_column(self) -> int:
        return max([_.column_index for _ in self.coords])

    def get_row_items(self, row_index: int) -> list[TableItem]:
        if not 0 < row_index < self.max_row:
            logger.info(f"Row index is out of bounds")
            raise TableElementIndexError
        return [_ for _ in iter(self) if _.row_index == row_index]

    def get_column_items(self, column_index: int) -> list[TableItem]:
        if not 0 < column_index < self.max_column:
            logger.info(f"Column index is out of bounds")
            raise TableElementIndexError
        return [_ for _ in iter(self) if _.column_index == column_index]

    def items_rows(self) -> list[list[TableItem]]:
        return [self.get_row_items(row_index) for row_index in range(self.max_row)]

    def items_columns(self) -> list[list[TableItem]]:
        return [self.get_column_items(column_index) for column_index in range(self.max_column)]

    def iter_rows(self) -> Iterator[list[TableItem]]:
        return iter(self.items_rows())

    def iter_columns(self) -> Iterator[list[TableItem]]:
        return iter(self.items_columns())

    def insert_row(self, index: int, table_items: Iterable[TableItem] = None):
        pass

    def insert_column(self, index: int, table_items: Iterable[TableItem] = None):
        pass
