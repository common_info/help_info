from pathlib import Path
from re import Pattern, compile, fullmatch, Match
from typing import Iterable, Sequence


def read_file(file: str):
    with open(file, "r+", encoding="utf-8", errors="ignore") as f:
        _: str = f.read()
    return _.split("\n")


def write_file(file: str, lines: Sequence[str] = None):
    if lines is None:
        return
    with open(file, "w", encoding="utf-8") as f:
        f.writelines([f"{_}\n" for _ in lines[-1]])
        f.write(lines[-1])
    return
#
#
# def multiple_replace(line: str, replacement: Mapping[str, str] = None):
#     if replacement is None:
#         return line
#     for k, v in replacement.items():
#         line = line.replace(k, v)
#     return line
#
#
# def find_pattern_file(file: str, pattern: Pattern, replacement: Mapping[int, str] = None):
#     if replacement is None:
#         return
#     _content: list[str] = read_file(file)
#     for index, line in enumerate(_content):
#         _m: Match = fullmatch(pattern, line)
#         if _m:
#             _replacement = {_m.group(_index): replacement.get(_index) for _index in replacement}
#             print(line)
#             _content[index] = multiple_replace(line, _replacement)
#             print(line)
#     print(_content)
#     write_file(file, _content)
#
#
#
# def reformat(file: str, pattern: Pattern):
#     _content: list[str] = read_file(file)
#     for index, line in enumerate(_content):
#         _m: Match = fullmatch(pattern, line)
#         if _m:
#
#
#
# def find_match(pattern: Pattern, line: str):
#     _m: Match = fullmatch(pattern, line)
#     return _m.groups()
#
#
# if __name__ == '__main__':
#     _file = r"C:\Users\tarasov-a\PycharmProjects\help_info\test.md"
#     _pattern: Pattern = compile(r"\*\s(\d+)\s-\s(.*)")
#     _replace = {1: "__LINUX__"}
#     print(find_pattern_file(_file, _pattern, _replace))
#     _ = multiple_replace("check_test_item", {"test": "__test__"})
#     print(_)


def reformat(file: str | Path):
    _content: list[str] = read_file(file)
    pattern: Pattern = compile(r"\*\s(\d+)\s-\s(.*)")
    for index, line in enumerate(_content):
        _m: Match = fullmatch(pattern, line)
        if _m:
            _content[index] = f"| {_m.group(1)} | {_m.group(2)} |"
        else:
            _content[index] = line
    print(type(_content))
    write_file(file, _content)


if __name__ == '__main__':
    _path: Path = Path(r"C:\Users\tarasov-a\PycharmProjects\Protei_MME\content\common\data_records\cdr")
    encoding: str = "cp1251"
    for _file in _path.iterdir():
        reformat(_file)
        # with open(_file, "rb") as f:
        #     content: bytes = f.read()
        # with open(_file, "w+") as f:
        #     f.write(content.decode(encoding))
