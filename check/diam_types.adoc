== Diameter AVP Types
:author: Andrew Tar
:email: andrew.tar@yahoo.com
// :lang: ru
:icons: font

Source: https://www.ietf.org/rfc/rfc3588.txt[RFC 3588].

.Generic Types
|===

|Name |Description Eng |Description Rus |Length (+V) |Additional Info

|[[integer-32]]Integer32
|Signed 32-bit integer
a|
[%collapsible]
====
Знаковое 32-битное целое число
====
|12 (16)
|

|Integer64
|Signed 64-bit integer
|Знаковое 64-битное целое число
|16 (20)
|

|Unsigned32
|Unsigned 32-bit integer
|Беззнаковое 32-битное целое число
|12 (16)
|

|Unsigned64
|Unsigned 64-bit integer
|Беззнаковое 64-битное целое число
|16 (20)
|

|Float32
|32-bit floating point single precision number
|32-битное число одинарной точности с плавающей точкой
|12 (16)
|

|Float64
|64-bit floating point single precision number
|64-битное число одинарной точности с плавающей точкой
|16 (20)
|

|[[octet-string]]OctetString
|String data of any length
|Строка произвольной длины
|8 (12)
|padded up to the full block, дополняется значениями до размеров блока

|Grouped
|Combination of values having different types
|Набор значений различных типов
|8 (12) + сумма всех AVP
|size must be a divisor of 4, размер должен быть кратен 4

| UTF8String +
(xref:diam_types.adoc#octet-string[OctetString])
|String encoded with UTF-8
|Строка с кодировкой UTF-8
|Any (in octets), любая (в октетах)
|No flags are recommended, if required, *CR LF* +
Не рекомендуются флаги, если необходимо, то *CR LF*

|Enumerated +
(xref:diam_types.adoc#integer-32[Integer32])
|Set of available options
|Перечень допустимых значений
|12 (16)
|

|Time +
(xref:diam_types.adoc#octet-string[OctetString])
|Timestamp from Jan 1, 1900, with NTP format
|Временная метка от 1 января, 1900, в формате NTP
|4 octets, 4 октета
|

|DiameterIdentity +
(xref:diam_types.adoc#octet-string[OctetString])
|Node Fully Qualified Domain Name (FDQN)
|Полностью определенное доменное имя узла
|8 (12)
|

| Address +
(xref:diam_types.adoc#octet-string[OctetString])
|IPv4/IPv6 address
|IPv4-/IPv6-адрес
|8 (12)
|

|DiameterURI
|Formatted URI
|URI специального формата
|
|xref:diam_types.adoc#address-format[Address format]

|IPFilterRule +
(xref:diam_types.adoc#octet-string[OctetString])
|Packet filter
|Фильтр пакетов
|
|xref:diam_types.adoc#filter-format[Filter format]

|===

[#address-format]
=== Address Format

[example]
aaa(s)://<FQDN>:{port=3868};{xref:diam_types.adoc#transport[transport]=sctp};
{xref:diam_types.adoc#proto[proto]=diameter}

[#transport]
Transport is one of udp/tcp/*sctp*.

[#proto]
Protocol is one of radius/tacacs+/*diameter*.

[#filter-format]
=== Filter Format

[example]
----
{xref:diam_types.adoc:#action[action] } {[dir](#dir)} {proto} [{src} {dest}](#src-and-dest) [{frag} {ipoptions spec}
{tcpoptions spec} {established} {setup} {tcpflags spec} {icmptypes types}](#options)
----

[#action]

* permit: allow packets that match the rule;
* deny: drop packets that match the rule.

==== Dir

* in: from the terminal;
* out: to the terminal.

==== Src and Dest

* ipno: IPv4/IPv6 address in dotted-quad or canonical IPv6 form;
* ipno/bits: IP address with a mask.

==== Options

* frag: match if the packet is a fragment and not the first one of the datagram;
* ipoptions spec: match if IP header contains the comma-separated list of options specified in spec;
* tcpoptions spec: match if TCP header contains the comma-separated list of options specified in spec;
* established: match TCP only packets that have the RST or ACK bits set;
* setup: match TCP only packets that have the SYN bit set but no ACK one;
* tcpflags spec: match TCP only packets if the TCP header contains the comma-separated list of flags specified in spec;
* icmptypes types: match ICMP only packets.
