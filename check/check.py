from pathlib import Path
from typing import TypeAlias

StrPath: TypeAlias = str | Path


def generate_pandoc_link(file_name: StrPath, extension: str, origin_file: StrPath):
    return f"pandoc --from=docx --to=asciidoc --standalone --output={file_name}.{extension} {origin_file}"


if __name__ == '__main__':
    _: str = generate_pandoc_link("rg", "adoc", r"C:\Users\tarasov-a\Desktop\PROTEI_RG.Admin_guide_2022_upd.docx")
    print(_)
