from enum import Enum
from typing import Any


class ValidationState(Enum):
    SUCCESS = "success"
    FAIL = "fail"
    NONE = "None"

    def __str__(self):
        return f"{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._name_}, {self._value_})>"

    def __bool__(self):
        return self is not self.__class__.NONE

    def __int__(self):
        _state: dict[str, int] = {"success": 1, "fail": 0, "None": -1}
        return _state.get(self._value_, -1)


class Validator:
    __slots__ = ("_type", "_value", "_state")

    def __init__(self, validation_type: type, value: Any = None):
        self._type: type = validation_type
        self._value: Any = value
        self._state: ValidationState = ValidationState.NONE
        self.validate()

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, v):
        self._value = v

    @value.deleter
    def value(self):
        self._value = None

    @classmethod
    def __call__(cls, *args, **kwargs):
        if "validation_type" in kwargs:
            validation_type = kwargs.get("validation_type")
            if not isinstance(validation_type, type):
                raise TypeError
            _validator = cls(validation_type)
        elif isinstance(args[0], type):
            _validator = cls(args[0])
        else:
            raise AttributeError
        if "value" in kwargs:
            _value = kwargs.get("value")
        elif len(args) >= 2:
            _value = args[1]
        else:
            _value = None
        _validator._value = _value
        return _validator

    def validate(self):
        return NotImplementedError

    def is_valid(self):
        return self._state == ValidationState.SUCCESS

    def __bool__(self):
        return self._state != ValidationState.NONE


class ValidatorInt(Validator):
    def __init__(self, value: Any = None):
        validation_type: type = int
        super().__init__(validation_type, value)

    def validate(self):
        if not (isinstance(self._value, int) and issubclass(self._value.__class__, int)):
            self._state = ValidationState.FAIL
        else:
            self._state = ValidationState.SUCCESS
        return


class ValidatorUint(Validator):
    def __init__(self, value: Any = None):
        validation_type: type = int
        super().__init__(validation_type, value)

    def validate(self):
        if not (isinstance(self._value, int) and issubclass(self._value.__class__, int)):
            self._state = ValidationState.FAIL
        elif self._value < 0:
            self._state = ValidationState.FAIL
        else:
            self._state = ValidationState.SUCCESS
        return


class ValidatorString(Validator):
    def __init__(self, value: Any = None):
        validation_type: type = str
        super().__init__(validation_type, value)

    def validate(self):
        if not (isinstance(self._value, str) and issubclass(self._value.__class__, str)):
            self._state = ValidationState.FAIL
        else:
            self._state = ValidationState.SUCCESS
        return


class ValidatorStringLengthAtLeast(Validator):
    def __init__(self, value: Any = None, length: int = 0):
        validation_type: type = str
        super().__init__(validation_type, value)
        self._length: int = length

    def validate(self):
        _validator_int: ValidatorUint = ValidatorUint(self._length)
        if not ValidatorUint(self._length).is_valid():
            self._state = ValidationState.FAIL
        if not ValidatorString(self._value).is_valid():
            self._state = ValidationState.FAIL
        if self._length == 0:
            self._state = ValidationState.FAIL
        elif len(self._value) < self._length:
            self._state = ValidationState.FAIL
        else:
            self._state = ValidationState.SUCCESS
        return


class ValidatorStringLengthNotMore(Validator):
    def __init__(self, value: Any = None, length: int = 0):
        validation_type: type = str
        super().__init__(validation_type, value)
        self._length: int = length

    def validate(self):
        _validator_int: ValidatorUint = ValidatorUint(self._length)
        if not ValidatorUint(self._length).is_valid():
            self._state = ValidationState.FAIL
        if not ValidatorString(self._value).is_valid():
            self._state = ValidationState.FAIL
        if self._length == 0:
            self._state = ValidationState.FAIL
        elif len(self._value) > self._length:
            self._state = ValidationState.FAIL
        else:
            self._state = ValidationState.SUCCESS
        return


class ValidatorStringLengthMoreThan(Validator):
    def __init__(self, value: Any = None, length: int = 0):
        validation_type: type = str
        super().__init__(validation_type, value)
        self._length: int = length

    def validate(self):
        _validator_int: ValidatorUint = ValidatorUint(self._length)
        if not ValidatorUint(self._length).is_valid():
            self._state = ValidationState.FAIL
        if not ValidatorString(self._value).is_valid():
            self._state = ValidationState.FAIL
        if self._length == 0:
            self._state = ValidationState.FAIL
        elif len(self._value) < self._length:
            self._state = ValidationState.FAIL
        else:
            self._state = ValidationState.SUCCESS
        return


class ValidatorStringLengthLessThan(Validator):
    def __init__(self, value: Any = None, length: int = 0):
        validation_type: type = str
        super().__init__(validation_type, value)
        self._length: int = length

    def validate(self):
        _validator_int: ValidatorUint = ValidatorUint(self._length)
        if not ValidatorUint(self._length).is_valid():
            self._state = ValidationState.FAIL
        if not ValidatorString(self._value).is_valid():
            self._state = ValidationState.FAIL
        if self._length == 0:
            self._state = ValidationState.FAIL
        elif len(self._value) > self._length:
            self._state = ValidationState.FAIL
        else:
            self._state = ValidationState.SUCCESS
        return


class ValidatorBool(Validator):
    def __init__(self, value: Any = None):
        validation_type: type = bool
        super().__init__(validation_type, value)

    def validate(self):
        if not (isinstance(self._value, bool)):
            self._state = ValidationState.FAIL
        else:
            self._state = ValidationState.SUCCESS
        return


class ValidatorList(Validator):
    def __init__(self, item_type: type):
        validation_type: type = list
        super().__init__(validation_type)