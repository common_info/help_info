:author: Andrew Tar
:email: andrew.tar@yahoo.com
:revdate: Jun 1, 2023
:revnumber: 1.0.0
:chapter-signifier: Раздел
:experimental:
:icons: font
:toc: auto
:toclevels: 5
:toc-title: Содержание

include::top_page.adoc[]

<<<

include::putty.adoc[]

<<<

include::cmd.adoc[]

<<<

include::draw_io.adoc[]