= Parameters and 3GPP TS Standards
:author: Andrew Tar
:email: andrew.tar@yahoo.com
:lang: ru
:icons: font

[%header]
|===

|Parameter |Standard

|APN
|https://www.etsi.org/deliver/etsi_ts/123000_123099/123003/16.05.00_60/ts_123003v160500p.pdf[3GPP TS 23.003]

|ECI
|https://www.etsi.org/deliver/etsi_ts/136400_136499/136413/17.04.00_60/ts_136413v170400p.pdf[3GPP TS 36.413]

|GUTI
|https://www.etsi.org/deliver/etsi_ts/123000_123099/123003/16.05.00_60/ts_123003v160500p.pdf[3GPP TS 23.003]

|IMEI
|https://www.etsi.org/deliver/etsi_ts/122000_122099/122016/17.00.01_60/ts_122016v170001p.pdf[3GPP TS 22.016]

|IMSI
|https://www.itu.int/rec/T-REC-E.212-201609-I/en[Recommendation ITU-T E.212]

|MME UE ID
|

|MSISDN
|https://www.itu.int/rec/T-REC-E.164-201011-I[Recommendation ITU-T E.164]

|OpCode
|

|PDN Type
|https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf[3GPP TS 24.301]

|PLMN
|https://www.etsi.org/deliver/etsi_ts/122100_122199/122101/17.05.00_60/ts_122101v170500p.pdf[3GPP TS 22.101]

|self TEID
|https://www.etsi.org/deliver/etsi_ts/129000_129099/129060/17.04.00_60/ts_129060v170400p.pdf[3GPP TS 29.060]

|SGW IP
|

|SGW TEID
|https://www.etsi.org/deliver/etsi_ts/129000_129099/129060/17.04.00_60/ts_129060v170400p.pdf[3GPP TS 29.060]

|HSSHost
|https://tools.ietf.org/html/rfc6733[RFC 6733]

|MMEHost
|https://tools.ietf.org/html/rfc6733[RFC 6733]

|MMERealm
|https://tools.ietf.org/html/rfc6733[RFC 6733]

|SessionId
|https://tools.ietf.org/html/rfc6733[RFC 6733]

|SGSN_Number
|

|===
