def _validate_int(value: str):
    if not value.isdigit():
        raise ValueError(f"Value {value} must be digit")
    return


def _validate_hex(value: str):
    if not value.startswith("0x"):
        raise ValueError(f"Value {value} must begin with the '0x' prefix")


def get_hex(value: int | str):
    if isinstance(value, str):
        _validate_int(value)
        return hex(int(value, 10))
    else:
        return hex(value)


def get_int(value: str):
    _validate_hex(value)
    return int(value[2:], 16)


if __name__ == '__main__':
    print(get_hex(8010))
