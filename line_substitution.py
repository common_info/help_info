from pathlib import Path
from re import Pattern, fullmatch, compile
from typing import Iterator, Mapping

from core.root_folder import RootFolder, StrPath


class FileSubstitution(RootFolder):
    def __init__(self, path: StrPath, conversion_table: Mapping[str, str] = None):
        super().__init__(path)
        if conversion_table is None:
            conversion_table: dict[str, str] = dict()
        self._conversion_table: dict[str, str] = {**conversion_table}

    def __iter__(self):
        return iter(self.recursive_files())

    def iter_select_files(self, pattern: Pattern) -> Iterator[Path]:
        return iter(file for file in iter(self) if fullmatch(pattern, file))

    def __add__(self, other):
        if isinstance(other, Mapping):
            for k, v in other.items():
                if k not in self._conversion_table:
                    self._conversion_table[k] = v
                else:
                    continue
        else:
            return

    @property
    def conversion_table(self):
        return self._conversion_table


class SubFile:
    def __init__(self, path: Path, file_substitution: FileSubstitution):
        self._path: Path = path
        self._file_substitution: FileSubstitution = file_substitution
        self._content: list[str] = []

    def __iter__(self):
        return iter(self._content)

    def __getitem__(self, item):
        if isinstance(item, (int, slice)):
            return self._content[item]
        else:
            return NotImplemented

    def __setitem__(self, key, value):
        if isinstance(key, int) and isinstance(value, str):
            self._content[key] = value
        else:
            return NotImplemented

    def read_file(self):
        with open(self._path, "r+", encoding="utf-8") as f:
            _: list[str] = f.readlines()
        self._content = _

    def write_file(self):
        _: str = "\n".join(self._content)
        with open(self._path, "w+") as f:
            f.write(_)
        self._content = []

    def substitute_lines(self):
        for index, line in enumerate(self._content):
            for k, v in self._file_substitution.conversion_table.items():
                if fullmatch(compile(k), line):
                    self[index] = v


if __name__ == '__main__':
    _path: str = r"C:\Users\tarasov-a\PycharmProjects\Protei_MME\content\common"
    _conversion_table: dict[str, str] = {
        r"\| Name\s*\|\sDescription\s*\|\sType\s*\|\sDefault\s*\|\sO/M\s*\|\sP/R\s*\|\sVersion\s*\|":
            "| Поле | Описание | Тип | По умолчанию | O/M | P/R | Версия |",
        r"\|\sN\s*\|\sField\s*\|\sDescription\s*\|\sType\s*\|":
            "| N | Поле | Описание | Тип |",
        r"\|\sN\s*|\sDescription\s*\|": "| N | Описание |"
    }
    file_substitution: FileSubstitution = FileSubstitution(_path, _conversion_table)
    for file in iter(file_substitution):
        sub_file: SubFile = SubFile(file, file_substitution)
        sub_file.read_file()
        sub_file.substitute_lines()
        sub_file.write_file()
