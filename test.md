---
title : "S1AP Context CDR"
description : ""
weight : 14
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
`gitlab_docs_path: "content/common/data_records/cdr/s1ap_context.md"
`gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_MME"
gitlab_project_path: "MobileDevelop/Protei_MME"
---

### �������� ������������ ����� ###

| N   | Field                    | Description                                                                                                                                                                                        | Type      |
|-----|--------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| 1   | DateTime                 | ���� � ����� �������. ������:<br>`YYYY-MM-DD HH:MM:SS`.                                                                                                                                            | datetime  |
| 2   | [Event](#event)          | ������� ��� ���������, �������������� ������.                                                                                                                                                      | string    |
| 3   | eNodeB UE ID             | ?������������ S1-��������� �� ������� eNodeB.                                                                                                                                                      | int       |
| 4   | MME UE ID                | ?������������ S1-��������� �� ���� MME.                                                                                                                                                            | int       |
| 5   | IMSI                     | ����� IMSI ��������.                                                                                                                                                                               | string    |
| 6   | MSISDN                   | ����� MSISDN ��������.                                                                                                                                                                             | string    |
| 7   | GUTI                     | ���������� ���������� ��������� ������������� ��������.                                                                                                                                            | string    |
| 8   | IMEI                     | ����� IMEI ����������.                                                                                                                                                                             | string    |
| 9   | PLMN                     | ?������������ PLMN.                                                                                                                                                                                | string    |
| 10  | TAC                      | ��� ������� ������������.                                                                                                                                                                          | string    |
| 11  | CellID                   | ?������������ ����.                                                                                                                                                                                | int/hex   |
| 12  | CauseType                | ��� �������.                                                                                                                                                                                       | string    |
| 13  | CauseReason              | ������� �������.                                                                                                                                                                                   | string    |
| 14  | Duration                 | ������������ ���������.                                                                                                                                                                            | int<br>�� |
| 15  | Procedure Type:ErrorCode | ��� ��������� + ���������� [��� MME](error_code) ����������. ������:<br>`<procedure_type>:<error_code>`.<br>1 --- [Service Req](#service-req);<br>2 --- [S1 Context Release](#s1-context-release). | object    |

#### <a name="event">������� � ���������, Event</a> ####

- [Service Req](#service-req);
- [S1 Context Release](#s1-context-release);
- [Reset](#reset);

#### <a name="service-req">Service Req</a> ####

```log
DateTime;Service Req;eNodeB UE ID;MME UE ID;IMSI;MSISDN;GUTI;IMEI;PLMN;TAC;CellID;0;0;Duration;1:ErrorCode;
```

��������� ���� ������

| 1 | ������������ PLMN � Service Request |
| 2 | � �������� ��� ���������� ������� |
| 3 | �������� ����������� ����� |
| 4 | ������ ��� ����������� ������ |
| 5 | ����������� User Data Container |
| 6 | �� ������� ������������ ESM Message Container |
| 7 | �� ������� �������� �������� |
| 8 | ���������� VLR GT ��� ������� �������� |
| 9 | ������ � ��������� Initial Context Setup |
| 10 | ��������� Service Req �� �������� � ��������� MME |

### S1 Context Release
**S1 Context Release;ENB UE ID;MME UE ID;IMSI;MSISDN;GUTI;IMEI;PLMN;TAC;Cell ID;CauseType;CauseReason;Duration;2:ErrorCode**

### Reset
**Reset;ENB UE ID;MME UE ID;IMSI;MSISDN;GUTI;IMEI;PLMN;TAC;Cell ID;CauseType;CauseReason;Duration;3:ErrorCode**

## Cause
���� **CauseType** � **CauseReason** ��������� ������� ������� S1AP

**CauseType** - ������ ������� (��.TS 36.413, 9.2.1.3 Cause - CHOICE Cause
Group)

��������� ��������:

| ������                    | �������� |
|---------------------------|----------|
| Radio Network Layer cause | 1        |
| Transport Layer cause     | 2        |
| NAS cause                 | 3        |
| Protocol cause            | 4        |
| Miscellaneous cause       | 5        |

**CauseReason** - ���������� ����� ������� ������ ������ ������ [CauseType](#causetype). ������ ������ �������� ������ � TS 36.413, ������ 9.2.1.3 Cause

#### Radio Network Layer cause

������� �������� CauseReason ��� Radio Network Layer cause

| ��� | �������                                                         | ��������                                                                                                                                                                                                                                                                                                                   |
|-----|-----------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 0   | Unspecified                                                     | Sent for radio network layer cause when none of the specified cause values applies.                                                                                                                                                                                                                                        |
| 1   | TX2RELOCOverall Expiry                                          | The timer guarding the handover that takes place over X2 has abnormally expired.                                                                                                                                                                                                                                           |
| 2   | Successful Handover                                             | Successful handover.                                                                                                                                                                                                                                                                                                       |
| 3   | Release due to E-UTRAN generated reason                         | Release is initiated due to E-UTRAN generated reason.                                                                                                                                                                                                                                                                      |
| 4   | Handover Cancelled                                              | The reason for the action is cancellation of Handover.                                                                                                                                                                                                                                                                     |
| 5   | Partial Handover                                                | Provides a reason for the handover cancellation. The HANDOVER COMMAND message from MME contained E-RABs to Release List IE and the source eNB estimated service continuity for the UE would be better by not proceeding with handover towards this particular target eNB.                                                  |
| 6   | Handover Failure In Target EPC/eNB Or Target System             | The handover failed due to a failure in target EPC/eNB or target system.                                                                                                                                                                                                                                                   |
| 7   | Handover Target not allowed                                     | Handover to the indicated target cell is not allowed for the UE in question.                                                                                                                                                                                                                                               |
| 8   | TS1RELOCoverall Expiry                                          | The reason for the action is expiry of timer TS1RELOCoverall.                                                                                                                                                                                                                                                              |
| 9   | TS1RELOCprep Expiry                                             | Handover Preparation procedure is cancelled when timer TS1RELOCprep expires.                                                                                                                                                                                                                                               |
| 10  | Cell not available                                              | The concerned cell is not available.                                                                                                                                                                                                                                                                                       |
| 11  | Unknown Target ID                                               | Handover rejected because the target ID is not known to the EPC.                                                                                                                                                                                                                                                           |
| 12  | No radio resources available in target cell                     | Load on target cell is too high.                                                                                                                                                                                                                                                                                           |
| 13  | Unknown or already allocated MME UE S1AP ID                     | The action failed because the MME UE S1AP ID is either unknown, or (for a first message received at the eNB) is known and already allocated to an existing context.                                                                                                                                                        |
| 14  | Unknown or already allocated eNB UE S1AP ID                     | The action failed because the eNB UE S1AP ID is either unknown, or (for a first message received at the MME) is known and already allocated to an existing context.                                                                                                                                                        |
| 15  | Unknown or inconsistent pair of UE S1AP ID                      | The action failed because both UE S1AP IDs are unknown, or are known but do not define a single UE context.                                                                                                                                                                                                                |
| 16  | Handover Desirable for Radio Reasons                            | The reason for requesting handover is radio related.                                                                                                                                                                                                                                                                       |
| 17  | Time Critical Handover                                          | Handover is requested for time critical reason i.e., this cause value is reserved to represent all critical cases where the connection is likely to be dropped if handover is not performed.                                                                                                                               |
| 18  | Resource Optimisation Handover                                  | The reason for requesting handover is to improve the load distribution with the neighbour cells.                                                                                                                                                                                                                           |
| 19  | Reduce Load in Serving Cell                                     | Load on serving cell needs to be reduced. When applied to handover preparation, it indicates the handover is triggered due to load balancing.                                                                                                                                                                              |
| 20  | User Inactivity                                                 | The action is requested due to user inactivity on all E-RABs, e.g., S1 is requested to be released in order to optimise the radio resources.                                                                                                                                                                               |
| 21  | Radio Connection With UE Lost                                   | The action is requested due to losing the radio connection to the UE.                                                                                                                                                                                                                                                      |
| 22  | Load Balancing TAU Required                                     | The action is requested for all load balancing and offload cases in the MME.                                                                                                                                                                                                                                               |
| 23  | CS Fallback triggered                                           | The action is due to a CS fallback that has been triggered. When it is included in UE CONTEXT RELEASE REQUEST message, it indicates the PS service suspension is not required in the EPC.                                                                                                                                  |
| 24  | UE Not Available for PS Service                                 | The action is requested due to a CS fallback to GERAN that has been triggered. When it is included in the UE CONTEXT RELEASE REQUEST message, it indicates that the PS service suspension is required in the EPC due to the target GERAN cell or the UE has no DTM capability.                                             |
| 25  | Radio resources not available                                   | No requested radio resources are available.                                                                                                                                                                                                                                                                                |
| 26  | Invalid QoS combination                                         | The action was failed because of invalid QoS combination.                                                                                                                                                                                                                                                                  |
| 27  | Inter-RAT Redirection                                           | The release is requested due to inter-RAT redirection or intra-LTE redirection. When it is included in UE CONTEXT RELEASE REQUEST message, the behaviour of the EPC is specified in TS 23.401 [11].                                                                                                                        |
| 28  | Failure in the Radio Interface Procedure                        | Radio interface procedure has failed.                                                                                                                                                                                                                                                                                      |
| 29  | Interaction with other procedure                                | The action is due to an ongoing interaction with another procedure.                                                                                                                                                                                                                                                        |
| 30  | Unknown E-RAB ID                                                | The action failed because the E-RAB ID is unknown in the eNB.                                                                                                                                                                                                                                                              |
| 31  | Multiple E-RAB ID Instances                                     | The action failed because multiple instance of the same E-RAB had been provided to the eNB.                                                                                                                                                                                                                                |
| 32  | Encryption and/or integrity protection algorithms not supported | The eNB is unable to support any of the encryption and/or integrity protection algorithms supported by the UE.                                                                                                                                                                                                             |
| 33  | S1 Intra system Handover triggered                              | The action is due to a S1 intra system handover that has been triggered.                                                                                                                                                                                                                                                   |
| 34  | S1 Inter system Handover triggered                              | The action is due to a S1 inter system handover that has been triggered.                                                                                                                                                                                                                                                   |
| 35  | X2 Handover triggered                                           | The action is due to an X2 handover that has been triggered.                                                                                                                                                                                                                                                               |
| 36  | Redirection towards 1xRTT                                       | The release of the UE-associated logical S1 connection is requested due to redirection towards a 1xRTT system e.g., CS fallback to 1xRTT, or SRVCC to 1xRTT, when the PS service suspension is required in the EPC. During this procedure, the radio interface message might but need not include redirection information. |
| 37  | Not supported QCI Value                                         | The E-RAB setup failed because the requested QCI is not supported.                                                                                                                                                                                                                                                         |
| 38  | Invalid CSG Id                                                  | The CSG ID provided to the target eNB was found invalid.                                                                                                                                                                                                                                                                   |
| 39  | Release due to Pre-Emption                                      | Release is initiated due to pre-emption.                                                                                                                                                                                                                                                                                   |
| 40  | N26 interface not available                                     | The action failed due to a temporary failure of the N26 interface.                                                                                                                                                                                                                                                         |

##### Transport Layer cause

������� �������� CauseReason ��� Transport Layer cause

| ��� | �������                        | ��������                                                                                                 |
|-----|--------------------------------|----------------------------------------------------------------------------------------------------------|
| 0   | Transport Resource Unavailable | The required transport resources are not available.                                                      |
| 1   | Unspecified                    | Sent when none of the above cause values applies but still the cause is Transport Network Layer related. |

##### NAS cause

������� �������� CauseReason ��� NAS cause

| ��� | �������                 | ��������                                                                             |
|-----|-------------------------|--------------------------------------------------------------------------------------|
| 0   | Normal Release          | The release is normal.                                                               |
| 1   | Authentication Failure  | The action is due to authentication failure.                                         |
| 2   | Detach                  | The action is due to detach.                                                         |
| 3   | Unspecified             | Sent when none of the above cause values applies but still the cause is NAS related. |
| 4   | CSG Subscription Expiry | The action is due to the UE becoming a non-member of the currently used CSG.         |

##### Protocol cause

������� �������� CauseReason ��� Protocol cause

| ��� | �������                                             | ��������                                                                                                             |
|-----|-----------------------------------------------------|----------------------------------------------------------------------------------------------------------------------|
| 0   | Transfer Syntax Error                               | The received message included a transfer syntax error.                                                               |
| 1   | Abstract Syntax Error (Reject)                      | The received message included an abstract syntax error and the concerning criticality indicated �reject�.            |
| 2   | Abstract Syntax Error (Ignore And Notify)           | The received message included an abstract syntax error and the concerning criticality indicated �ignore and notify�. |
| 3   | Message Not Compatible With Receiver State          | The received message was not compatible with the receiver state.                                                     |
| 4   | Semantic Error                                      | The received message included a semantic error.                                                                      |
| 5   | Abstract Syntax Error (Falsely Constructed Message) | The received message contained IEs or IE groups in wrong order or with too many occurrences.                         |
| 6   | Unspecified                                         | Sent when none of the above cause values applies but still the cause is Protocol related.                            |


##### Miscellaneous cause

������� �������� CauseReason ��� Miscellaneous cause

| ��� | �������                 | ��������                                                                             |
|-------------|------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 0           | Control Processing Overload                          | Control processing overload.                                                                                                                                          |
| 1           | Not Enough User Plane Processing Resources Available | No enough resources are available related to user plane processing.                                                                                                   |
| 2           | Hardware Failure                                     | Action related to hardware failure.                                                                                                                                   |
| 3           | O&M Intervention                                     | The action is due to O&M intervention.                                                                                                                                |
| 4           | Unspecified Failure                                  | Sent when none of the above cause values applies and the cause is not related to any of the categories Radio Network Layer, Transport Network Layer, NAS or Protocol. |
| 5           | Unknown PLMN                                         | The MME does not identify any PLMN provided by the eNB.                                                                                                               |

